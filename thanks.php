<?php 
    include ('./elements/header.php');

    require './libs/PHPMailer/src/PHPMailer.php';
    require './libs/PHPMailer/src/Exception.php';
    require './libs/PHPMailer/src/SMTP.php';
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
?>

        <section class="contentWrapper">
        <?php 
            include ('./elements/sidebar.php');
            include ('./elements/db.php');
            
        ?>
        <?php
            function gateway($method, $data) {
                $curl = curl_init(); // Инициализируем запрос
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://3dsec.sberbank.ru/payment/rest/'.$method, // Полный адрес метода
                    CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
                    CURLOPT_POST => true, // Метод POST
                    CURLOPT_POSTFIELDS => http_build_query($data) // Данные в запросе
                ));
                $response = curl_exec($curl); // Выполненяем запрос

                $response = json_decode($response, true); // Декодируем из JSON в массив
                curl_close($curl); // Закрываем соединение
                return $response; // Возвращаем ответ
            }



            if (isset($_GET['orderId']) || $_COOKIE['type'] == '1') {

                $ch = curl_init('./ORDER/index.php');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, array(
                    'name' => $_COOKIE['name'],
                    'phone' => $_COOKIE['phone'],
                    'email' => $_COOKIE['email'],
                    'city' => $_COOKIE['city'],
                    'street' => $_COOKIE['street'],
                    'house'   => $_COOKIE['house'],
                    'apt' => $_COOKIE['apt'],
                    'total' => $_COOKIE['total'],
                    'last_id' => $_COOKIE['orderId'],
                    'goods' => $_COOKIE['basket'],
                    'weight' => $_COOKIE['weight'],
                    'length' => $_COOKIE['length'],
                    'width' => $_COOKIE['width'],
                    'height' => $_COOKIE['height']
                ));
                curl_close($ch);

                $data = array(
                    'userName' => 'shop-assorti-api',
                    'password' => 'shop-assorti',
                    'orderId' => $_GET['orderId']
                );
                $response = gateway('getOrderStatus.do', $data);
                
                $db->query("UPDATE `order` SET paid='1' WHERE id=".$_COOKIE['orderId']);

                $basket = json_decode($_COOKIE['basket']);
                foreach ($basket as $value) { 
                    try {
                        mysqli_autocommit($db,FALSE);
                        if ($db->query("UPDATE `good` SET in_stock=in_stock-".$value->quantity." WHERE id=".$value->id)) {
                            mysqli_commit($db);
                        } else {
                            mysqli_rollback($db);
                        }
                    } catch (\Throwable $th) {
                        var_dump($th);
                    }
                }


                if ($response['ErrorCode'] == 0) {
                    $db->query("UPDATE `order` SET paid='1' WHERE id=".$_COOKIE['orderId']);




                    $emailAddress = array();
                    $contacts_res = mysqli_query($db, "SELECT * FROM contacts");
                    while ($contacts_row = mysqli_fetch_array($contacts_res)) {
                        $emailAddress[] = $contacts_row['email'];
                    } 
                    $emailAddress[] = 'zakaz@5agency.ru';
                    $mail = new PHPMailer();
                    $mail->isSMTP();                   // Отправка через SMTP
                    $mail->Host   = 'smtp.gmail.com';  // Адрес SMTP сервера
                    $mail->SMTPAuth   = true;          // Enable SMTP authentication
                    $mail->Username   = 'karailfokus';       // ваше имя пользователя
                    $mail->Password   = 'karail2002fokus';    // ваш пароль
                    $mail->SMTPSecure = 'ssl';         // шифрование ssl
                    $mail->Port   = 465;    
                    $mail->CharSet = "utf-8";
                    $mail->setFrom('karailfokus@gmail.com', 'Ilkara');    // от кого
                    for ($i = 0; $i < count($emailAddress); $i++) {
                        $mail->addAddress($emailAddress[$i]);
                    } 
                    $mail->Subject = 'Заказ';                       // тема письма
                    $mail->msgHTML('
                    <html>
                    </body>
                        <h4>От '.$_COOKIE['name'].'</h4>
                        <ul>
                            <li>Почта: '.$_COOKIE['email'].'</li>
                            <li>Телефон: '.$_COOKIE['phone'].'</li>
                        </ul>
                        Товары: 
                        '.json_encode($_COOKIE['basket']).'
                    </body>
                    </html>');
            
                    $mail->send();

                    ?>

                    <script>
                        localStorage.removeItem('basket');
                        document.cookie = "type=0"
                    </script>
                    
                
            <?php
                }
                else {?>
                    
                    <script>
                        alert('что то пошло не так');
                    </script>

            <?php
                }
            } else if ($_COOKIE['type'] == '2') {
                
                include ('./elements/db.php');
                $db->query("UPDATE `order` SET paid='1' WHERE id=".$_COOKIE['orderId']);




                $emailAddress = array();
                $contacts_res = mysqli_query($db, "SELECT * FROM contacts");
                while ($contacts_row = mysqli_fetch_array($contacts_res)) {
                    $emailAddress[] = $contacts_row['email'];
                } 
                $emailAddress[] = 'zakaz@5agency.ru';
                $mail = new PHPMailer();
                $mail->isSMTP();                   // Отправка через SMTP
                $mail->Host   = 'smtp.gmail.com';  // Адрес SMTP сервера
                $mail->SMTPAuth   = true;          // Enable SMTP authentication
                $mail->Username   = 'karailfokus';       // ваше имя пользователя
                $mail->Password   = 'karail2002fokus';    // ваш пароль
                $mail->SMTPSecure = 'ssl';         // шифрование ssl
                $mail->Port   = 465;    
                $mail->CharSet = "utf-8";
                $mail->setFrom('karailfokus@gmail.com', 'Ilkara');    // от кого
                for ($i = 0; $i < count($emailAddress); $i++) {
                    $mail->addAddress($emailAddress[$i]);
                } 
                $mail->Subject = 'Заказ';                       // тема письма
                $mail->msgHTML('
                <html>
                </body>
                    <h4>От '.$_COOKIE['name'].'</h4>
                    <ul>
                        <li>Почта: '.$_COOKIE['email'].'</li>
                        <li>Телефон: '.$_COOKIE['phone'].'</li>
                    </ul>
                    Товары: 
                    '.json_encode($_COOKIE['basket']).'
                </body>
                </html>');
        
                $mail->send();
                
                $basket = json_decode($_COOKIE['basket']);
                foreach ($basket as $value) { 
                    try {
                        mysqli_autocommit($db,FALSE);
                        if ($db->query("UPDATE `good` SET in_stock=in_stock-".$value->quantity." WHERE id=".$value->id)) {
                            mysqli_commit($db);
                        } else {
                            mysqli_rollback($db);
                        }
                    } catch (\Throwable $th) {
                        var_dump($th);
                    }
                }

                ?>

                <script>
                    localStorage.removeItem('basket');
                    document.cookie = "type=0"
                </script>

                <?php
                
            }?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'thanks'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Ожидание заказа</title>

            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [MAIN] -->
                <main>
                    <figure class="orangeBlock">
                        <img src="img/thanksGirl.png" alt="">
                        <div class="orangeBlock__wrapper">
                            <figcaption>
                                <h2>Спасибо<br>за заказ</h2>
                                <span>
                                    Заказ оплачен полностью и будет<br>
                                    доставлен вам в течение 4 дней.<br>
                                    Информация о заказе отправлена<br>
                                    на указанный вами электронный адрес.
                                </span>
                                <a href="/">Перейти на главную</a>
                            </figcaption>
                        </div>
                    </figure>
                </main>
                <!-- [/END] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    
    <?php 
        include ('./elements/footer.php');
    ?>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <!-- [/SCRIPTS] -->
</body>
</html>