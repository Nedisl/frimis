<?php 
    include ('./elements/header.php');
?>


        <section class="contentWrapper">
        <?php
            include ('./elements/sidebar.php');
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'franchise'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Франшиза</title>

            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="/">Главная</a></li>
                    <li><a href="franchise.php">Франшиза</a></li>
                </nav>
                <h1>Франшиза</h1>
                <!-- [/End Head] -->

                <!-- [MAIN CONTENT] -->
                <main>
                    <form class="franchiseForm" onsubmit="return false;">
                        <span class="franchiseForm__hText">
                            Узнайте о преимуществах и условиях франчайзинга магазинов украшений и аксессуаров «Assorti», оставив заявку и контактные данные.
                        </span>
                        <div class="franchiseForm__wrapper">
                            <div class="franchiseForm__left">
                            <input type="text" placeholder="Ваше имя" id="name">
                            <input type="text" placeholder="Ваш e-mail" id="email">
                            <input type="text" placeholder="Ваш телефон" id="phone" class="phone_mask">
                        </div>
                        <div class="franchiseForm__right">
                         <textarea placeholder="Ваше сообщение" id="message"></textarea>
                         <button class="buttonSubscribe" onclick="franchiseRequest()" style="margin: 0;">Отправить</button>
                     </div>
                        </div>
                        <span class="franchiseForm__uText">
                            Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных
                            и ознакомлен(а) с условиями конфиденциальности.
                        </span>
                    </form>

                    <button class="questionModal" data-modal=".modal__question">У меня есть вопрос</button>
                </main>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    <div class="modalWrapper">
        <section class="modal modal_medium modal__question">
            <h3>Задать вопрос</h3>
            <form onsubmit="return false;">
                <div class="form-box">
                    <input type="text" placeholder="Ваше имя" id="modal-name">
                    <input type="text" placeholder="Ваш телефон | e-mail" id="modal-email">
                    <input type="text" placeholder="Ваш город" id="modal-city">
                    <input type="text" placeholder="Ваше сообщение" id="modal-message">
                </div>
                <div class="form-box">
                    <!-- <button class="sendFile">Выбрать файл</button>
                    <input type="file" id="modal-file"> -->
                    <button class="buttonSubscribe" onclick="questionRequest()" style="margin: 0;">Отправить</button>
                </div>
                <span class="uText">Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных и ознакомлен(а) с условиями конфиденциальности.</span>
            </form>
        </section>
    </div>
    <?php 
        include ('./elements/footer.php');
    ?>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script>
        function questionRequest() {
            var questionData = {};
                questionData.modalName = $('#modal-name').val().trim();
                questionData.modalEmail = $('#modal-email').val().trim();
                questionData.modalCity = $('#modal-city').val().trim();
                questionData.modalMessage = $('#modal-message').val().trim();
                let f = false;


                if (questionData.modalName == false && questionData.modalEmail == false && questionData.modalCity == false && questionData.modalMessage == false) {
                    f = 'Не все поля заполнены';
                } else if (questionData.modalEmail != +questionData.modalEmail || questionData.modalEmail == false) {
                    if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(questionData.modalEmail) == false) {
                        f = 'Не корректный email';
                    }
                } 
                if (f) {
                    new Toast({
                        message: f,
                        type: 'danger'
                    });
                } else {
                    $.post("/api/application.php", questionData).done(function(data) {
                        if (data.status == 1) {
                            console.log(data);
                            new Toast({
                                message: 'Сообщение отправленно!',
                                type: 'danger'
                            });
                            $('#modal-name').val('');
                            $('#modal-email').val('');
                            $('#modal-city').val('');
                            $('#modal-message').val('');
                        } else {
                            new Toast({
                                message: 'Не удалось отправить сообщение!',
                                type: 'danger'
                            });
                        }
                    });
                }
        }
        function franchiseRequest() {
            var franchiseData = {};
                franchiseData.name = $('#name').val().trim();
                franchiseData.email = $('#email').val().trim();
                franchiseData.phone = $('#phone').val().trim();
                franchiseData.message = $('#message').val().trim();
                let f = false;


                if (franchiseData.name == false && (franchiseData.email == false || franchiseData.phone == false) && franchiseData.message == false) {
                    f = 'Не все поля заполнены';
                } else if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(franchiseData.email) == false) {
                    f = 'Не корректный email';
                } else if (franchiseData.phone != +franchiseData.phone || franchiseData.phone.length != 11) {
                    f = 'Не корректный номер телефона';
                }
                if (f) {
                    new Toast({
                        message: f,
                        type: 'danger'
                    });
                } else {
                    $.post("api/application.php", franchiseData).done(function(data) {
                        if (data.status == 1) {
                            new Toast({
                                message: 'Сообщение отправленно!',
                                type: 'danger'
                            });
                            $('#name').val('');
                            $('#email').val('');
                            $('#phone').val('');
                            $('#message').val('');
                        } else {
                            new Toast({
                                message: 'Не удалось отправить сообщение!',
                                type: 'danger'
                            });
                        }
                    });
                }
                
        }
    </script>
    <!-- [/SCRIPTS] -->
</body>
</html>