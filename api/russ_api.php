<?php
use Symfony\Component\Yaml\Yaml;
use LapayGroup\RussianPost\Providers\OtpravkaApi;
use LapayGroup\RussianPost\Entity\Order;

try {
    $otpravkaApi = new OtpravkaApi(Yaml::parse(file_get_contents('path_to_config.yaml')));
    
    $orders = [];
    $order = new Order();
    $order->setIndexTo(115551);
    $order->setPostOfficeCode(109012);
    $order->setGivenName('Иван');
    $order->setHouseTo('92');
    $order->setCorpusTo('3');
    $order->setMass(1000);
    $order->setOrderNum('2');
    $order->setPlaceTo('Москва');
    $order->setRecipientName('Иванов Иван');
    $order->setRegionTo('Москва');
    $order->setStreetTo('Каширское шоссе');
    $order->setRoomTo('1');
    $order->setSurname('Иванов');
    $orders[] = $order->asArr();
    
    $result = $otpravkaApi->createOrders($orders);
    var_dump($result);
}
    
catch (\InvalidArgumentException $e) {
  var_dump($e);
}
echo 'as';