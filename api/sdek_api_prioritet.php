<?php
require_once '.././ORDER/vendor/autoload.php';

use CdekSDK\Requests;

$index = $_POST['index'];
$basketHeight = $_POST['basketHeight'];

$client = new \CdekSDK\CdekClient('account', 'password');
$request = new Requests\CalculationWithTariffListRequest();
$request->setSenderCityPostCode('295000')
    ->setReceiverCityPostCode($index)
    ->addTariffToList(1)
    ->addTariffToList(8)
    ->addPackage([
        'weight' => 0.2*$basketHeight,
        'length' => 25,
        'width'  => 15,
        'height' => 10*$basketHeight,
    ]);

$response = $client->sendCalculationWithTariffListRequest($request);

/** @var \CdekSDK\Responses\CalculationWithTariffListResponse $response */

$res = array();

if ($response->hasErrors()) {
    echo json_encode($res);
} else {
    foreach ($response->getResults() as $result) {

        if (!$result->getStatus()) {
            continue;
        }
    
        $result->getTariffId();
        // int(1)
    
        $res['price'] = $result->getPrice();
        // double(1570)
    
        $res['time'] = $result->getDeliveryPeriodMin();
        // int(4)
    
        $result->getDeliveryPeriodMax();
        // int(5)
    }
    $res['weight'] = 0.2*$basketHeight;
    $res['length'] = 25;
    $res['width']  = 15;
    $res['height'] = 10*$basketHeight;
}

echo json_encode($res);
?>