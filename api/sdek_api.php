<?php
require_once '.././ORDER/vendor/autoload.php';

use CdekSDK\Requests;

$client = new \CdekSDK\CdekClient('account', 'password');
$request = new Requests\CalculationRequest();

$index = $_POST['index'];
$basketHeight = $_POST['basketHeight'];

$request->setSenderCityPostCode('295000')
    ->setReceiverCityPostCode($index)
    ->setTariffId(1)
    ->addPackage([
        'weight' => 0.2*$basketHeight,
        'length' => 25,
        'width'  => 15,
        'height' => 10*$basketHeight,
    ]);

$response = $client->sendCalculationRequest($request);


/** @var \CdekSDK\Responses\CalculationResponse $response */
if ($response->hasErrors()) {
    echo '0';
} else {
    $api = array();
    $api['weight'] = 0.2*$basketHeight;
    $api['length'] = 25;
    $api['width']  = 15;
    $api['height'] = 10*$basketHeight;
    $api['price'] = $response->getPrice();
    echo json_encode($api);
}

?>