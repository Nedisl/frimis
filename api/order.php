<?php
    include ('../elements/db.php');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    if (isset($_POST['basket'])) {

        $basket = $_POST['basket'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $lastname = $_POST['lastname'];
        $phone = $_POST['phone'];
        $city = $_POST['city'];
        $street = $_POST['street'];
        $house = $_POST['house'];
        $apt = $_POST['apt'];
        $payment = $_POST['payment'];
        $total = $_POST['total'];

        $weight = $_POST['weight'];
        $length = $_POST['length'];
        $width = $_POST['width'];
        $height = $_POST['height'];
    
        if($db->query("INSERT INTO `order` (name, lastname, phone, city, type, street, num, apt, total, payment, created_at, updated_at, paid,weight,length,width,height) VALUES('$name', '$lastname', '$phone', '$city', 'tp', '$street', '$house', '$apt', '$total', '$payment', NOW(), NOW(), '0', '$weight','$length','$width','$height')") === TRUE) {
            $last_id = $db->insert_id;
            foreach ($basket as $value) { 

                $good_id = $value['id'];
                $color = $value['color'];
                $quantity = $value['quantity'];
                $size = $value['size'];
                $good_price = $value['price'];
                $db->query("INSERT INTO `order_good` (order_id, good_id, color, size, quantity, created_at, updated_at) VALUES('$last_id', '$good_id', '$color', '$size', '$quantity', NOW(), NOW())");
           
            } 
    

            if ($_POST['type'] == 1) {
                $data = array(
                    'userName' => 'shop-assorti-api',
                    'password' => 'shop-assorti',
                    'orderNumber' => $last_id+2300,
                    'amount' => $total*100,
                    'returnUrl' => 'http://'.$_SERVER['SERVER_NAME'].'/thanks.php'
                );
    
                $response = gateway('register.do', $data);
    
                if (empty($response['orderId'])){
    
                    header('Content-type: application/json');
                    $res['status'] = 0;
                    $res['message'] = "Order successfully added with id ".$last_id;
                    echo json_encode($res);
    
                } else {
    
                    header('Content-type: application/json');
                    $res['status'] = 1;
                    $res['message'] = "Order successfully added with id ".$last_id;
                    $res['url'] = $response['formUrl'];
                    $res['name'] = $name;
                    $res['phone'] = $phone;
                    $res['email'] = $email;
                    $res['city'] = $city;
                    $res['street'] = $street;
                    $res['house'] = $house;
                    $res['total'] = $total;
                    $res['apt'] = $apt; 
                    $res['basket'] = $basket;
                    $res['id'] = $last_id;
                    echo json_encode($res);
    
                }
            } else {
                header('Content-type: application/json');
                $res['status'] = 1;
                $res['message'] = "Order successfully added with id ".$last_id;
                $res['url'] = '/thanks.php';
                $res['name'] = $name;
                $res['phone'] = $phone;
                $res['email'] = $email;
                $res['city'] = $city;
                $res['street'] = $street;
                $res['house'] = $house;
                $res['total'] = $total;
                $res['apt'] = $apt; 
                $res['basket'] = $basket;
                $res['id'] = $last_id;
                echo json_encode($res);
            }
        }   
    }

    function gateway($method, $data) {

        $curl = curl_init(); // Инициализируем запрос
        curl_setopt_array($curl, array(
            
            CURLOPT_URL => 'https://3dsec.sberbank.ru/payment/rest/'.$method, // Полный адрес метода
            CURLOPT_RETURNTRANSFER => true, // Возвращать ответ
            CURLOPT_POST => true, // Метод POST
            CURLOPT_POSTFIELDS => http_build_query($data) // Данные в запросе

        ));
        $response = curl_exec($curl); // Выполненяем запрос
    
        $response = json_decode($response, true); // Декодируем из JSON в массив
        curl_close($curl); // Закрываем соединение
        return $response; // Возвращаем ответ
    }

   			
?>