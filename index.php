<?php 
include ('./elements/header.php');
?>

        <section class="contentWrapper">
        <?php 
            include ('./elements/sidebar.php');
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'index'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Frimis</title>
        <?php
            $collection_res = mysqli_query($db, "SELECT * FROM collection");
            $collection_main_res = mysqli_query($db, "SELECT * FROM collection WHERE is_main = 1");
            $category_res = mysqli_query($db, "SELECT * FROM category WHERE is_on_main_page = 1");
            $collection_main_row = mysqli_fetch_array($collection_main_res);

            $a = array();
            if(mysqli_num_rows($collection_res) > 0) {
                while ($row = mysqli_fetch_assoc($collection_res)) {
                    $a[] = $row;
                }
            }
        ?>

            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [MAIN CONTENT] -->
                <main class="homePage">
                    <section class="firstSection">
                        <div class="mainSlider owl-carousel">
                            <?php
                                for ($i = 0; $i < mysqli_num_rows($collection_res); $i++) {
                            ?>
                                <figure style="background: #9fa3af;">
                                    <img src="<?=$pathAdm?><?= $a[$i]['background'] ?>" alt="">
                                    <figcaption>
                                        <span class="mainSlider__caption"><?php echo $a[$i]['season'] ?> <?php echo $a[$i]['year'] ?></span>
                                        <h2><?php echo $a[$i]['title'] ?></h2>
                                        <a href="/collection.php?id=<?php echo $a[$i]['id'] ?>">Смотреть</a>
                                    </figcaption>
                                </figure>
                            <?php } ?>
                        </div>
                        <style>
                            .autumnLook:hover {
                                cursor: pointer;
                            }
                        </style>
                            <figure class="autumnLook" onclick="window.location = '/looks.php?id=1'">
                            <img src="<?=$pathAdm?><?php echo $collection_main_row['image'] ?>" alt="">
                         
                                <div class="autumnLook__wrapper">
                                    <figcaption>
                                        <h2><?php echo $collection_main_row['call_to_action'] ?></h2>
                                    </figcaption>
                                </div>
                            </figure>
                    </section>
                    <section class="secondSection">
                        <?php
                            $even = 1;
                            while ($category_row = mysqli_fetch_array($category_res)) {
                                if ($even % 2 != 0) {
                        ?>
                                <figure class="whiteBox top">
                                    <img src="<?=$pathAdm?><?php echo $category_row['image']; ?>" alt="">
                                    <figcaption>
                                        <h3><?php echo $category_row['name']; ?></h3>
                                        <span>от <?php echo $category_row['price_start']; ?> р.</span>
                                        <a href="/category.php?id=<?php echo $category_row['id']; ?>">Показать</a>
                                    </figcaption>
                                </figure>
                                <?php } else { ?>
                                <figure class="whiteBox bottom">
                                    <img src="<?=$pathAdm?><?php echo $category_row['image']; ?>" alt="">
                                    <figcaption>
                                        <h3><?php echo $category_row['name']; ?></h3>
                                        <span>от <?php echo $category_row['price_start']; ?> р.</span>
                                        <a href="/category.php?id=<?php echo $category_row['id']; ?>">Показать</a>
                                    </figcaption>
                                </figure>
                        <?php
                                }
                                $even++;
                            }
                        ?>
                        <figure class="orangeBlock">
                            <img src="img/orangeBlock_girl1.png" alt="">
                            <div class="orangeBlock__wrapper">
                                <figcaption>
                                    <h2>Готовые образы<br>от стилиста</h2>
                                    <a href="/looks.php?id=1">Смотреть</a>
                                </figcaption>
                            </div>
                        </figure>
                    </section>
                </main>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    
    <?php 
        include ('./elements/footer.php');
    ?>
   

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script>
    $(".mainSlider").owlCarousel({
        loop: true,
        autoplay: true,
        items: 1,
        nav: false,
        dots: true,
        mouseDrag: true,
        touchDrag: false,
        // autoplayHoverPause: true,
        // animateOut: 'slideOutUp',
        // animateIn: 'slideInUp'
    });
    </script>
    <!-- [/SCRIPTS] -->
</body>
</html>