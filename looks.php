<?php 
//TODO: Корзина
include ('elements/header.php');
?>

        <section class="contentWrapper">
            <?php include ('./elements/sidebar.php'); ?>
            <?php 
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
                if (isset($_GET["id"])) {
                    $look_id = $_GET["id"];
                } else {
                    $look_id = 1;
                }
                $look_res = mysqli_query($db, "SELECT * FROM look LEFT JOIN look_photo ON look.id = look_photo.look_id AND look_photo.is_main = 1 WHERE look.id = $look_id LIMIT 1");
                $look_row = mysqli_fetch_assoc($look_res);
                $look_photo_res = mysqli_query($db, "SELECT * FROM look_photo WHERE look_id = $look_id");
                $looks_res = mysqli_query($db, "SELECT * FROM look");
                $looks_count_res = mysqli_query($db, "SELECT COUNT(*) AS count FROM look");
                $looks_count_row = mysqli_fetch_array($looks_count_res);
                $more_looks_res = mysqli_query($db, "SELECT * FROM look LEFT JOIN look_photo ON look.id = look_photo.look_id AND look_photo.is_main = 1");

                if (isset($_GET['style'])) {
                    $style_id = $_GET["style"];
                    $style_res_as = mysqli_query($db, "SELECT * FROM style WHERE style.look_id = $look_id AND style.id = $style_id");
                } else {
                    $style_res_as = mysqli_query($db, "SELECT * FROM style WHERE style.look_id = $look_id LIMIT 1");
                    
                }
                
                
                $style_row_as = mysqli_fetch_assoc($style_res_as);
                $style_row_as_id = $style_row_as['id'];
                $style_id = $style_row_as['id'];

                // if (isset($_GET["style"])) {
                //     $style_id = $_GET["style"];
                // } else {
                //     $style_id = 1;
                // }

                
                $style_res = mysqli_query($db, "SELECT * FROM style WHERE style.look_id = $look_id AND style.id = $style_id");
                // $style_photo_res = mysqli_query($db, "SELECT * FROM style_photo WHERE style_id = $style_id");
                // $style_count = mysqli_num_rows($style_res);
                
              
             
                $styleArr = Array();
                while ($style_row = mysqli_fetch_assoc($style_res)) {
                    $styleArr = $style_row;
                }
               

                $resultq = mysqli_query($db, "SELECT * FROM style WHERE id < $style_id AND look_id = $look_id ORDER BY id DESC");
                $resultn = mysqli_query($db, "SELECT * FROM style WHERE id > $style_id AND look_id = $look_id ORDER BY id");

                if ($styleArr) {
                    echo "";
                }
                ini_set('display_errors', 0);
                $styleArr['prev'] = mysqli_fetch_assoc($resultq)['id'];
                $styleArr['next'] = mysqli_fetch_assoc($resultn)['id'];
                ini_set('display_errors', 1);

                

                

             
                $style_photo_res_as = mysqli_query($db, "SELECT * FROM style WHERE id = ".$style_row_as['id']);
                $style_photo_row_as = mysqli_fetch_assoc($style_photo_res_as);


                $style_cat_res_arr = mysqli_query($db, "
                
                SELECT * FROM category as cat
                    LEFT JOIN style_category as sc 
                        ON sc.category_id = cat.id 
                    WHERE sc.style_id =".$style_row_as_id);
            ?>
            <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'looks'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Готовые образы</title>
            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/looks.php">Готовые образы</a></li>
                </nav>
                <h1>Готовые образы</h1>
                <!-- [/End Head] -->
                <?php
                    $style_row = mysqli_fetch_array($style_cat_res_arr);
                ?>
                <!-- [MAIN CONTENT] -->
                <main class="readyLooks">
                    <nav class="readyLooks__nav">
                    <?php while ($looks_row = mysqli_fetch_array($looks_res)) { ?>
                    <li><a href="/looks.php?id=<?= $looks_row['id'] ?>" <?php if ($look_id == $looks_row['id']) { ?> class="navItem_active" <?php } ?>><?= $looks_row['name'] ?></a></li>
                    <?php } ?>
                    </nav>
                    
                    <section class="readyLooks__wrapper">
                        <section class="readyLooks__photo">
                            <img src="<?=$pathAdm?><?= $style_photo_row_as['photo'] ?>" class="readyLooks__photo_active" alt="">
                            <div class="readyLooks__chClothes">
                                <?php if (isset($style_row)) {
                                   
                                    ?>
                                    <script>console.log('asdasdasd')</script>
                                    <button class="btn-cat" onclick="cat_style(<?= $style_row['category_id']?>,<?= $style_row_as_id?>)">
                                        <img src="<?=$pathAdm?><?=$style_row['ico']?>" alt="">
                                    </button>
                                    <?php
                                } 
                                while ($style_cat_row_arr = mysqli_fetch_array($style_cat_res_arr)) { 
                                    
                                    ?>
                                    
                                    <button class="btn-cat"  onclick="cat_style(<?= $style_cat_row_arr['category_id']?>,<?= $style_row_as_id?>)">
                                        <img src="<?=$pathAdm?><?=$style_cat_row_arr['ico']?>" alt="">
                                    </button>
                                <?php } ?>
                            </div>
                            <style>
                            #readyLooks__toCart__id:hover {
                                cursor: pointer;
                            }
                            </style>
                            <a class="readyLooks__toCart" data-modal=".modal__addToCart" id="readyLooks__toCart__id">
                                <i class="fas fa-shopping-basket"></i>
                                <span>Весь образ</span>
                            </a>
                        </section>
                        <?php 
                                $look_combined_count_res = mysqli_query($db, "SELECT COUNT(*) AS count FROM look_combined WHERE look_id = $look_id");   
                                $look_combined_count_row = mysqli_fetch_array($look_combined_count_res);  
                            ?>  

                        <section class="readyLooks__useful">
                            <h3>К этому образу<br>также подойдут:</h3>
                            <div class="goodsSlider">
                                    <?php 
                                        $look_combined_count_res = mysqli_query($db, "SELECT COUNT(*) as count FROM look_combined WHERE look_id = $look_id");
                                        $look_combined_count_row = mysqli_fetch_array($look_combined_count_res);

                                        if($look_combined_count_row['count'] > 3) {
                                    ?>
                                        <button class="goodsSlider__prev"></button>
                                    <?php } ?>
  

                                <div class="goodsSlider__wrapper">
  
                                </div>
                                <?php if($look_combined_count_row['count'] > 3) { ?><button class="goodsSlider__next"></button><?php } ?>
                            </div>
                        </section>
                            <?php if (isset($styleArr['prev'])) { ?><a href="/looks.php?id=<?= $look_id?>&style=<?= $styleArr['prev']?>" class="navBtn__prev"></a><?php } ?>
                            <?php if (isset($styleArr['next'])) { ?><a href="/looks.php?id=<?= $look_id?>&style=<?= $styleArr['next']?>" class="navBtn__next"></a><?php } ?>
                    </section>
                    <section class="bottomSlider">
                        <h2>Другие образы</h2>
                        <div class="bottomSlider__wrapper owl-carousel">
                            <?php while ($more_looks_row = mysqli_fetch_array($more_looks_res)) { ?>
                                <a href="/looks.php?id=<?= $more_looks_row['id'] ?>"><img src="<?=$pathAdm?><?= $more_looks_row['photo'] ?>" alt="" style="height: 310px; width: 200px"></a>
                            <?php } ?>
                        </div>
                    </section>
                </main>
                <div class="modalWrapper">
                <section class="modal modal_wide modal__addToCart">
                <h3>Купить весь образ</h3>
                <section class="modal__wrapper">
                <?php

                    $good_id = $_GET['id'];
                    
                    $good_res = mysqli_query($db, "SELECT g.*, gp.photo FROM good as g LEFT JOIN good_photo as gp ON g.id = gp.good_id AND gp.is_main = 1 WHERE g.look_id = $good_id");
                    $good_main_photo_res = mysqli_query($db, "SELECT * FROM good_photo LEFT JOIN good ON good.id=good_photo.good_id WHERE good.look_id = $good_id AND is_main = 1");

                    $good_main_photo_row = mysqli_fetch_assoc($good_main_photo_res);
                    //$good_colors_row = mysqli_fetch_all($good_colors_res, MYSQLI_ASSOC);
                    $good_row['photo'] = $good_main_photo_row;
                    //$good_row['colors'] = $good_colors_row;
                    $i = 0;
                   $list = '[';
                   $a = array();
                    while ($good_row = mysqli_fetch_assoc($good_res)) {
                        $a[] = $good_row;
                ?>
                  
                    <figure class="goodItem" id="good<?=$good_row['id']?>">
                        <div class="goodItem__wrapper">
                            <img src="<?=$pathAdm?><?=$good_row['photo']?>" alt="">
                            <figcaption>
                                <a href="good.php?id=<?=$good_row['id']?>"><?=$good_row['name']?></a>
                                <span>Артикул: <?=$good_row['art']?></span>
                            </figcaption>
                        </div>
                        <section class="goodPage__color">
                            <h4>Цвет:</h4>
                            <?php 
                            $good_colors_res = mysqli_query($db, "SELECT c.*, gc.good_id, good.id FROM good_color as gc  LEFT JOIN good ON gc.good_id=good.id LEFT JOIN color as c ON gc.good_id = good.id AND gc.color_id = c.id WHERE good.look_id = $good_id AND gc.good_id = ".$good_row['id']." AND c.hex IS NOT NULL");
                            while ($good_colors_row = mysqli_fetch_array($good_colors_res)) { ?>
                                <button class="ColorGood<?=$good_row['id']?>" id="color<?= $good_colors_row['0'] ?>" name="<?= $good_colors_row['hex'] ?>" onclick="setColor(`<?= $good_colors_row['hex'] ?>`, `#color` + <?= $good_colors_row['0'] ?>, <?=$good_row['id']?>)" style="background-color: <?= $good_colors_row['hex'] ?>;">
                                </button>
                            <?php } ?>
                        </section>
                        <select name="" class="selectSize<?=$good_row['id']?>" onchange="setSize(<?=$good_row['id']?>)">
                        <?php 
                        $good_sizes_res = mysqli_query($db, "SELECT s.*, gs.good_id FROM good_size as gs LEFT JOIN good ON gs.good_id=good.id LEFT JOIN size as s ON gs.good_id = good.id AND gs.size_id = s.id WHERE s.name AND good.look_id = $good_id AND gs.good_id = ".$good_row['id']." AND gs.good_id = good.id  IS NOT NULL");
                        while ($good_sizes_row = mysqli_fetch_array($good_sizes_res)) { ?>
                                <option value="<?=$good_sizes_row['name']?>"><?=$good_sizes_row['name']?></option>
                        <?php } ?>
                        </select>
                        <div class="itemCounter">
                            <button onclick="decreaseQuantity(<?=$good_row['id']?>)">-</button>
                            <input id="r<?=$good_row['id']?>" type="text" value="1">
                            <button onclick="increaseQuantity(<?=$good_row['id']?>)">+</button>
                        </div>
                        <span class="goodItem__price"><?=$good_row['price']?> руб.</span>
                        <button class="goodItem__close" onclick="removeElement(<?=$good_row['id']?>)">
                            <i class="fas fa-times" aria-hidden="true"></i>
                        </button>
                    </figure>
                  <?php 
                    $list .= '{size: "", color: "", id:"'.$good_row['id'].'",art:"'.$good_row['art'].'",best:"'.$good_row['best'].'",category_id:"'.$good_row['category_id'].'",collection_id:"'.$good_row['collection_id'].'",contanis:"'.$good_row['contains'].'",created_at:"'.$good_row['created_at'].'",description:"'.$good_row['description'].'",in_stock:"'.$good_row['in_stock'].'",look_id:"'.$good_row['look_id'].'",name:"'.$good_row['name'].'",new:"'.$good_row['new'].'",old_price:"'.$good_row['old_price'].'",photo:"'.$good_row['photo'].'",price:"'.$good_row['price'].'",updated_at:"'.$good_row['updated_at'].'", quantity: 1},';
                    $i++; }
                    $list .= ']';
                    ?>
                    <button style="margin-top: 15px;" onclick="putToBasketModal(localStorage.getItem('basketStorage'))" class="goodPage__toCart">Добавить в корзину</button>
                </section>
            </div>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    
    <?php 
        include ('./elements/footer.php');
    ?>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>

    <script>

    for (val of charr) {
        $("#choosColor" + val).css("color", "#ff4625");
        
    }
    
    function cat_style(category_id=<?= $style_row['category_id']?>, style_id=<?= $style_row_as_id?>) {
        document.querySelector('.goodsSlider__wrapper').innerHTML = ''
        let obj = {
            category_id: category_id,
            style_id: style_id,
        }
        console.log(obj)
        $.post("api/style_category_goods.php",  obj).done(function(data) {

            console.log(data)
            data.forEach(el => {
                if (el.photo == null) {
                    el.photo = 'img/box.jpg';
                }
                $('.goodsSlider__wrapper').append(`
                <figure>    
                    <a href="good.php?id=${el.id}">
                        <img src="<?=$pathAdm?>${el.photo}" alt="">
                    </a>
                        <div class="goodsSlider__addBtn">
                            <button id="choosColor${el.id}" onclick='putToChoosen(\`${JSON.stringify(el)}\`)'><i class="far fa-heart"></i></button>
                            <button onclick='putToBasket(\`${JSON.stringify(el)}\`, \`\`, \`\`)'><i class="fas fa-shopping-basket"></i></button>
                        </div>
                </figure>
                `)
            })
            data.forEach((el) => {
                for (val of charr) {
                    if(el.id == val) {
                        $("#choosColor" + el.id).css("color", "#ff4625");
                    }
                }
            })
        })
    }
    cat_style()

    var basket = localStorage.getItem("basket");
    var total = 0;
    var basketArray = { basket: [] };
    var basketStorage = <?=$list?> ;
    localStorage.setItem('basketStorage', JSON.stringify(basketStorage));

    function setColor(color, id, gId) {
        $(`.ColorGood${gId}`).removeClass('active')
        $(`button${id}.ColorGood${gId}`).addClass('active')
        basketStorage.forEach(elem => {
            if (elem.id == gId) {
                elem.color = color
            }
        });
        localStorage.setItem('basketStorage', JSON.stringify(basketStorage));
    }
    
    function setSize(i) {
        basketStorage.forEach(elem => {
            if (elem.id == i) {
                elem.size = $(`.selectSize${i}`).val()
            }
        });
        localStorage.setItem('basketStorage', JSON.stringify(basketStorage))
    }
    

    function removeElement(i) {
        // basketStorage.splice(i, 1); 
        basketStorage = basketStorage.filter(item => item.id != i);
        $("#good" + i).remove();
        localStorage.setItem('basketStorage', JSON.stringify(basketStorage));
    }
    function increaseQuantity(i) {
        basketStorage.forEach(elem => {
            if (elem.id == i) {
                elem.quantity++
                $("#r" + i).val(elem.quantity)
            }
        })
        localStorage.setItem('basketStorage', JSON.stringify(basketStorage));
    }
    function decreaseQuantity(i) {
        
        basketStorage.forEach(elem => {
            if (elem.quantity > 1) {
                if (elem.id == i) {
                    elem.quantity--
                    $("#r" + i).val(elem.quantity)
                }
            }
        })

        localStorage.setItem('basketStorage', JSON.stringify(basketStorage));
    }
    function watchPhoto(id, link) {
        console.log(id)
        $("#bigPhoto").attr("src", link);
        $("button[id^='photo']").removeClass('active');
        $('#photo' + id).addClass('active');
    }

    function putToBasketModal(element) {
        var element = JSON.parse(element);
        element.forEach(elem => {
            putToBasket(JSON.stringify(elem), elem.size, elem.color)
        })
        localStorage.removeItem('basketStorage')
    }

    function putToBasket(element, size, color) {
        var element = JSON.parse(element);
        element.color = color;
        element.size = size;
        console.log(element.size + ' ' + element.color);
        var basket = localStorage.getItem("basket");
        var basketArray = { basket: [] };
        
        if (basket !== null && basket !== '') {
            var basketArray = JSON.parse(basket);
            var index = basketArray.basket.findIndex(el => el.id === element.id && el.color === color && el.size === size);
            console.log(element);
            if (index !== null && index !== -1) {
                console.log('in:' + index);
                basketArray.basket[index].quantity = +basketArray.basket[index].quantity + 1;
                console.log(element.size);
                new Toast({
                    message: 'Товар успешно добавлен в корзину',
                    type: 'danger'
                });
            } else {
                console.log(element.size);
                basketArray.basket.push(element);
                new Toast({
                    message: 'Товар успешно добавлен в корзину',
                    type: 'danger'
                });
            }
        } else {
            console.log(element.size);
            basketArray.basket.push(element);
            new Toast({
                message: 'Товар успешно добавлен в корзину',
                type: 'danger'
            });
        }
        
        localStorage.setItem('basket', JSON.stringify(basketArray));
    }

    
    function putToChoosen(element) {
        var element = JSON.parse(element);
        var choosen = localStorage.getItem("choosen");
        var choosenArray = { choosen: [] };
        
        if (choosen !== null && choosen !== '') {
            var choosenArray = JSON.parse(choosen);
            var index = choosenArray.choosen.findIndex(el => el.id === element.id);
            if (index === null || index === -1) {
                $("#choosColor" + element.id).css("color", "#ff4625");
                console.log(element.id);
                choosenArray.choosen.push(element);
                new Toast({
                    message: 'Товар успешно добавлен в избранное',
                    type: 'danger'
                });
            } else {
                $("#choosColor" + element.id).css("color", "rgb(73, 73, 73)");
                choosenArray.choosen.splice(index, 1); 
                new Toast({
                    message: 'Товар удален из избранного',
                    type: 'danger'
                });
            }
        } else {
            console.log(element.id);
            $("#choosColor" + element.id).css("color", "#ff4625");
            choosenArray.choosen.push(element);
            new Toast({
                    message: 'Товар успешно добавлен в избранное',
                    type: 'danger'
            });
        }
        
        localStorage.setItem('choosen', JSON.stringify(choosenArray));
    }
    /* == [OWL SLIDER] == */
    $('.bottomSlider__wrapper').owlCarousel({
        margin: 14,
        items: 4,
        loop: true,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false
    });
    </script>


    <!-- [/SCRIPTS] -->
</body>
</html>
