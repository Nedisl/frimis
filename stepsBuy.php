<?php 
    include ('./elements/header.php');
?>

        <section class="contentWrapper">
        <?php 
            include ('./elements/sidebar.php');
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'stepsBuy'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Как заказать</title>


            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="/">Главная</a></li>
                    <li><a href="#">Покупателям</a></li>
                    <li><a href="stepsBuy.php">Как заказать</a></li>
                </nav>
                <h1>Как заказать</h1>
                <!-- [/End Head] -->

                <!-- [MAIN STEPS] -->
                <main class="stepsBuy">
                    <section class="stepsBuy__wrapper">
                        <figure>
                            <img src="img/stepsBuy1.jpg" alt="">
                            <figcaption>
                                <h3>Положите выбранный товар в корзину</h3>
                                <span>Выберите в каталоге интересующие вас товары и перейдите в раздел «Корзина»</span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="img/stepsBuy2.jpg" alt="">
                            <figcaption>
                                <h3>Введите адрес</h3>
                                <span>Заполните форму, расположенную под списком выбранных товаров.<br>Все поля являются обязательными </span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="img/stepsBuy3.jpg" alt="">
                            <figcaption>
                                <h3>Выберите способ<br>доставки и оплаты</h3>
                                <span>Выберите способ доставки<br> и оплаты из выпадающих списков</span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="img/stepsBuy4.jpg" alt="">
                            <figcaption>
                                <h3>Отправьте заказ</h3>
                                <span>Если все заполнено верно, нажмите кнопку «Отправить заказ».<br>Наши менеджеры свяжутся с вами, если вы не отказались от звонка</span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="img/stepsBuy5.jpg" alt="">
                            <figcaption>
                                <h3>Оплатите</h3>
                                <span>Ожидайте счет на указанный<br>вами электронный адрес<br> и оплатите его </span>
                            </figcaption>
                        </figure>
                        <figure>
                            <img src="img/stepsBuy6.jpg" alt="">
                            <figcaption>
                                <h3>Получите ваш заказ</h3>
                                <span>Получите ваш заказ!<br>Мы будем очень рады вашему <a href="#">отзыву</a></span>
                            </figcaption>
                        </figure>
                    </section>
                    <button class="stepsBuy__question" data-modal=".modal__question">У меня есть вопрос</button>
                </main>
                <!-- [/END STEPS] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    <div class="modalWrapper">
        <section class="modal modal_medium modal__question">
            <h3>Задать вопрос</h3>
            <form onsubmit="return false;">
                <div class="form-box">
                    <input type="text" placeholder="Ваше имя" id="modal-name">
                    <input type="text" placeholder="Ваш телефон | e-mail" id="modal-email">
                    <input type="text" placeholder="Ваш город" id="modal-city">
                    <input type="text" placeholder="Ваше сообщение" id="modal-message">
                </div>
                <div class="form-box">
                    <!-- <button class="sendFile">Выбрать файл</button>
                    <input type="file" id="modal-file"> -->
                    <button class="buttonSubscribe" onclick="questionRequest()" style="margin: 0;">Отправить</button>
                </div>
                <span class="uText">Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных и ознакомлен(а) с условиями конфиденциальности.</span>
            </form>
        </section>
    </div>
    <?php 
        include ('./elements/footer.php');
    ?>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
          function questionRequest() {
            var questionData = {};
                questionData.modalName = $('#modal-name').val().trim();
                questionData.modalEmail = $('#modal-email').val().trim();
                questionData.modalCity = $('#modal-city').val().trim();
                questionData.modalMessage = $('#modal-message').val().trim();
                let f = false;


                if (questionData.modalName == false && questionData.modalEmail == false && questionData.modalCity == false && questionData.modalMessage == false) {
                    f = 'Не все поля заполнены';
                } else if (questionData.modalEmail != +questionData.modalEmail || questionData.modalEmail == false) {
                    if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(questionData.modalEmail) == false) {
                        f = 'Не корректный email';
                    }
                } else if (questionData.modalEmail == +questionData.modalEmail) {
                    if (questionData.modalEmail.length != 11) {
                        f = 'Не корректный номер телефона';
                    }
                }
                if (f) {
                    new Toast({
                        message: f,
                        type: 'danger'
                    });
                } else {
                    $.post("/api/application.php", questionData).done(function(data) {
                        if (data.status == 1) {
                            console.log(data);
                            new Toast({
                                message: 'Сообщение отправленно!',
                                type: 'danger'
                            });
                            $('#modal-name').val('');
                            $('#modal-email').val('');
                            $('#modal-city').val('');
                            $('#modal-message').val('');
                        } else {
                            new Toast({
                                message: 'Не удалось отправить сообщение!',
                                type: 'danger'
                            });
                        }
                    });
                }
        }
    </script>
    <!-- [/SCRIPTS] -->
</body>
</html>