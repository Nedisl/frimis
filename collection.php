<?php 
include ('elements/header.php');
?>
        <section class="contentWrapper">
        <?php 
            include ('./elements/sidebar.php');
            $cf = " WHERE g.id > 0 ";
            if (isset($_GET["id"])) {
                $collection_id = $_GET["id"];
                if ($collection_id == "all") {
                    $cf = " WHERE g.id > 0 ";
                } else {
                    $cf = " WHERE g.collection_id = $collection_id ";
                }
            } else {
                $collection_id = "all";
            }

            $search = "";
            $query = 0;
            $sale = 0;
            $new = 0;
            $best = 0;
            $color = 0;
            $look = 0;
            $qlook = "";
            $qcolor = "";
            $col_q = "";
            if (isset($_GET["look"]) && $_GET["look"] != '0') {
                $look = $_GET["look"];
                $qlook =  "AND g.look_id = $look ";
            } 

            if (isset($_GET["color"])  && $_GET["color"] != '0') {
                $color = $_GET["color"];
                $qcolor =  " AND gc.color_id IS NOT NULL ";
                $col_q = " LEFT JOIN good_color as gc ON g.id = gc.good_id AND gc.color_id = ".$color." ";
            } 

            if (isset($_GET["search"])  && $_GET["search"] != '') {
                $query = $_GET["search"];
                $search = " AND g.name LIKE '%$query%' ";
            }
            $q = "";

            if (isset($_GET["sale"])  && $_GET["sale"] != '0') {
                $sale = 1;
                $new = 0;
                $best = 0;
                $q =  "AND g.is_on_sale = $sale ";
            } 

            if (isset($_GET["new"])  && $_GET["new"] != '0') {
                $sale = 0;
                $new = 1;
                $best = 0;
                $q =  "AND g.new = $new ";
            }

            if (isset($_GET["best"]) && $_GET["best"] != '0') {
                $sale = 0;
                $new = 0;
                $best = 1;
                $q =  "AND g.best = $best ";
            }

            $good_colors_res = mysqli_query($db, "SELECT * FROM color");
            $look_res = mysqli_query($db, "SELECT * FROM look");
            if ($collection_id != "all") {
                $collection_res = mysqli_query($db, "SELECT * FROM collection WHERE id = $collection_id");
                $collection_row = mysqli_fetch_array($collection_res);
            }
            $good_res = mysqli_query($db, "SELECT COUNT(*) AS count FROM good AS g LEFT JOIN good_photo AS gp ON g.id = gp.good_id AND gp.is_main = 1 ".$col_q.$cf.$q.$search.$qcolor.$qlook);
            $good_count_row = mysqli_fetch_array($good_res);
            $total_pages = 1;
            if ((int)$good_count_row['count'] % 12 > 0) {
                $total_pages = floor($good_count_row['count'] / 12) + 1;
            } else {
                $total_pages = floor($good_count_row['count'] / 12);
            }
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'collection'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title><?php if ($collection_id != "all") { echo $collection_row['season']." ".$collection_row['year']; } ?></title>


            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="/">Главная</a></li>
                    <?php if ($collection_id != "all") { ?><li><a href="/collection.php?id=<?= $collection_id ?>"><?= $collection_row['season'] ?> <?= $collection_row['year'] ?></a></li><?php } ?>
                </nav>
                <h1><?php if ($collection_id != "all") { echo $collection_row['season']." ".$collection_row['year']; } ?></h1>
                <!-- [/End Head] -->

                <!-- [MAIN CONTENT] -->
                <main class="catalogPage">
                    <!-- [FILTER PANEL] -->
                    <menu class="catalogPage__menu">
                        <section class="catalogPage__chLook">
                            <span>Стиль:</span>
                            <select id="ch_look" name="chooseLook" class="chLook__list">
                            <option value="0">любой</option>
                                <?php while ($look_row = mysqli_fetch_array($look_res)) { ?>
                                    <option value="<?= $look_row['id'] ?>"><?= $look_row['name'] ?></option>
                                <?php } ?>
                            </select>
                        </section>
                        <section class="catalogPage__chColor">
                            <span>Цвет:</span>
                            <select id="ch_color" name="chooseLook" class="chLook__list">
                                <option value="0">любой</option>
                                <?php while ($color_row = mysqli_fetch_array($good_colors_res)) { ?>
                                    <option value="<?= $color_row['id'] ?>"><?= $color_row['color_name'] ?></option>
                                <?php } ?>
                                
                            </select>
                        </section>
                    </menu>
                    <!-- [/END FILTER] -->

                    <!-- [CATALOG] -->
                    <section class="catalogPage__wrapper">
                        
                    </section>
                    <!-- [/END CATALOG] -->

                    <button class="showMore" onclick="loadMoreGoods()">Показать ещё</button>

                    <!-- [PAGE NAV] -->
                    <menu class="catalogPage__pageNav">
                        <button class="navBtn__prev" onclick="loadPage(--currentPage)"></button>
                        <?php for ($i = 1; $i <= $total_pages; $i++) { ?>
                            <li><button class="pagin" id="page<?= $i ?>" onclick="loadPage(<?= $i ?>)"><?= $i ?></button></li>
                        <?php } ?>
                        <button class="navBtn__next" onclick="loadPage(++currentPage)"></button>
                    </menu>
                    <!-- [/END PAGE NAV] -->

                    <section class="bottomSlider">
                        <h2>Другие образы</h2>
                        
                        <div class="bottomSlider__wrapper owl-carousel">
                            <?php
                                 $look_res = mysqli_query($db, "SELECT l.*, lp.photo FROM look as l LEFT JOIN look_photo as lp ON l.id = lp.look_id AND lp.is_main = 1");
                                 while ($look_row = mysqli_fetch_array($look_res)) {
                            ?>
                                <a href="looks.php?id=<?= $look_row['id'] ?>"><img src="<?=$pathAdm?><?= $look_row['photo'] ?>" alt="" height="300px"></a>
                            <?php } ?>
                        </div>  
                    </section>
                </main>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    <?php include('elements/footer.php'); ?>
  

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
    currentPage = 1;
    var color = <?= $color ?>;
    var look = <?= $look ?>;
    $("#ch_look").val(look);
    $("#ch_color").val(color);
    nextUrl = '/api/collection.php?id=<?= $collection_id ?>&limit=12&offset=0&sale=<?= $sale ?>&new=<?= $new ?>&best=<?= $best ?>&search=<?= $query ?>&look=' + look + '&color=' + color;
    $(".navBtn__prev").hide();
    loadMoreGoods();
    function loadMoreGoods() {
            $.get(nextUrl, (res) => {
                console.log(nextUrl);
                if (res.data.length < 12) {
                    $('.showMore').hide();
                } else {
                    $('.showMore').show();
                }
                console.log(res);
                res.data.forEach((el) => {
                    console.log(el);
                    if (el.photo == null) {
                        el.photo = 'img/box.jpg';
                    }
                    $('.catalogPage__wrapper').append(`<figure>
                    <a href="good.php?id=` + el.id + `">
                    <img src="<?=$pathAdm?>` + el.photo + `" alt="">
                    </a>
                    <div class="goodPanel">
                    <button onclick='putToChoosen(\`${JSON.stringify(el)}\`)'><i class="far fa-heart"></i></button>
                    </div>
                    <figcaption>
                    <div class="goodTitle">
                    <a href="good.php?id=` + el.id + `">` + el.name + `</a>
                    <span>` + el.price + ` руб.</span>
                    </div>
                    <button onclick='putToBasket(\`${JSON.stringify(el)}\`, \`\`, \`\`)'><i class="fas fa-shopping-basket"></i></button>
                    </figcaption>
                    </figure>`);
                });
                $(".pagin").removeClass("active");
                $("#page" + res.page).addClass("active");
                currentPage = res.page;
                if (+currentPage > 1) {
                    $(".navBtn__prev").show();
                } else {
                    $(".navBtn__prev").hide();
                }
                if (+currentPage < +<?= $total_pages ?>) {
                    $(".navBtn__next").show();
                } else {
                    $(".navBtn__next").hide();
                }
                nextUrl = res.next_url;
                console.log(nextUrl);
            });
    }

    function colorSelect(c) {
        console.log(c);
    }
    function putToBasket(element, size, color) {
        var element = JSON.parse(element);
        element.quantity = 1;
        element.color = color;
        element.size = size;
        console.log(element.size + ' ' + element.color);
        var basket = localStorage.getItem("basket");
        var basketArray = { basket: [] };
        
        if (basket !== null && basket !== '') {
            var basketArray = JSON.parse(basket);
            var index = basketArray.basket.findIndex(el => el.id === element.id && el.color === color && el.size === size);
            console.log(element);
            if (index !== null && index !== -1) {
                console.log('in:' + index);
                basketArray.basket[index].quantity = +basketArray.basket[index].quantity + 1;
                console.log(element.size);
                new Toast({
                    message: 'Товар успешно добавлен в корзину',
                    type: 'danger'
                });
            } else {
                console.log(element.size);
                basketArray.basket.push(element);
                new Toast({
                    message: 'Товар успешно добавлен в корзину',
                    type: 'danger'
                });
            }
        } else {
            console.log(element.size);
            basketArray.basket.push(element);
            new Toast({
                message: 'Товар успешно добавлен в корзину',
                type: 'danger'
            });
        }
        
        localStorage.setItem('basket', JSON.stringify(basketArray));
    }
    
    function putToChoosen(element) {
        var element = JSON.parse(element);
        var choosen = localStorage.getItem("choosen");
        var choosenArray = { choosen: [] };
        
        if (choosen !== null && choosen !== '') {
            var choosenArray = JSON.parse(choosen);
            var index = choosenArray.choosen.findIndex(el => el.id === element.id);
            if (index === null || index === -1) {
                choosenArray.choosen.push(element);
                new Toast({
                    message: 'Товар успешно добавлен в избранное',
                    type: 'danger'
                });
            } else {
                choosenArray.choosen.splice(index, 1); 
                new Toast({
                    message: 'Товар удален из избранного',
                    type: 'danger'
                });
            }
        } else {
            choosenArray.choosen.push(element);
            new Toast({
                    message: 'Товар успешно добавлен в избранное',
                    type: 'danger'
            });
        }
        
        localStorage.setItem('choosen', JSON.stringify(choosenArray));
    }

    function loadPage(page) {
            nextUrl = '/api/collection.php?id=<?= $collection_id ?>&limit=12&page=' + page + '&sale=<?= $sale ?>&new=<?= $new ?>&best=<?= $best ?>&search=<?= $query ?>&look=' + look + '&color=' + color;
            $('.catalogPage__wrapper').html('');
            loadMoreGoods();
    }
    /* == [SELECT UI] == */
    $('#ch_color').selectmenu({
        change: function (event, ui) {
            color = $("#ch_color option:selected").val();
            if ( <?=$sale?> != 0) {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color +'&sale=' + <?=$sale?>;
            } else if (<?=$new?> != 0) {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color +'&new=' + <?=$new?>;
            } else if (<?=$best?> != 0) {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color +'&best=' + <?=$best?>;
            } else {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color;
            }
            
        }
    });

    $('#ch_look').selectmenu({
        change: function (event, ui) {
            look = $("#ch_look option:selected").val();
            if ( <?=$sale?> != 0) {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color +'&sale=' + <?=$sale?>;
            } else if (<?=$new?> != 0) {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color +'&new=' + <?=$new?>;
            } else if (<?=$best?> != 0) {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color +'&best=' + <?=$best?>;
            } else {
                window.location.href = '/collection.php?id=<?= $collection_id ?>&look=' + look + '&color=' + color;
            }
        }
    });


    $('.chLook__list').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectMenu_closed',
            'ui-selectmenu-button-open': 'selectMenu_open',
            'ui-selectmenu-menu': 'selectMenu__menu'
        }
    });

    
    /* == [OWL SLIDER] == */
    $('.bottomSlider__wrapper').owlCarousel({
        margin: 14,
        items: 4,
        loop: true,
        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,
        nav: true,
        dots: false
    });
    </script>


    <!-- [/SCRIPTS] -->
</body>
</html>