-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: onoartsr.beget.tech    Database: onoartsr_frimis
-- ------------------------------------------------------
-- Server version	5.7.21-20-beget-5.7.21-20-1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `message` text,
  `type` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES (1,'Ilkara','qwe','franchise','karail-fokus@mail.ru',NULL,'2019-11-17 16:31:42','2019-11-17 16:31:42'),(2,'Ilkara','qwe','question','123','Киров','2019-11-17 16:32:01','2019-11-17 16:32:01'),(3,'qwe','qwe','franchise','qwe',NULL,'2019-11-18 09:38:00','2019-11-18 09:38:00'),(4,'qwe','qwe','question','qwe','qwe','2019-11-18 09:40:42','2019-11-18 09:40:42'),(5,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:00:13','2019-11-19 11:00:13'),(6,'qwe','qwe','question','qwe','qwe','2019-11-19 11:00:21','2019-11-19 11:00:21'),(7,'qwe','qwe','question','qwe','qwe','2019-11-19 11:03:18','2019-11-19 11:03:18'),(8,'qwe','qwe','question','qwe','qwe','2019-11-19 11:03:20','2019-11-19 11:03:20'),(9,'qwe','qwe','question','qwe3','qwe','2019-11-19 11:03:50','2019-11-19 11:03:50'),(10,'qwe','qwe','question','qwe3','qwe','2019-11-19 11:08:23','2019-11-19 11:08:23'),(11,'qwe','qwe','question','qwe','qwe','2019-11-19 11:10:42','2019-11-19 11:10:42'),(12,'qwe','qwe','question','qwe','qwe','2019-11-19 11:10:49','2019-11-19 11:10:49'),(13,'qwe','qwe','question','qwe3','qwe','2019-11-19 11:10:54','2019-11-19 11:10:54'),(14,'qwe','qwe','question','qwe3qwe','qwe','2019-11-19 11:11:04','2019-11-19 11:11:04'),(15,'qwe','qwe','question','qwe','qwe','2019-11-19 11:11:13','2019-11-19 11:11:13'),(16,'qwe','qwe','question','qwe','qwe','2019-11-19 11:11:21','2019-11-19 11:11:21'),(17,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:14:41','2019-11-19 11:14:41'),(18,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:18:02','2019-11-19 11:18:02'),(19,'qwe','qwe','question','qwe3','qwe','2019-11-19 11:19:18','2019-11-19 11:19:18'),(20,'qwe','qwe','question','qwe','qwe','2019-11-19 11:19:52','2019-11-19 11:19:52'),(21,'qwe','qwe','question','qwe','qwe','2019-11-19 11:19:57','2019-11-19 11:19:57'),(22,'qwe','qwe','question','qwe','qwe','2019-11-19 11:20:12','2019-11-19 11:20:12'),(23,'qwe','qwe','question','qwe3qwe','qwe','2019-11-19 11:20:21','2019-11-19 11:20:21'),(24,'qwe','qe','question','qwe3qwe','qwe','2019-11-19 11:24:44','2019-11-19 11:24:44'),(25,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:25:25','2019-11-19 11:25:25'),(26,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:25:30','2019-11-19 11:25:30'),(27,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:26:28','2019-11-19 11:26:28'),(28,'qwe','qwe','question','qwe3qwe','qwe','2019-11-19 11:26:35','2019-11-19 11:26:35'),(29,'qwe','qwe','question','qwe','qwe','2019-11-19 11:27:17','2019-11-19 11:27:17'),(30,'qwe','qwe','question','qwe','qwe','2019-11-19 11:27:34','2019-11-19 11:27:34'),(31,'qwe','qwe','question','qwe','qwe','2019-11-19 11:34:22','2019-11-19 11:34:22'),(32,'qwe','qwe','question','qwe3qwe','qwe','2019-11-19 11:35:10','2019-11-19 11:35:10'),(33,'qwe','qwe','question','qwe3qwe','qew','2019-11-19 11:35:18','2019-11-19 11:35:18'),(34,'qwe','qwe','franchise','qwe',NULL,'2019-11-19 11:38:01','2019-11-19 11:38:01'),(35,'dsfdsf','fsdfsdfdsf','franchise','dsfsdf',NULL,'2019-11-19 11:40:38','2019-11-19 11:40:38'),(36,'qwe','qe','question','qwe3qwe','s','2019-11-19 11:59:20','2019-11-19 11:59:20'),(37,'qwe','qwe','question','qwe','qwe','2019-11-20 08:18:46','2019-11-20 08:18:46'),(38,'qwe','qwe','question','qwe','qwe','2019-11-20 08:21:38','2019-11-20 08:21:38'),(39,'qwe','qwe','question','qwe','qwe','2019-11-20 08:22:10','2019-11-20 08:22:10'),(40,'qw','w','question','w','e','2019-11-20 08:22:26','2019-11-20 08:22:26'),(41,'qwe','qwe','question','w','w','2019-11-20 08:24:00','2019-11-20 08:24:00'),(42,'qwe','qwe','question','w','qwe','2019-11-20 08:25:50','2019-11-20 08:25:50'),(43,'qwe','qwe','question','qwe','qwe','2019-11-20 08:26:36','2019-11-20 08:26:36'),(44,'qwe','qwe','question','qwe','qwe','2019-11-20 08:26:52','2019-11-20 08:26:52'),(45,'qwe','qwe','franchise','karail-fokus@mail.ru',NULL,'2019-11-20 08:42:17','2019-11-20 08:42:17'),(46,'йцу','йцу','franchise','karail-fokus@mail.ru',NULL,'2019-11-20 08:43:28','2019-11-20 08:43:28'),(47,'йцв','йцв','question','123','йцв','2019-11-20 08:43:38','2019-11-20 08:43:38'),(48,'qwe','qwe','franchise','karail-fokus@mail.ru',NULL,'2019-11-20 09:35:17','2019-11-20 09:35:17'),(49,'qwe','qwe','question','89536918617','qwe','2019-11-20 09:40:20','2019-11-20 09:40:20'),(50,'парпар','папрапр','question','апр','парапр','2019-11-20 12:59:44','2019-11-20 12:59:44'),(51,'орнп','оепам','question','орнбпми','нп','2019-11-20 13:04:04','2019-11-20 13:04:04'),(52,'qwe','qwe','question','karail-fokus@mail.ru','qwe','2019-11-21 15:34:03','2019-11-21 15:34:03');
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_on_main_page` tinyint(1) DEFAULT NULL,
  `is_full_on_sidebar` tinyint(1) DEFAULT NULL,
  `price_start` decimal(10,0) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Перчатки','uploads/autumn1.jpg',1,0,500,'2019-11-05 18:56:34','2019-11-05 18:56:37'),(2,'Шарфы и платки','uploads/autumn2.jpg',1,1,300,'2019-11-05 18:57:49','2019-11-05 18:57:51'),(3,'Солнцезащитные платки','uploads/autumn3.jpg',1,0,200,'2019-11-05 18:58:17','2019-11-05 18:58:20'),(4,'Солнцезащитные перчатки','uploads/autumn4.jpg',1,0,100,'2019-11-05 18:58:17','2019-11-05 18:58:20'),(5,'Варежки','category/November2019/0lPtIyad6GDuNwFcoKPM.jpg',0,1,2000,'2019-11-16 13:33:35','2019-11-16 13:33:35');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Kirov','2019-05-19 14:39:56','2019-07-19 14:40:02'),(2,'Moscow','2019-09-19 14:40:23','2019-12-19 14:40:28'),(3,'Kirov2','2019-06-19 14:41:18','2019-08-19 14:41:21'),(4,'Moscow2','2019-06-19 14:41:37','2019-07-19 14:41:39');
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collection`
--

DROP TABLE IF EXISTS `collection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) DEFAULT NULL,
  `season` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_main` tinyint(1) DEFAULT NULL,
  `call_to_action` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collection`
--

LOCK TABLES `collection` WRITE;
/*!40000 ALTER TABLE `collection` DISABLE KEYS */;
INSERT INTO `collection` VALUES (1,2010,'Осень',1,'СОБЕРИ ОСЕННИЙ ОБРАЗ','uploads/autumnGirl.jpg','НОВАЯ КОЛЛЕКЦИЯ','uploads/slider1.jpg','2019-11-05 19:00:59','2019-11-05 19:01:02'),(2,2019,'Зима',2,'СОБЕРИ ЗИМНИЙ ОБРАЗ','uploads/autumnGirl.jpg','НОВАЯ КОЛЛЕКЦИЯ','uploads/slider1.jpg',NULL,NULL);
/*!40000 ALTER TABLE `collection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `color`
--

DROP TABLE IF EXISTS `color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hex` varchar(255) DEFAULT NULL,
  `color_name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'#000000','Чёрный','2019-11-16 14:41:30','2019-11-16 14:41:33'),(2,'#FF0000','Красный','2019-11-16 14:42:05','2019-11-16 14:42:07'),(3,'#00FF00','Зелёный','2019-11-16 14:42:19','2019-11-16 14:42:21');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),(22,4,'id','text','Id',1,0,0,0,0,0,'{}',1),(23,4,'name','text','Имя',0,1,1,0,0,0,'{}',2),(24,4,'message','text','Сообщение',0,1,1,0,0,0,'{}',3),(25,4,'type','text','Тип',0,1,1,0,0,0,'{}',4),(26,4,'contact','text','Контактные данные',0,1,1,0,0,0,'{}',5),(27,4,'city','text','Город',0,1,1,0,0,0,'{}',6),(28,4,'created_at','timestamp','Добавлено',0,1,1,0,0,0,'{}',7),(29,4,'updated_at','text','Updated At',0,0,0,0,0,0,'{}',8),(30,5,'id','text','Id',1,0,0,0,0,0,'{}',1),(31,5,'hex','color','Цвет',0,1,1,1,1,1,'{}',2),(32,5,'color_name','text','Имя цвета',0,1,1,1,1,1,'{}',3),(33,5,'created_at','timestamp','Создан',0,1,1,0,0,0,'{}',4),(34,5,'updated_at','text','Обновлен',0,0,0,0,0,0,'{}',5),(35,6,'id','text','Id',1,0,0,0,0,0,'{}',1),(36,6,'name','media_picker','Размер',0,1,1,1,1,1,'{}',2),(37,6,'created_at','text','Created At',0,0,0,0,0,0,'{}',3),(38,6,'updated_at','text','Updated At',0,0,0,0,0,0,'{}',4),(39,7,'id','text','Id',1,0,0,0,0,0,'{}',1),(40,7,'name','text','Название',0,1,1,1,1,1,'{}',2),(41,7,'image','image','Изображение',0,1,1,1,1,1,'{}',3),(42,7,'is_on_main_page','checkbox','Разместить на главной? (не более 4х!)',0,1,1,1,1,1,'{\"on\":\"\\u041e\\u0442\\u043e\\u0431\\u0440\\u0430\\u0436\\u0430\\u0442\\u044c\",\"off\":\"\\u041d\\u0435 \\u043e\\u0442\\u043e\\u0431\\u0440\\u0430\\u0436\\u0430\\u0442\\u044c\",\"checked\":\"true\"}',4),(43,7,'is_full_on_sidebar','checkbox','Отображать все товары категории в сайдбаре',0,1,1,1,1,1,'{\"on\":\"\\u041e\\u0442\\u043e\\u0431\\u0440\\u0430\\u0436\\u0430\\u0442\\u044c\",\"off\":\"\\u041d\\u0435 \\u043e\\u0442\\u043e\\u0431\\u0440\\u0430\\u0436\\u0430\\u0442\\u044c\",\"checked\":\"true\"}',5),(44,7,'price_start','number','Стартовая цена',0,1,1,1,1,1,'{}',6),(45,7,'created_at','text','Created At',0,0,0,0,0,0,'{}',7),(46,7,'updated_at','text','Updated At',0,0,0,0,0,0,'{}',8),(47,8,'id','text','Id',1,0,0,0,0,0,'{}',1),(48,8,'name','text','Название',0,1,1,1,1,1,'{}',4),(49,8,'description','text_area','Описание',0,1,1,1,1,1,'{}',5),(50,8,'price','number','Цена',0,1,1,1,1,1,'{}',6),(51,8,'old_price','number','Старая цена',0,1,1,1,1,1,'{}',7),(52,8,'contains','text_area','Содержит',0,1,1,1,1,1,'{}',8),(53,8,'is_on_top','text','Is On Top',0,0,0,0,0,0,'{}',9),(54,8,'art','text','Артикул',0,1,1,1,1,1,'{}',10),(55,8,'look_id','text','Look Id',0,0,0,0,0,0,'{}',11),(56,8,'collection_id','text','Collection Id',0,0,0,0,0,0,'{}',2),(57,8,'category_id','text','Category Id',0,0,0,0,0,0,'{}',3),(58,8,'is_on_sale','checkbox','Распродажа',0,1,1,1,1,1,'{\"on\":\"\\u0414\\u0430\",\"off\":\"\\u041d\\u0435\\u0442\",\"checked\":\"false\"}',12),(59,8,'best','checkbox','Лучшее',0,1,1,1,1,1,'{\"on\":\"\\u0414\\u0430\",\"off\":\"\\u041d\\u0435\\u0442\",\"checked\":\"false\"}',13),(60,8,'new','checkbox','Новое',0,1,1,1,1,1,'{\"on\":\"\\u0414\\u0430\",\"off\":\"\\u041d\\u0435\\u0442\",\"checked\":\"false\"}',14),(61,8,'in_stock','number','На складе',0,1,1,1,1,1,'{}',15),(62,8,'created_at','text','Created At',0,0,0,0,0,0,'{}',17),(63,8,'updated_at','text','Updated At',0,0,0,0,0,0,'{}',18),(64,10,'id','text','Id',1,0,0,0,0,0,'{}',1),(65,10,'good_id','text','Good Id',0,0,0,0,0,0,'{}',2),(66,10,'photo','image','Фото',0,1,1,1,1,1,'{}',3),(67,10,'is_main','checkbox','Основное фото',0,1,1,1,1,1,'{\"on\":\"\\u0414\\u0430\",\"off\":\"\\u041d\\u0435\\u0442\",\"checked\":\"false\"}',4),(68,10,'created_at','text','Created At',0,0,0,0,0,0,'{}',6),(69,10,'updated_at','text','Updated At',0,0,0,0,0,0,'{}',7),(70,10,'good_photo_hasone_good_relationship','relationship','good',0,1,1,1,1,1,'{\"model\":\"App\\\\Good\",\"table\":\"good\",\"type\":\"belongsTo\",\"column\":\"good_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"application\",\"pivot\":\"0\",\"taggable\":\"0\"}',5),(71,8,'good_hasmany_good_photo_relationship','relationship','Фото',0,1,1,1,1,1,'{\"model\":\"App\\\\GoodPhoto\",\"table\":\"good_photo\",\"type\":\"hasMany\",\"column\":\"good_id\",\"key\":\"id\",\"label\":\"photo\",\"pivot_table\":\"application\",\"pivot\":\"0\",\"taggable\":\"0\"}',16);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','TCG\\Voyager\\Http\\Controllers\\VoyagerUserController','',1,0,NULL,'2019-11-16 10:11:13','2019-11-16 10:11:13'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2019-11-16 10:11:13','2019-11-16 10:11:13'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2019-11-16 10:11:14','2019-11-16 10:11:14'),(4,'application','application','Заявки и вопросы','Заявки и вопросы',NULL,'App\\Application',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-11-16 10:19:22','2019-11-16 10:19:22'),(5,'color','color','Цвет','Цвета',NULL,'App\\Color',NULL,NULL,'Создавайте цвета для ваших товаров',1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-11-16 10:23:19','2019-11-16 10:23:19'),(6,'size','size','Размер','Размеры',NULL,'App\\Size',NULL,NULL,'Добавляйте размеры для ваших товаров',1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}','2019-11-16 10:25:40','2019-11-16 10:25:40'),(7,'category','category','Категория','Категории',NULL,'App\\Category',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-11-16 10:29:57','2019-12-05 10:42:37'),(8,'good','good','Товар','Товары',NULL,'App\\Good',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-11-16 10:41:44','2019-11-16 10:54:16'),(10,'good_photo','good-photo','Фотография товара','Фотографии товаров',NULL,'App\\GoodPhoto',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}','2019-11-16 10:45:17','2019-11-16 10:50:06');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `good`
--

DROP TABLE IF EXISTS `good`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `price` decimal(10,0) DEFAULT NULL,
  `old_price` decimal(10,0) DEFAULT NULL,
  `contains` text COLLATE utf8_unicode_ci,
  `is_on_top` tinyint(1) DEFAULT NULL,
  `art` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `look_id` int(11) DEFAULT NULL,
  `collection_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `is_on_sale` tinyint(1) DEFAULT '0',
  `best` tinyint(1) DEFAULT '0',
  `new` tinyint(1) DEFAULT '0',
  `in_stock` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `good_category_id_fk` (`category_id`),
  KEY `good_collection_id_fk` (`collection_id`),
  CONSTRAINT `good_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `good_collection_id_fk` FOREIGN KEY (`collection_id`) REFERENCES `collection` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `good`
--

LOCK TABLES `good` WRITE;
/*!40000 ALTER TABLE `good` DISABLE KEYS */;
INSERT INTO `good` VALUES (1,'Шарф','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',999,0,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1,'111111',2,1,2,1,1,1,0,'2019-11-05 19:01:16','2019-11-05 19:01:18'),(2,'Шапка','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,1,2,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(3,'Сланцы','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,2,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(4,'Шерстяные рукавицы','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,2,5,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(5,'dsf','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(6,'defdsfsdf','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,4,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(7,'m,.','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(8,'dfgdfg','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,4,1,1,1,0,1,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(9,',m.m,.','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(10,'rytrtyrty','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,2,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(11,'ewewewewe','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,2,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(12,'jkjkjkjk','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(13,'qwe','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(14,'jkjk','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(15,'88','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(16,'d','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(17,'gf','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,1,1,1,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(18,'12','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(19,'f','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(20,'g','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,1,0,1,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(21,'h','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(22,'jf','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(23,'hj','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,1,1,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(24,'444','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(25,'sdfsdfsdfs444','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56'),(26,'555','Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',1000,1999,'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolore, suscipit, similique',NULL,NULL,NULL,1,1,0,0,0,0,'2019-11-06 17:09:54','2019-11-06 17:09:56');
/*!40000 ALTER TABLE `good` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `good_color`
--

DROP TABLE IF EXISTS `good_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `good_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `good_color_good_id_fk` (`good_id`),
  KEY `good_color_color_id_fk` (`color_id`),
  CONSTRAINT `good_color_color_id_fk` FOREIGN KEY (`color_id`) REFERENCES `color` (`id`),
  CONSTRAINT `good_color_good_id_fk` FOREIGN KEY (`good_id`) REFERENCES `good` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `good_color`
--

LOCK TABLES `good_color` WRITE;
/*!40000 ALTER TABLE `good_color` DISABLE KEYS */;
INSERT INTO `good_color` VALUES (1,1,1,'2019-11-05 19:01:53','2019-11-05 19:01:55'),(2,1,2,'2019-11-06 16:55:48','2019-11-06 16:55:50'),(3,2,1,'2019-11-08 15:38:04','2019-11-08 15:38:06'),(4,2,3,'2019-11-12 14:41:22','2019-11-12 14:41:19');
/*!40000 ALTER TABLE `good_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `good_combined`
--

DROP TABLE IF EXISTS `good_combined`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `good_combined` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `combined_good_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `good_combined_good_id_fk` (`combined_good_id`),
  CONSTRAINT `good_combined_good_id_fk` FOREIGN KEY (`combined_good_id`) REFERENCES `good` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `good_combined`
--

LOCK TABLES `good_combined` WRITE;
/*!40000 ALTER TABLE `good_combined` DISABLE KEYS */;
INSERT INTO `good_combined` VALUES (1,1,2,'2019-11-06 17:11:07','2019-11-06 17:11:11');
/*!40000 ALTER TABLE `good_combined` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `good_photo`
--

DROP TABLE IF EXISTS `good_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `good_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_main` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `good_photo_good_id_fk` (`good_id`),
  CONSTRAINT `good_photo_good_id_fk` FOREIGN KEY (`good_id`) REFERENCES `good` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `good_photo`
--

LOCK TABLES `good_photo` WRITE;
/*!40000 ALTER TABLE `good_photo` DISABLE KEYS */;
INSERT INTO `good_photo` VALUES (1,1,'https://sammy-icon.ru/upload/iblock/ca5/ca5d107a3cc4141fe0ea954d66691fca.jpg',1,'2019-11-05 19:02:40','2019-11-05 19:02:41'),(2,1,'uploads/bigGoodPhoto.jpg',0,'2019-11-06 16:41:49','2019-11-06 16:41:51'),(3,1,'uploads/bigGoodPhoto.jpg',0,'2019-11-06 16:42:14','2019-11-06 16:42:16'),(4,2,'uploads/bigGoodPhoto.jpg',1,'2019-11-06 17:10:51','2019-11-06 17:10:53'),(5,NULL,'good-photo/November2019/svj6qMAIc6ORkBnwCR9U.jpg',0,'2019-11-16 13:51:07','2019-11-16 13:51:07'),(6,NULL,'good-photo/November2019/rsmAJ2AviiY5oSAcI8EN.jpg',1,'2019-11-18 14:03:48','2019-11-18 14:03:48');
/*!40000 ALTER TABLE `good_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `good_size`
--

DROP TABLE IF EXISTS `good_size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `good_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `new_column` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `good_size_good_id_fk` (`good_id`),
  KEY `good_size_size_id_fk` (`size_id`),
  CONSTRAINT `good_size_good_id_fk` FOREIGN KEY (`good_id`) REFERENCES `good` (`id`),
  CONSTRAINT `good_size_size_id_fk` FOREIGN KEY (`size_id`) REFERENCES `size` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `good_size`
--

LOCK TABLES `good_size` WRITE;
/*!40000 ALTER TABLE `good_size` DISABLE KEYS */;
INSERT INTO `good_size` VALUES (1,1,1,'2019-11-05 19:03:08','2019-11-05 19:03:11',NULL),(2,1,2,'2019-11-06 16:53:10','2019-11-06 16:53:12',NULL),(3,2,1,'2019-11-08 15:44:54','2019-11-08 15:44:56',NULL);
/*!40000 ALTER TABLE `good_size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instagram`
--

DROP TABLE IF EXISTS `instagram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instagram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) DEFAULT NULL,
  `cid` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instagram`
--

LOCK TABLES `instagram` WRITE;
/*!40000 ALTER TABLE `instagram` DISABLE KEYS */;
INSERT INTO `instagram` VALUES (1,'3166346308.1677ed0.807a4668a4fc400485eedc3aba569185','3166346308','2019-11-07 11:14:18','2019-11-07 11:14:20');
/*!40000 ALTER TABLE `instagram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `look`
--

DROP TABLE IF EXISTS `look`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `look` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `look`
--

LOCK TABLES `look` WRITE;
/*!40000 ALTER TABLE `look` DISABLE KEYS */;
INSERT INTO `look` VALUES (1,'Праздничный','2019-11-13 16:57:28','2019-11-13 16:57:30'),(2,'Деловой','2019-11-13 16:57:43','2019-11-13 16:57:44'),(3,'Повседневный','2019-11-13 16:57:52','2019-11-13 16:57:54'),(4,'Пляжный','2019-11-13 16:58:06','2019-11-13 16:58:08'),(5,'Спорт','2019-11-13 16:58:17','2019-11-13 16:58:20'),(6,'Casual','2019-11-13 16:58:26','2019-11-13 16:58:28');
/*!40000 ALTER TABLE `look` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `look_combined`
--

DROP TABLE IF EXISTS `look_combined`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `look_combined` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `good_id` int(11) DEFAULT NULL,
  `look_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `look_combined_good_id_fk` (`good_id`),
  KEY `look_combined_look_id_fk` (`look_id`),
  CONSTRAINT `look_combined_good_id_fk` FOREIGN KEY (`good_id`) REFERENCES `good` (`id`),
  CONSTRAINT `look_combined_look_id_fk` FOREIGN KEY (`look_id`) REFERENCES `look` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `look_combined`
--

LOCK TABLES `look_combined` WRITE;
/*!40000 ALTER TABLE `look_combined` DISABLE KEYS */;
INSERT INTO `look_combined` VALUES (1,1,1,'2019-11-13 17:25:24','2019-11-13 17:25:26'),(2,2,1,'2019-11-13 17:27:36','2019-11-13 17:27:36'),(3,3,1,'2019-11-13 17:27:34','2019-11-13 17:27:37'),(4,3,2,'2019-11-13 17:27:39','2019-11-13 17:27:38'),(5,4,3,'2019-11-13 17:27:40','2019-11-13 17:27:40');
/*!40000 ALTER TABLE `look_combined` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `look_photo`
--

DROP TABLE IF EXISTS `look_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `look_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `look_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_main` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `look_photo_look_id_fk` (`look_id`),
  CONSTRAINT `look_photo_look_id_fk` FOREIGN KEY (`look_id`) REFERENCES `look` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `look_photo`
--

LOCK TABLES `look_photo` WRITE;
/*!40000 ALTER TABLE `look_photo` DISABLE KEYS */;
INSERT INTO `look_photo` VALUES (1,1,'uploads/girl1.jpg',1,'2019-11-13 17:00:53','2019-11-13 17:01:02'),(2,2,'uploads/girl2.jpg',1,'2019-11-13 17:00:54','2019-11-13 17:01:01'),(3,3,'uploads/girl3.jpg',1,'2019-11-13 17:00:56','2019-11-13 17:01:04'),(4,4,'uploads/girl4.jpg',1,'2019-11-13 17:00:57','2019-11-13 17:01:03'),(5,5,'uploads/sliderGirl3.jpg',1,'2019-11-13 17:00:58','2019-11-13 17:01:05'),(6,6,'uploads/sliderGirl2.jpg',1,'2019-11-13 17:00:59','2019-11-13 17:01:04'),(7,1,'uploads/girl4.jpg',0,'2019-11-13 17:01:00','2019-11-13 17:01:07'),(8,1,'uploads/girl3.jpg',0,'2019-11-13 17:01:00','2019-11-13 17:01:06');
/*!40000 ALTER TABLE `look_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `market`
--

DROP TABLE IF EXISTS `market`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `market` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) DEFAULT NULL,
  `lat` decimal(10,0) DEFAULT NULL,
  `lng` decimal(10,0) DEFAULT NULL,
  `place` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coordinates` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `market_city_id_fk` (`city_id`),
  CONSTRAINT `market_city_id_fk` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `market`
--

LOCK TABLES `market` WRITE;
/*!40000 ALTER TABLE `market` DISABLE KEYS */;
INSERT INTO `market` VALUES (1,1,56,38,'ТРЦ «Jam Молл»','Karl Marks','10:00','22:00','8953','333x444','2019-06-19 14:43:46','2019-05-19 14:43:50'),(2,2,57,39,'ТРЦ «Jam Молл»','Lenina','8:00','20:00','1234','445x666','2019-06-19 14:45:28','2019-11-19 14:45:30'),(3,3,58,41,'ТРЦ «Jam Молл»','Miliceiskaia','8:00','21:00','7651','666x666','2019-05-19 14:46:37','2019-09-19 14:46:40'),(4,4,61,35,'ТРЦ «Jam Молл»','Engelsa','11:20','00:00','6898','554x667','2019-05-19 14:47:38','2019-08-19 14:47:41'),(5,2,55,33,'ТРЦ «Jam Молл»','Kazanskaia','00:00','6:00','8800','555x222','2019-06-19 15:02:24','2019-09-19 15:02:27');
/*!40000 ALTER TABLE `market` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2019-11-16 10:11:31','2019-11-16 10:11:31','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2019-11-16 10:11:32','2019-11-16 10:11:32','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2019-11-16 10:11:33','2019-11-16 10:11:33','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2019-11-16 10:11:33','2019-11-16 10:11:33','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2019-11-16 10:11:34','2019-11-16 10:11:34',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,10,'2019-11-16 10:11:35','2019-11-16 10:11:35','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,11,'2019-11-16 10:11:36','2019-11-16 10:11:36','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,12,'2019-11-16 10:11:37','2019-11-16 10:11:37','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,13,'2019-11-16 10:11:38','2019-11-16 10:11:38','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2019-11-16 10:11:39','2019-11-16 10:11:39','voyager.settings.index',NULL),(11,1,'Hooks','','_self','voyager-hook',NULL,5,13,'2019-11-16 10:12:13','2019-11-16 10:12:13','voyager.hooks',NULL),(12,1,'Заявки и вопросы','','_self',NULL,NULL,NULL,15,'2019-11-16 10:19:33','2019-11-16 10:19:33','voyager.application.index',NULL),(13,1,'Цвета','','_self',NULL,NULL,NULL,16,'2019-11-16 10:23:27','2019-11-16 10:23:27','voyager.color.index',NULL),(14,1,'Размеры','','_self',NULL,NULL,NULL,17,'2019-11-16 10:25:47','2019-11-16 10:25:47','voyager.size.index',NULL),(15,1,'Категории','','_self',NULL,NULL,NULL,18,'2019-11-16 10:30:07','2019-11-16 10:30:07','voyager.category.index',NULL),(16,1,'Товары','','_self',NULL,NULL,NULL,19,'2019-11-16 10:42:00','2019-11-16 10:42:00','voyager.good.index',NULL),(17,1,'Фотографии товаров','','_self',NULL,NULL,NULL,20,'2019-11-16 10:45:26','2019-11-16 10:45:26','voyager.good-photo.index',NULL);
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2019-11-16 10:11:30','2019-11-16 10:11:30');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2019_08_19_000000_create_failed_jobs_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num` int(11) DEFAULT NULL,
  `apt` int(11) DEFAULT NULL,
  `bonus` decimal(10,0) DEFAULT NULL,
  `total` decimal(10,0) DEFAULT NULL,
  `payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (1,'Имя','Фамилия','+79991008820','Херов','тип','ул',3,3,3,3,'3','2019-11-11 11:32:36','2019-11-11 11:32:36'),(2,'Имя','Фамилия','+79991008820','Херов','тип','ул',3,3,3,3,'3','2019-11-11 11:32:39','2019-11-11 11:32:39'),(3,'Имя','Фамилия','+79991008820','Херов','тип','ул',3,3,3,3,'3','2019-11-11 11:39:46','2019-11-11 11:39:46'),(4,'sdfsd','fsdfsd','fdsf','sdfdsf','tp','dsfdsfsd',23,0,233,4997,'4764','2019-11-11 11:41:03','2019-11-11 11:41:03'),(5,'sdfsd','fsdfsd','fdsf','sdfdsf','tp','dsfdsfsd',23,0,233,4997,'4764','2019-11-11 11:48:25','2019-11-11 11:48:25'),(6,'sdfsd','fsdfsd','fdsf','sdfdsf','tp','dsfdsfsd',23,0,233,4997,'4764','2019-11-11 11:48:46','2019-11-11 11:48:46'),(7,'dfgdf','gdfgdf','gdfg','dfgdfg','tp','fgdfg',33,0,333,4997,'4664','2019-11-11 11:51:45','2019-11-11 11:51:45'),(8,'dfgdf','gdfgdf','gdfg','dfgdfg','tp','fgdfg',33,0,333,4997,'4664','2019-11-11 11:51:56','2019-11-11 11:51:56'),(9,'dfgdf','gdfgdf','gdfg','dfgdfg','tp','fgdfg',33,0,333,4997,'4664','2019-11-11 11:51:59','2019-11-11 11:51:59'),(10,'dfgdf','gdfgdf','gdfg','dfgdfg','tp','fgdfg',33,0,333,4997,'4664','2019-11-11 11:53:54','2019-11-11 11:53:54'),(11,'dfgdf','gdfgdf','gdfg','dfgdfg','tp','fgdfg',33,0,333,4997,'4664','2019-11-11 12:04:48','2019-11-11 12:04:48'),(12,'dfgdf','gdfgdf','gdfg','dfgdfg','tp','fgdfg',33,0,333,4997,'4664','2019-11-11 12:19:22','2019-11-11 12:19:22'),(13,'ывавыаывавы','выавыаыв','ывавы','аывавыа','tp','ываыва',33,0,33,4997,'4964','2019-11-11 12:29:46','2019-11-11 12:29:46'),(14,'dfgdfg','dfgdfg','dfgdf','gdfgdfg','tp','dfgdfg',0,0,6546,29000,'22454','2019-11-15 14:40:14','2019-11-15 14:40:14'),(15,'sdfsdf','sdfsdf','dsfdsf','sdfsdf','tp','sdfsdf',22,22,222,5000,'4778','2019-11-18 10:53:00','2019-11-18 10:53:00'),(16,'ываыва','ываыва','ываыва','ываыва','tp','ываыва',22,22,0,999,'999','2019-11-18 11:29:57','2019-11-18 11:29:57');
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_good`
--

DROP TABLE IF EXISTS `order_good`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL,
  `color` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_good_order_id_fk` (`order_id`),
  KEY `order_good_good_id_fk` (`good_id`),
  CONSTRAINT `order_good_good_id_fk` FOREIGN KEY (`good_id`) REFERENCES `good` (`id`),
  CONSTRAINT `order_good_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_good`
--

LOCK TABLES `order_good` WRITE;
/*!40000 ALTER TABLE `order_good` DISABLE KEYS */;
INSERT INTO `order_good` VALUES (1,11,2,'#00FF00',30,2,'2019-11-11 12:04:48','2019-11-11 12:04:48'),(2,11,1,'#000000',52,2,'2019-11-11 12:04:48','2019-11-11 12:04:48'),(3,11,1,'#FF0000',54,1,'2019-11-11 12:04:48','2019-11-11 12:04:48'),(4,12,2,'#00FF00',30,2,'2019-11-11 12:19:22','2019-11-11 12:19:22'),(5,12,1,'#000000',52,2,'2019-11-11 12:19:22','2019-11-11 12:19:22'),(6,12,1,'#FF0000',54,1,'2019-11-11 12:19:22','2019-11-11 12:19:22'),(7,13,2,'#00FF00',30,2,'2019-11-11 12:29:46','2019-11-11 12:29:46'),(8,13,1,'#000000',52,2,'2019-11-11 12:29:46','2019-11-11 12:29:46'),(9,13,1,'#FF0000',54,1,'2019-11-11 12:29:46','2019-11-11 12:29:46'),(10,14,2,'#000000',30,28,'2019-11-15 14:40:14','2019-11-15 14:40:14'),(11,14,2,'#00FF00',30,1,'2019-11-15 14:40:14','2019-11-15 14:40:14'),(12,15,2,'#000000',52,1,'2019-11-18 10:53:00','2019-11-18 10:53:00'),(13,15,3,'',0,2,'2019-11-18 10:53:01','2019-11-18 10:53:01'),(14,15,4,'',0,2,'2019-11-18 10:53:01','2019-11-18 10:53:01'),(15,16,1,'#FF0000',54,1,'2019-11-18 11:29:57','2019-11-18 11:29:57');
/*!40000 ALTER TABLE `order_good` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `page`
--

DROP TABLE IF EXISTS `page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `content` text,
  `identifier` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `page`
--

LOCK TABLES `page` WRITE;
/*!40000 ALTER TABLE `page` DISABLE KEYS */;
INSERT INTO `page` VALUES (1,'О нас','Контент','about','2019-11-15 10:33:10','2019-11-15 10:33:12'),(2,'Оплата','Контент','pay','2019-11-15 11:47:16','2019-11-15 11:47:19'),(3,'Гарантии','Контент','guarantee','2019-11-15 11:47:44','2019-11-15 11:47:45'),(4,'Возврат','Контент','refund','2019-11-15 11:48:00','2019-11-15 11:48:02'),(5,'Бонусная программа','Контент','bonus','2019-11-15 11:48:31','2019-11-15 11:48:33');
/*!40000 ALTER TABLE `page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2019-11-16 10:11:41','2019-11-16 10:11:41'),(2,'browse_bread',NULL,'2019-11-16 10:11:42','2019-11-16 10:11:42'),(3,'browse_database',NULL,'2019-11-16 10:11:43','2019-11-16 10:11:43'),(4,'browse_media',NULL,'2019-11-16 10:11:43','2019-11-16 10:11:43'),(5,'browse_compass',NULL,'2019-11-16 10:11:44','2019-11-16 10:11:44'),(6,'browse_menus','menus','2019-11-16 10:11:44','2019-11-16 10:11:44'),(7,'read_menus','menus','2019-11-16 10:11:45','2019-11-16 10:11:45'),(8,'edit_menus','menus','2019-11-16 10:11:45','2019-11-16 10:11:45'),(9,'add_menus','menus','2019-11-16 10:11:46','2019-11-16 10:11:46'),(10,'delete_menus','menus','2019-11-16 10:11:47','2019-11-16 10:11:47'),(11,'browse_roles','roles','2019-11-16 10:11:47','2019-11-16 10:11:47'),(12,'read_roles','roles','2019-11-16 10:11:48','2019-11-16 10:11:48'),(13,'edit_roles','roles','2019-11-16 10:11:49','2019-11-16 10:11:49'),(14,'add_roles','roles','2019-11-16 10:11:49','2019-11-16 10:11:49'),(15,'delete_roles','roles','2019-11-16 10:11:50','2019-11-16 10:11:50'),(16,'browse_users','users','2019-11-16 10:11:51','2019-11-16 10:11:51'),(17,'read_users','users','2019-11-16 10:11:52','2019-11-16 10:11:52'),(18,'edit_users','users','2019-11-16 10:11:52','2019-11-16 10:11:52'),(19,'add_users','users','2019-11-16 10:11:53','2019-11-16 10:11:53'),(20,'delete_users','users','2019-11-16 10:11:53','2019-11-16 10:11:53'),(21,'browse_settings','settings','2019-11-16 10:11:54','2019-11-16 10:11:54'),(22,'read_settings','settings','2019-11-16 10:11:55','2019-11-16 10:11:55'),(23,'edit_settings','settings','2019-11-16 10:11:55','2019-11-16 10:11:55'),(24,'add_settings','settings','2019-11-16 10:11:56','2019-11-16 10:11:56'),(25,'delete_settings','settings','2019-11-16 10:11:57','2019-11-16 10:11:57'),(26,'browse_hooks',NULL,'2019-11-16 10:12:14','2019-11-16 10:12:14'),(27,'browse_application','application','2019-11-16 10:19:29','2019-11-16 10:19:29'),(28,'read_application','application','2019-11-16 10:19:29','2019-11-16 10:19:29'),(29,'edit_application','application','2019-11-16 10:19:30','2019-11-16 10:19:30'),(30,'add_application','application','2019-11-16 10:19:31','2019-11-16 10:19:31'),(31,'delete_application','application','2019-11-16 10:19:31','2019-11-16 10:19:31'),(32,'browse_color','color','2019-11-16 10:23:24','2019-11-16 10:23:24'),(33,'read_color','color','2019-11-16 10:23:24','2019-11-16 10:23:24'),(34,'edit_color','color','2019-11-16 10:23:25','2019-11-16 10:23:25'),(35,'add_color','color','2019-11-16 10:23:26','2019-11-16 10:23:26'),(36,'delete_color','color','2019-11-16 10:23:26','2019-11-16 10:23:26'),(37,'browse_size','size','2019-11-16 10:25:43','2019-11-16 10:25:43'),(38,'read_size','size','2019-11-16 10:25:44','2019-11-16 10:25:44'),(39,'edit_size','size','2019-11-16 10:25:44','2019-11-16 10:25:44'),(40,'add_size','size','2019-11-16 10:25:45','2019-11-16 10:25:45'),(41,'delete_size','size','2019-11-16 10:25:45','2019-11-16 10:25:45'),(42,'browse_category','category','2019-11-16 10:30:04','2019-11-16 10:30:04'),(43,'read_category','category','2019-11-16 10:30:04','2019-11-16 10:30:04'),(44,'edit_category','category','2019-11-16 10:30:05','2019-11-16 10:30:05'),(45,'add_category','category','2019-11-16 10:30:05','2019-11-16 10:30:05'),(46,'delete_category','category','2019-11-16 10:30:06','2019-11-16 10:30:06'),(47,'browse_good','good','2019-11-16 10:41:56','2019-11-16 10:41:56'),(48,'read_good','good','2019-11-16 10:41:57','2019-11-16 10:41:57'),(49,'edit_good','good','2019-11-16 10:41:57','2019-11-16 10:41:57'),(50,'add_good','good','2019-11-16 10:41:58','2019-11-16 10:41:58'),(51,'delete_good','good','2019-11-16 10:41:58','2019-11-16 10:41:58'),(52,'browse_good_photo','good_photo','2019-11-16 10:45:22','2019-11-16 10:45:22'),(53,'read_good_photo','good_photo','2019-11-16 10:45:23','2019-11-16 10:45:23'),(54,'edit_good_photo','good_photo','2019-11-16 10:45:24','2019-11-16 10:45:24'),(55,'add_good_photo','good_photo','2019-11-16 10:45:24','2019-11-16 10:45:24'),(56,'delete_good_photo','good_photo','2019-11-16 10:45:25','2019-11-16 10:45:25');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quick_order`
--

DROP TABLE IF EXISTS `quick_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quick_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `good_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quick_order_good_id_fk` (`good_id`),
  CONSTRAINT `quick_order_good_id_fk` FOREIGN KEY (`good_id`) REFERENCES `good` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quick_order`
--

LOCK TABLES `quick_order` WRITE;
/*!40000 ALTER TABLE `quick_order` DISABLE KEYS */;
INSERT INTO `quick_order` VALUES (1,'sdfdsfsdf','sdfdsf',1,'2019-11-18 15:29:46','2019-11-18 15:29:46'),(2,'митит','смрт',13,'2019-11-20 13:00:14','2019-11-20 13:00:14'),(3,'qwe','qwe',2,'2019-11-20 14:19:28','2019-11-20 14:19:28'),(4,'qwe','qwe',1,'2019-11-21 16:14:28','2019-11-21 16:14:28'),(5,'qwe','qwe',2,'2019-11-23 18:06:43','2019-11-23 18:06:43'),(6,'89536918617','qwe',2,'2019-11-23 18:14:03','2019-11-23 18:14:03'),(7,'89536918617','qwe',1,'2019-11-23 18:14:39','2019-11-23 18:14:39'),(8,'89536918617','qwe',2,'2019-11-23 18:53:53','2019-11-23 18:53:53');
/*!40000 ALTER TABLE `quick_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review`
--

DROP TABLE IF EXISTS `review`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `review` text COLLATE utf8_unicode_ci,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review`
--

LOCK TABLES `review` WRITE;
/*!40000 ALTER TABLE `review` DISABLE KEYS */;
INSERT INTO `review` VALUES (1,'sdfsdfsd','sfdfdsfdsf','sdfdsfs','sdfsdf','2019-11-14 15:38:44','2019-11-14 15:38:44'),(2,'dsfsdf','dsfsdf','dsfdsf','sdfsdf','2019-11-14 15:39:45','2019-11-14 15:39:45'),(3,'sdfsdf','sdfsdfdsf','sdfsdfdsf','dsfsdf','2019-11-14 15:40:38','2019-11-14 15:40:38'),(4,'dsfdsfs','sdfdsfsdf','sdfdsfsdf','sdfdsf','2019-11-14 15:41:30','2019-11-14 15:41:30'),(5,'sdfdsf','sdfsdfdsf','dsfdsf','sdfsdfdsf','2019-11-14 15:42:08','2019-11-14 15:42:08'),(6,'sdfsdf','sdfsdfsdf','sdfsdf','sdfsdf','2019-11-14 15:46:10','2019-11-14 15:46:10'),(7,'qwe','qwe','qwe','qwe','2019-11-20 03:32:52','2019-11-20 03:32:52'),(8,'нпамог','пношепнорр р дщр ку кшш ше ш шеш  шоз щзщш ощ  шощш р ','орп','попро','2019-11-20 13:04:35','2019-11-20 13:04:35'),(9,'парар','арр','апрапр','апрапр','2019-11-20 13:04:49','2019-11-20 13:04:49'),(10,'qwe','qwe','qwe','89536918617','2019-11-21 15:34:17','2019-11-21 15:34:17'),(11,'qwe','asDsdf;ldkgdlfkg;ldflg','qwe','karail-fokus@mail.ru','2019-11-23 18:01:53','2019-11-23 18:01:53');
/*!40000 ALTER TABLE `review` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `review_photo`
--

DROP TABLE IF EXISTS `review_photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `review_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review_id` int(11) DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `review_photo_review_id_fk` (`review_id`),
  CONSTRAINT `review_photo_review_id_fk` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `review_photo`
--

LOCK TABLES `review_photo` WRITE;
/*!40000 ALTER TABLE `review_photo` DISABLE KEYS */;
/*!40000 ALTER TABLE `review_photo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2019-11-16 10:11:40','2019-11-16 10:11:40'),(2,'user','Normal User','2019-11-16 10:11:41','2019-11-16 10:11:41');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo_meta`
--

DROP TABLE IF EXISTS `seo_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `robots` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo_meta`
--

LOCK TABLES `seo_meta` WRITE;
/*!40000 ALTER TABLE `seo_meta` DISABLE KEYS */;
INSERT INTO `seo_meta` VALUES (1,'','','','contacts','2019-06-20 02:28:55','2019-08-20 02:28:58'),(2,'','','','franchise','2019-06-20 03:44:41','2019-10-20 03:44:43'),(3,NULL,NULL,NULL,'404','2019-07-20 10:24:52','2019-10-20 10:24:56'),(4,NULL,NULL,NULL,'about','2019-07-20 10:25:19','2019-11-20 10:25:21'),(5,NULL,NULL,NULL,'cart','2019-07-20 10:25:34','2019-06-20 10:25:43'),(6,NULL,NULL,NULL,'category','2019-10-20 10:26:08','2019-06-20 10:26:04'),(7,NULL,NULL,NULL,'collection','2019-12-20 10:26:27','2019-07-20 10:26:23'),(8,NULL,NULL,NULL,'good','2019-09-20 10:26:42','2019-08-20 10:26:56'),(9,NULL,NULL,NULL,'index','2019-07-20 10:27:14','2019-12-20 10:27:17'),(10,NULL,NULL,NULL,'instashop','2019-07-20 10:27:36','2019-12-20 10:27:39'),(11,NULL,NULL,NULL,'looks','2019-07-20 10:27:51','2019-08-20 10:27:54'),(13,NULL,NULL,NULL,'qaPage','2019-06-20 10:30:10','2019-11-20 10:30:13'),(14,NULL,NULL,NULL,'reviews','2019-06-20 10:30:31','2019-11-20 10:30:35'),(15,NULL,NULL,NULL,'search','2019-11-20 10:30:53','2019-11-20 10:30:51'),(16,NULL,NULL,NULL,'stepsBuy','2019-11-20 10:31:27','2019-11-20 10:31:31'),(17,NULL,NULL,NULL,'thanks','2019-11-20 10:31:54','2019-11-20 10:31:55'),(18,NULL,NULL,NULL,'bonus','2019-11-20 11:06:58','2019-11-20 11:07:00'),(19,NULL,NULL,NULL,'pay','2019-11-20 11:07:11','2019-11-20 11:07:13'),(20,NULL,NULL,NULL,'guarantee','2019-11-20 11:07:26','2019-11-20 11:07:30'),(21,NULL,NULL,NULL,'privacy','2019-11-20 11:09:04','2019-11-20 11:09:06'),(22,NULL,NULL,NULL,'favorite','2019-11-20 12:18:27','2019-11-20 12:18:29');
/*!40000 ALTER TABLE `seo_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `size`
--

DROP TABLE IF EXISTS `size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `size`
--

LOCK TABLES `size` WRITE;
/*!40000 ALTER TABLE `size` DISABLE KEYS */;
INSERT INTO `size` VALUES (1,'52','2019-11-16 15:08:58','2019-11-16 15:09:00'),(2,'54','2019-11-16 15:09:09','2019-11-16 15:09:11'),(3,'60','2019-11-16 15:09:17','2019-11-16 15:09:18');
/*!40000 ALTER TABLE `size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriber`
--

DROP TABLE IF EXISTS `subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriber`
--

LOCK TABLES `subscriber` WRITE;
/*!40000 ALTER TABLE `subscriber` DISABLE KEYS */;
INSERT INTO `subscriber` VALUES (1,'pitt.asv@yandex.ru','2019-11-15 11:44:09','2019-11-15 11:44:09'),(2,'dsadsa@dsfdsf','2019-11-15 11:46:22','2019-11-15 11:46:22'),(3,'','2019-11-15 15:43:23','2019-11-15 15:43:23'),(4,'','2019-11-15 15:44:26','2019-11-15 15:44:26'),(5,'qwe','2019-11-15 16:19:54','2019-11-15 16:19:54'),(6,'','2019-11-15 16:19:56','2019-11-15 16:19:56'),(7,'qew','2019-11-16 23:56:17','2019-11-16 23:56:17'),(8,'89536929350','2019-11-17 00:09:13','2019-11-17 00:09:13'),(9,'','2019-11-17 00:09:21','2019-11-17 00:09:21'),(10,'karail-fokus@mail.ru','2019-11-17 00:11:56','2019-11-17 00:11:56'),(11,'asd','2019-11-17 15:48:15','2019-11-17 15:48:15'),(12,'qwe','2019-11-17 15:48:55','2019-11-17 15:48:55'),(13,'qwe','2019-11-18 09:33:30','2019-11-18 09:33:30'),(14,'qwe','2019-11-18 09:33:30','2019-11-18 09:33:30'),(15,'qwe','2019-11-18 23:43:43','2019-11-18 23:43:43'),(16,'','2019-11-19 11:20:30','2019-11-19 11:20:30'),(17,'','2019-11-19 11:26:44','2019-11-19 11:26:44'),(18,'asdasdasd','2019-11-19 15:22:03','2019-11-19 15:22:03'),(19,'karail-fokus@mail.ru','2019-11-20 12:31:30','2019-11-20 12:31:30'),(20,'','2019-11-20 13:07:48','2019-11-20 13:07:48'),(21,'karail-fokus@mai.lru','2019-11-21 15:35:03','2019-11-21 15:35:03');
/*!40000 ALTER TABLE `subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'admin','admin@frimis.ru','users/default.png',NULL,'$2y$10$I7HCGdUQI5NsQy8jXpHmFu6pFSRY48iGhkZCtCZtZW4iLfqzXZG5y','kE57fU8D6Yhs1JI9mphr7SYmfHdwLg9l4xQJcW5xO3HxxWHJTI9EirD0Cawj',NULL,'2019-11-16 10:13:21','2019-11-16 10:13:23');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-06 11:44:01
