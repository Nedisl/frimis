<?php 
include ('elements/header.php');
?>
        <section class="contentWrapper">
        <?php 
            include ('./elements/sidebar.php');
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'cart'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Просмотренные</title>

            <!-- [RIGHT SIDE] -->
            <section class="mainContent">
                <!-- [Head] -->
                <nav class="minLinks">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/heart.php">Избранное</a></li>
                </nav>
                <h1>Просмотренные</h1>
                <!-- [/End Head] -->

                <!-- [MAIN CONTENT] -->
                <main class="basketPage">
                    <section class="basketPage__itemList">
                        
                    </section>
                </main>
                <!-- [/END CONTENT] -->
            </section>
            <!-- [/END RIGHT] -->
        </section>
    </div>
    <style>
        .fa-shopping-basket {
            transition: 0.3s
        }
        .fa-shopping-basket:hover {
            color: #ff4625;
        }
        .fa-heart {
            transition: 0.3s
        }
        .fa-heart:hover {
            color: #ff4625;
        }
    </style>
    <?php include('elements/footer.php') ?>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="/js/toast.js"></script>

    <!-- Custom -->
    <script>

        if (viewed !== null && viewed !== '') {
            var viewedArray = JSON.parse(localStorage.getItem("viewed"));

            viewedArray.viewed.forEach((el, index) => {
                    $('.basketPage__itemList').append(`
                        <figure class="goodItem" id="goodC${el.id}"> 
                            <div class="goodItem__wrapper">
                                <img src="<?=$pathAdm?>` + el.photo + `" alt="">
                                <figcaption>
                                    <a href=good.php?id="` + el.id + `">` + el.name + `</a>
                                    <span>Артикул: ` + el.art + `</span>
                                </figcaption>
                            </div>
                          
                            <span class="goodItem__price">` + el.price + ` руб.</span>
                            <button id="choosColor`+el.id+`" onclick='putToChoosen(\`${JSON.stringify(el)}\`, \`\`, \`\`)'>
                                <i style="font-size: 30px;" class="far fa-heart"></i>
                            </button>
                            <button onclick='putToBasket(\`${JSON.stringify(el)}\`, \`\`, \`\`)'>
                                <i style="font-size: 30px;" class="fas fa-shopping-basket"></i>
                            </button>
                            <button class="goodItem__close" onclick="removeElementEyeStr(${el.id})">
                                <i class="fas fa-times" aria-hidden="true"></i>
                            </button>
                        </figure>
                        `);      
            });
            viewedArray.viewed.forEach((el, index) => {
                for (val of charr) {
                    if(el.id == val) {
                        $("#choosColor" + el.id).css("color", "#ff4625");
                    }
                    
                } 
            });
        }







        function removeElementEyeStr(i) {
            // viewed["viewed"].splice(i, 1); 
            viewed["viewed"] = viewed["viewed"].filter(item => item.id != i);
            $("#goodC" + i).remove();
            localStorage.setItem('viewed', JSON.stringify(viewed));
        }

        function putToChoosen(element) {
        var element = JSON.parse(element);
        var choosen = localStorage.getItem("choosen");
        var choosenArray = { choosen: [] };
        
        if (choosen !== null && choosen !== '') {
            var choosenArray = JSON.parse(choosen);
            var index = choosenArray.choosen.findIndex(el => el.id === element.id);
            if (index === null || index === -1) {
                $("#choosColor" + element.id).css("color", "#ff4625");
                console.log(element.id);
                choosenArray.choosen.push(element);
                new Toast({
                    message: 'Товар успешно добавлен в избранное',
                    type: 'danger'
                });
            } else {
                $("#choosColor" + element.id).css("color", "rgb(73, 73, 73)");
                choosenArray.choosen.splice(index, 1); 
                new Toast({
                    message: 'Товар удален из избранного',
                    type: 'danger'
                });
            }
        } else {
            console.log(element.id);
            $("#choosColor" + element.id).css("color", "#ff4625");
            choosenArray.choosen.push(element);
            new Toast({
                    message: 'Товар успешно добавлен в избранное',
                    type: 'danger'
            });
        }
        
        localStorage.setItem('choosen', JSON.stringify(choosenArray));
    }


    $('.selectSize').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectSize_closed',
            'ui-selectmenu-button-open': 'selectSize_open',
            'ui-selectmenu-menu': 'selectSize__menu'
        }
    });

    $('.blockPrice select').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectPrice_closed',
            'ui-selectmenu-button-open': 'selectPrice_open',
            'ui-selectmenu-menu': 'selectPrice__menu'
        }
    });

    $('.selectType').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectType_closed',
            'ui-selectmenu-button-open': 'selectType_open',
            'ui-selectmenu-menu': 'selectType__menu'
        }
    });
    </script>
    <!-- /Custom -->
    <!-- [/SCRIPTS] -->
</body>
</html>