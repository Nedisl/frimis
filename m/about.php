        <!-- [HEAD] -->
        <?php
            include ('elements/header.php');
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'about'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>О нас</title>
        <!-- [/END HEAD] -->

        <!-- [MAIN CONTENT] -->
        <main class="aboutPage">
            <h1>О нас</h1>
            <img src="img/aboutUs.jpg" alt="">
            <span>
                Aксессуары Frimis созданы специально для девушек и женщин, стремящихся следовать модным тенденциям.<br>
                Frimis - это широкий выбор бижутерии, аксессуаров, украшений для волос.
            </span>

            <section class="aboutPage__wrapper">
                <figure>
                    <img src="img/about1.jpg" alt="">
                    <figcaption>Опыт работы с 2007 года</figcaption>
                </figure>
                <figure>
                    <img src="img/about2.jpg" alt="">
                    <figcaption>Регулярные скидки, акции, распродажи, накопительная бонусная система</figcaption>
                </figure>
                <figure>
                    <img src="img/about3.jpg" alt="">
                    <figcaption>Новинки поступления<br>каждую неделю</figcaption>
                </figure>
            </section>
        </main>
        <!-- [/END CONTENT] -->

        <!-- [FOOTER] -->
        <?php
            include ('elements/footer.php');
        ?>
        <!-- [/END FOOTER] -->
    </section>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <!-- [/SCRIPTS] -->
</body>

</html>