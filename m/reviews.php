
        <!-- [HEAD] -->
        <?php
            include ('elements/header.php');
        ?>
          <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'reviews'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Отзывы</title>
        <!-- [/END HEAD] -->

        <!-- [MAIN CONTENT] -->
        <main class="reviewsPage">
            <!-- [Head] -->
            <h1>Отзывы</h1>
            <!-- [/End Head] -->
            <figure class="orangeBlock">
                <img src="img/reviewsGirl.png" alt="">
                <div class="orangeBlock__wrapper">
                    <figcaption>
                        <h2>Помогите нам<br>стать лучше!</h2>
                        <span>
                            Напишите, что вы заказывали,<br>
                            понравился ли вам товар,<br>
                            удобно ли было заказывать.
                        </span>
                        <a data-modal=".modal__sendReview">Написать отзыв</a>
                    </figcaption>
                </div>
            </figure>

            <section class="reviewsPage__wrapper">
              
            </section>

            <button class="showMore" onclick="loadMoreReviews()">Показать ещё</button>
        </main>
        <!-- [/END CONTENT] -->

        <!-- [FOOTER] -->
        <?php
            include ('elements/footer.php');
        ?>
        <!-- [/END FOOTER] -->
    </section>

    <!-- [MODAL] -->
    <div id="modalReview" class="modalWrapper">

<section class="modal modal_medium modal__sendReview">
    <h3>Оставить отзыв</h3>
    <div style="margin-bottom:14px">
        <form class="form_message" >
            <div class="form-box">
                <input id="user_name" type="text" placeholder="Ваше имя" name="name">
                <input id="user_phone" type="text" placeholder="Ваш телефон | e-mail" name="phone">
                <input id="user_city" type="text" placeholder="Ваш город" name="city">
                <input id="user_message" type="text" placeholder="Ваше сообщение" name="review">
            </div>
            <div class="form-box"  style="margin-bottom:16px">
                <button class="sendFile">Выбрать файл</button>
                <input id="file" type="file" name="file">
                <input type="submit" class="showMore1" value="Отправить">
            </div>
        </form>
        <div class="saveFile-block" >
            <p class="savefile" style="display: inline-block;"></p>
            <button>X</button>
        </div>
    </div>
        <span class="uText" >Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных и ознакомлен(а) с условиями конфиденциальности.</span>
    
</section>
<style>
.showMore1 {
    background-color: #363636;
    width: 50%;
    height: 45px;
    font-size: 18px;
    color: #fff;
    text-align: center;
    font-family: GothamPro,Verdana;
    font-weight: 400;
    padding: 10px 0;
    -webkit-transition: background-color .3s;
    -moz-transition: background-color .3s;
    transition: background-color .3s;
}
.reviews-photo {
    margin-top: 10px;
    width: 260px;
}
.saveFile-block button {
    position: absolute;
    margin-left: 7px;
    margin-top: 2px;
    font-weight: 1000;
}
</style>
</div>
    <!-- [/END MODAL] -->

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/js/toast.js"></script>

    <script src="/fancybox-master/dist/jquery.fancybox.min.js"></script>

    <script>
    nextUrl = '/api/reviews.php?limit=3&offset=0';

    
    document.querySelector('.saveFile-block').style.display = 'none'

    document.querySelector('#file').onchange = function() {
        document.querySelector('.savefile').innerHTML = this.files[0].name
        document.querySelector('.saveFile-block').style.display = 'block'
        
    }
    document.querySelector('.saveFile-block button').onclick = function() {
        document.querySelector('#file').value = null
        document.querySelector('.savefile').innerHTML = null
        document.querySelector('.saveFile-block').style.display = 'none'
    }

    loadMoreReviews();
    function loadMoreReviews() {
            $.get(nextUrl, (res) => {
                if (res.data.length < 3) {
                    $('.showMore').hide();
                } else {
                    $('.showMore').show();
                }
                console.log(res);
                res.data.forEach((el) => {
                    if (el.answer == null) {
                        $('.reviewsPage__wrapper').append(`
                        <article>
                            <section class="reviewContent">
                                <div class="reviewContent__head">
                                    <img src="img/noPhoto.jpg" alt="">
                                    <div>
                                        <h3>${el.name}</h3>
                                        <span class="reviewContent__uHead">${el.created_at} |  ${el.city}</span>
                                    </div>    
                                </div>
                                <span class="reviewContent__text">
                                    ${el.review}
                                    <div class="attachFiles">
                                        <a data-fancybox href="/files/${el.photo}" class="reviews-photo-a${el.id}">
                                            <img class="reviews-photo" src="/files/${el.photo}" alt="">
                                        </a>
                                    </div>
                                </span>
                            </section>
                        </article>
                        `);
                    } else {
                        $('.reviewsPage__wrapper').append(`
                        <article>
                            <section class="reviewContent">
                                <div class="reviewContent__head">
                                    <img src="img/noPhoto.jpg" alt="">
                                    <div>
                                        <h3>${el.name}</h3>
                                        <span class="reviewContent__uHead">${el.created_at} |  ${el.city}</span>
                                    </div>    
                                </div>
                                <span class="reviewContent__text">
                                    ${el.review}
                                    <div class="attachFiles">
                                        <a data-fancybox href="/files/${el.photo}" class="reviews-photo-a${el.id}">
                                            <img class="reviews-photo" src="/files/${el.photo}" alt="">
                                        </a>
                                    </div>
                                </span>
                                <section>
                                    <h3 style="padding-top: 20px">Администратор</h3>
                                    <span class="reviewContent__uHead">${el.updated_at} |  ${el.city}</span>
                                    <span class="reviewContent__text">
                                        ${el.answer}
                                    </span>
                                </section>
                            </section>
                        </article>
                            `);
                    }
                    

                    if (el.photo == null) {
                        $(`.reviews-photo-a${el.id}`).css('display','none')
                    }
                });
                
               
                nextUrl = res.next_url;
                console.log(nextUrl);
            });
    }
    $(".form_message").on("submit", function(e) {
        e.preventDefault();
        var orderData = {};
            orderData.name = $('#user_name').val();
            orderData.phone = $('#user_phone').val();
            orderData.city = $('#user_city').val();
            orderData.review = $('#user_message').val();

        let f = false;

        if (orderData.name == false && orderData.phone == false && orderData.city == false && orderData.review == false) {
            f = 'Не все поля заполнены';
        } else if (orderData.phone != +orderData.phone || orderData.phone == false) {
            if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(orderData.phone) == false) {
                f = 'Не корректный email';
            }
        } else if (orderData.phone == +orderData.phone) {
            if (orderData.phone.length != 11) {
                f = 'Не корректный номер телефона';
            }
        }
        if (f) {
                new Toast({
                    message: f,
                    type: 'danger'
                });
        } else {

            var fd = new FormData($(this)[0]);    
            fd.append('file', $('#file')[0].files[0]);

            $.ajax({
                url: 'api/message.php',
                data: fd,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function(data){
                    if (data.status == 1) {
                        new Toast({
                            message: 'Ваше сообщение отправлено!',
                            type: 'danger'
                        });
                        closeModal();
                        document.querySelector('#user_name').value = null
                        document.querySelector('#user_phone').value = null
                        document.querySelector('#user_city').value = null
                        document.querySelector('#user_message').value = null
                        document.querySelector('#file').value = null
                        location.reload()
                    } else {
                        new Toast({
                            message: 'Не удалось отправить ваше сообщение!',
                            type: 'danger'
                        });
                    }
                }
            });
            
        }
    });
    /* == [SELECT UI] == */
    $('.chLook__list').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectMenu_closed',
            'ui-selectmenu-button-open': 'selectMenu_open',
            'ui-selectmenu-menu': 'selectMenu__menu'
        }
    });
    </script>


    <!-- [/SCRIPTS] -->
</body>

</html>