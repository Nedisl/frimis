
        <!-- [HEAD] -->
        <?php
            include ('elements/header.php');
        ?>
                <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'favorite'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Избранное</title>
        <!-- [/END HEAD] -->

        <!-- [MAIN CONTAINER] -->
        <main class="favoritePage">
            <h1>Избранное</h1>
            <!-- [MAIN CONTENT] -->
            <div class="favoritePage__wrapper">

            </div>
            <!-- [/END CONTENT] -->
        </main>
        <!-- [/MAIN CONTAINER] -->

        <!-- [FOOTER] -->
        <?php
            include ('elements/footer.php');
        ?>
        <!-- [/END FOOTER] -->
    </section>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


    <script>
            let choosen = { choosenArray: [] };
            let arrval1 = document.querySelector('.favoritePage__wrapper');
            
            
            if (localStorage.getItem('choosen')) {
            choosenArray = JSON.parse(localStorage.getItem('choosen'));
            choosen = choosenArray; 
            } else {
                choosenArray = {};
                choosen = choosenArray;
            }
            
            let out = '';
            if (localStorage.getItem('choosen')) {
                for (let i = 0; i < choosen['choosen'].length; i++) {
                    if (choosen["choosen"][i]["photo"] == null) {
                        choosen["choosen"][i]["photo"] = 'img/box.jpg';
                    }
                    out +=`<figure id="good${choosen["choosen"][i]["id"]}">`;
                    out += '<a href="good.php?id='+choosen["choosen"][i]["id"]+'"><img src="<?=$pathAdm?>'+choosen["choosen"][i]["photo"]+'" alt=""></a>';
                    out += `<div class="goodPanel">
                                <button onclick="removeElementHeart(${choosen["choosen"][i]["id"]})"><i class="fas fa-times"></i></button>
                            </div>`
                    out += '<figcaption>';
                    out += '<div class="goodTitle">';
                    out += '<a href="good.php?id='+choosen["choosen"][i]['id']+'">'+choosen["choosen"][i]['name']+'</a>';
                    out += '<span>'+choosen["choosen"][i]['price']+'  руб.</span>';
                    out += '</div>';
                    out += `<button onclick='putToBasket(\`${JSON.stringify(choosen["choosen"][i])}\`, \`\`, \`\`)'><i class="fas fa-shopping-basket"></i></button>`
                    out += '</figcaption>';
                    out +='</figure>';
                }
            
            } 
            arrval1.innerHTML = out;


            function removeElementHeart(i) {
                // choosen["choosen"].splice(i, 1); 
                choosen["choosen"] = choosen["choosen"].filter(item => item.id != i);
                $("#good" + i).remove();
                localStorage.setItem('choosen', JSON.stringify(choosen));
            }


            function putToBasket(element, size, color) {
                var element = JSON.parse(element);
                element.quantity = 1;
                element.color = color;
                element.size = size;
                console.log(element.size + ' ' + element.color);
                var basket = localStorage.getItem("basket");
                var basketArray = { basket: [] };
                
                if (basket !== null && basket !== '') {
                    var basketArray = JSON.parse(basket);
                    var index = basketArray.basket.findIndex(el => el.id === element.id && el.color === color && el.size === size);
                    console.log(element);
                    if (index !== null && index !== -1) {
                        console.log('in:' + index);
                        basketArray.basket[index].quantity = +basketArray.basket[index].quantity + 1;
                        console.log(element.size);
                        new Toast({
                            message: 'Товар успешно добавлен в корзину',
                            type: 'danger'
                        });
                    } else {
                        console.log(element.size);
                        basketArray.basket.push(element);
                        new Toast({
                            message: 'Товар успешно добавлен в корзину',
                            type: 'danger'
                        });
                    }
                } else {
                    console.log(element.size);
                    basketArray.basket.push(element);
                    new Toast({
                        message: 'Товар успешно добавлен в корзину',
                        type: 'danger'
                    });
                }
                
                localStorage.setItem('basket', JSON.stringify(basketArray));
            }
            </script>

    <!-- [/SCRIPTS] -->
</body>

</html>