<?php
    include ("db.php");
    $categories_res = mysqli_query($db, "SELECT * FROM category");
?>
<!-- [PANELS] -->
<aside class="menuBar">
            <section class="menuBar__head">
                <button class="menuBar__toggle menuBar__toggle_close"><i class="fas fa-times"></i></button>
                <form class="searchBox" action="" onsubmit="return false;">
                    <input type="text" placeholder="Введите товар..." id="searchInput">
                    <button onclick="search()"><i class="fas fa-search"></i></button>
                </form>
                <script>
                    function search() {
                        var q = $('#searchInput').val();
                        if (q != '') {
                            window.location.href = '/m/search.php?search=' + q;
                        }
                    }
                </script>
            </section>
            <section class="menuBar__wrapper">
                <!-- [TOGGLE MENU] -->
                <nav class="firstMenu">
                    <li><a href="/m/category.php?id=all&new=1&look=0&color=0">НОВИНКИ</a></li>
                    <li><a href="/m/category.php?id=all&best=1&look=0&color=0">ЛУЧШИЕ ПРЕДЛОЖЕНИЯ</a></li>
                    <li><a href="/m/category.php?id=all&sale=1&look=0&color=0">РАСПРОДАЖА</a></li>

                    <?php while ($categories_row = mysqli_fetch_array($categories_res)) { 
                        if ($categories_row['root_id'] == 0) { ?>
                            <?php $cat_id = $categories_row['id']; $goods_res = mysqli_query($db, "SELECT * FROM category WHERE root_id = $cat_id");  ?>
                            <li>
                            <a class="menuBar__more"><?php echo $categories_row['name']; ?></a>
                                <ul>
                                    <?php while ($goods_row = mysqli_fetch_array($goods_res)) {  ?>
                                        <li><a href="/m/category.php?id=<?= $goods_row['id'] ?>"><?php echo $goods_row['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php }
                        } ?>
                </nav>
                <style>
                    .mainWrapper .menuBar__wrapper .firstMenu > li:nth-last-child(2) {
                        margin-top: 0px !important;
                    }
                </style>
                <!-- [/END MENU] -->