<!-- [FOOTER] -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="libs/readmore/readmore.min.js"></script>
    <script src=".././libs/jquery.maskedinput.js"></script>
    <script>
        $(".phone_mask").mask("+7(999)999-99-99");
    </script>
    <script src="/js/toast.js"></script>
    <script src="js/main.js"></script>
    <script>
        function subscribe() {
            var subscriberData = {};
                subscriberData.email = $('#email-footer').val().trim();
                let f = false;

                if (subscriberData.email == false) {
                    f = 'Введите email';
                } else if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(subscriberData.email) == false) {
                    f = 'Не корректный email';
                }
                if (f) {
                    new Toast({
                        message: f,
                        type: 'danger'
                    });
                } else {
                    $.post("/m/api/subscribe.php", subscriberData).done(function(data) {
                    if (data.status == 1) {
                        new Toast({
                            message: 'Вы успешно подписались!',
                            type: 'danger'
                        });
                        $('#email-footer').val('');
                    } else {
                        new Toast({
                            message: 'Не удалось подписаться!',
                            type: 'danger'
                        });
                    }
                });
                }
        }
    </script>
<footer>
            <section class="footer__wrapper">
                <section class="footer__top">
                    <section class="footer__menu">
                        <nav>
                            <li><a href="stepsBuy.php">Как заказать</a></li>
                            <li><a href="page.php?id=bonus">Бонусная программа</a></li>
                            <li><a href="page.php?id=pay">Оплата и доставка</a></li>
                            <li><a href="page.php?id=guarantee">Гарантии и возврат</a></li>
                            <li><a href="qaPage.php">Вопрос-ответ</a></li>
                        </nav>
                        <nav>
                            <li><a href="about.php">О компании</a></li>
                            <li><a href="reviews.php">Отзывы</a></li>
                            <li><a href="franchise.php">Франшиза</a></li>
                            <li><a href="contacts.php">Контакты</a></li>
                        </nav>
                    </section>
                    <section class="footer__contacts">
                        <ul class="number">
                            <?php
                            $contacts_res = mysqli_query($db, "SELECT * FROM contacts");
                            while ($contacts_row = mysqli_fetch_array($contacts_res)) {
                            ?>
                                <li style="cursor: pointer"><a href=""><i class="fas fa-phone-alt"></i><?=$contacts_row['phone']?></a></li>
                                <li style="cursor: pointer"><a href=""><i class="fas fa-envelope"></i><?=$contacts_row['email']?></a></li>
                            <?php } ?> 
                        </ul>
                        <ul class="social">
                            <li><a href="#"><i class="fab fa-vk"></i></a></li>
                            <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fab fa-odnoklassniki"></i></a></li>
                        </ul>
                    </section>
                    <form onsubmit="return false;" id="footer-form">
                        <h4>Оформите подписку</h4>
                        <input type="text" placeholder="Укажите e-mail" id="email-footer">
                        <button class="buttonSubscribe-mobile" onclick="subscribe()">Подписаться</button>
                        <label>
                            Нажимая на кнопку «Подписаться», я
                            соглашаюсь на обработку моих персональных
                            данных и ознакомлен(а) с условиями
                            конфиденциальности.
                        </label>
                    </form>
                </section>
                <section class="footer__info">
                    <span>
                        © «Frimis» — интернет-магазин украшений и аксессуаров.<br>
                        <a href="page.php?id=privacy">Политика конфиденциальности.</a>
                    </span>
                    <a href="https://5agency.ru" class="fiveLogo">Разработка<br>и дизайн сайта «FIVE»</a>
                </section>
            </section>
        </footer>
        <!-- [/END FOOTER] -->