<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/fancybox-master/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="libs/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="libs/owlcarousel/owl.theme.default.min.css">
    <link rel="stylesheet" href="toast.css">
    <link rel="stylesheet" href="additional.css">
    <link rel="stylesheet" href="styles.css">
    <script src="https://kit.fontawesome.com/c6f437d80c.js" crossorigin="anonymous"></script>
</head>

<body>
    <?php
        $pathAdm = 'http://frimisadm.onoartsr.beget.tech/storage/';
    ?>
    <section class="mainWrapper">
        <!-- [HEAD] -->
        <header>
            <button class="menuBar__toggle"><i class="fas fa-bars"></i></button>

            <a href="/m/"><img src="img/logo.jpg" alt=""></a>
            <menu>
                <li>
                    <button class="goodsHistory__show" id="goods-eye"><i class="far fa-eye"></i></button>
                </li>
                <li>
                    <a href="favorite.php"><button class="goodsHistory__show"><i class="far fa-heart"></i></button></a>
                </li>
                <li>
                    <button onclick="window.location.href = 'cart.php'"><i class="fas fa-shopping-basket"></i></button>
                </li>
            </menu>
        </header>
        <!-- [/END HEAD] -->
        <?php
            include ('elements/sidebar.php');
        ?>
        
                <!-- [Under Nav] -->
                <nav class="secondMenu">
                    <li><a href="/m/looks.php?id=1">Готовые образы</a></li>
                    <li><a href="/m/instashop.php">Instashop</a></li>
                    <li>
                        <a href="#">Покупателям</a>
                        <ul style="display: none;">
                            <li><a href="stepsBuy.php">Как заказать</a></li>
                            <li><a href="#">Оплата</a></li>
                            <li><a href="#">Бонусная программа</a></li>
                            <li><a href="#">Доставка</a></li>
                            <li><a href="#">Гарантии</a></li>
                            <li><a href="#">Возврат</a></li>
                            <li><a href="#">Вопрос-ответ</a></li>
                            <li><a href="reviews.php">Отзывы</a></li>
                            <li><a href="aboutUs.php">О компании</a></li>
                            <li><a href="#">Политика конфиденциальности</a></li>
                        </ul>
                    </li>
                    <li><a href="contacts.php">Контакты</a></li>
                    <li><a href="franchise.php">Франшиза</a></li>
                </nav>
                <!-- [/Under Nav] -->
            </section>
        </aside>

        <section class="goodsHistory">

        </section>
        <!-- [/PANELS] -->
        <script>
        if (localStorage.getItem('choosen')) {
            choosenArray = JSON.parse(localStorage.getItem('choosen'));
            choosen = choosenArray; 
        } else {
            choosenArray = {};
            choosen = choosenArray;
        }
        let charr = [];
        if (choosenArray.choosen != undefined) {
            for (let i = 0; i < choosenArray.choosen.length; i++) {
                charr.push(choosenArray.choosen[i].id)
            }
        }

        console.log(charr)



            let viewed = { viewedArray: [] };
            //let choosen = { choosenArray: [] };
            let arrval = document.querySelector('.goodsHistory');
            
            document.querySelector('#goods-eye').onclick = function() {
                let out = '';
                if (localStorage.getItem('viewed')) {
                viewedArray = JSON.parse(localStorage.getItem('viewed'));
                viewed = viewedArray;
                } else {
                    viewedArray = {};
                    viewed = viewedArray;
                }
                if (viewed['viewed'] == undefined || !localStorage.getItem('viewed') || viewed['viewed'].length == 0) {
                        arrval.style.border = 'none';
                }

                if (localStorage.getItem('viewed')) {
                    for (let i = 0; i < viewed['viewed'].length; i++) {
                        if (viewed["viewed"][i]["photo"] == null) {
                            viewed["viewed"][i]["photo"] = 'img/box.jpg';
                        }
                        out +=`<figure id="good${viewed["viewed"][i]["id"]}">`;
                        out += '<a href="good.php?id='+viewed["viewed"][i]["id"]+'"><img src="'+viewed["viewed"][i]["photo"]+'" alt=""></a>';
                        out += '<figcaption>';
                        out += '<a href="good.php?id='+viewed['viewed'][i]['id']+'">'+viewed['viewed'][i]['name']+'</a>';
                        out += '<span>Артикул: '+viewed['viewed'][i]['art']+'</span>';
                        out += '<strong>'+viewed['viewed'][i]['price']+'  руб.</strong>';
                        out += '</figcaption>';
                        out += `<div class="goodsHistory__panel">
                                    <button id="choosColor`+viewed['viewed'][i]['id']+`" onclick='putToChoosen(\`${JSON.stringify(viewed["viewed"][i])}\`, \`\`, \`\`)'><i class="far fa-heart"></i></button>
                                    <button onclick='putToBasket(\`${JSON.stringify(viewed["viewed"][i])}\`, \`\`, \`\`)'><i class="fas fa-shopping-basket"></i></button>
                                    <button onclick="removeElementEye(${viewed["viewed"][i]["id"]})"><i class="fas fa-times"></i></button>
                                </div>`;
                        out +='</figure>';
                    }
                    arrval.style.border = '1 px solid black';
                } 
                
                arrval.innerHTML = out;

                viewedArray.viewed.forEach((el, index) => {
                    for (val of charr) {
                        if(el.id == val) {
                            $("#choosColor" + el.id).css("color", "#ff4625");
                        }
                        console.log(val)
                    } 
                    console.log($("#choosColor" + el.id))
                });
            }
            

            function removeElementEye(i) {
                // viewed["viewed"].splice(i, 1); 
                viewed["viewed"] = viewed["viewed"].filter(item => item.id != i);
                $("#good" + i).remove();
                localStorage.setItem('viewed', JSON.stringify(viewed));

            }

            function putToBasket(element, size, color) {
                var element = JSON.parse(element);
                element.quantity = 1;
                element.color = color;
                element.size = size;
                console.log(element.size + ' ' + element.color);
                var basket = localStorage.getItem("basket");
                var basketArray = { basket: [] };
                
                if (basket !== null && basket !== '') {
                    var basketArray = JSON.parse(basket);
                    var index = basketArray.basket.findIndex(el => el.id === element.id && el.color === color && el.size === size);
                    console.log(element);
                    if (index !== null && index !== -1) {
                        console.log('in:' + index);
                        basketArray.basket[index].quantity = +basketArray.basket[index].quantity + 1;
                        console.log(element.size);
                        new Toast({
                            message: 'Товар успешно добавлен в корзину',
                            type: 'danger'
                        });
                    } else {
                        console.log(element.size);
                        basketArray.basket.push(element);
                        new Toast({
                            message: 'Товар успешно добавлен в корзину',
                            type: 'danger'
                        });
                    }
                } else {
                    console.log(element.size);
                    basketArray.basket.push(element);
                    new Toast({
                        message: 'Товар успешно добавлен в корзину',
                        type: 'danger'
                    });
                }
                
                localStorage.setItem('basket', JSON.stringify(basketArray));
            }

            function putToChoosen(element) {
                var element = JSON.parse(element);
                var choosen = localStorage.getItem("choosen");
                var choosenArray = { choosen: [] };
                
                if (choosen !== null && choosen !== '') {
                    var choosenArray = JSON.parse(choosen);
                    var index = choosenArray.choosen.findIndex(el => el.id === element.id);
                    if (index === null || index === -1) {
                        $("#choosColor" + element.id).css("color", "#ff4625");
                        console.log(element.id);
                        choosenArray.choosen.push(element);
                        new Toast({
                            message: 'Товар успешно добавлен в избранное',
                            type: 'danger'
                        });
                    } else {
                        $("#choosColor" + element.id).css("color", "rgb(73, 73, 73)");
                        choosenArray.choosen.splice(index, 1); 
                        new Toast({
                            message: 'Товар удален из избранного',
                            type: 'danger'
                        });
                    }
                } else {
                    console.log(element.id);
                    $("#choosColor" + element.id).css("color", "#ff4625");
                    choosenArray.choosen.push(element);
                    new Toast({
                            message: 'Товар успешно добавлен в избранное',
                            type: 'danger'
                    });
                }
                
                localStorage.setItem('choosen', JSON.stringify(choosenArray));
            }
            </script>


        <!-- [/PANELS] -->
        