
        <!-- [HEAD] -->
        <?php
            include ('elements/header.php');
        ?>
             <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'qaPage'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Вопрос-ответ</title>
        <!-- [/END HEAD] -->

        <!-- [MAIN CONTENT] -->
        <!-- [/End Head] -->

        <!-- [MAIN CONTENT] -->
        <main class="qaPage">
            <h1>Вопрос-ответ</h1>
            <figure class="orangeBlock">
                <img src="img/qaWoman.png" alt="">
                <div class="orangeBlock__wrapper">
                    <figcaption>
                        <h2>Остались<br>вопросы?</h2>
                        <span>
                            Напишите нам,<br>
                            и менеджер перезвонит<br>
                            вам в ближайшее время
                        </span>
                        <button data-modal=".modal__question"><a>Задать вопрос</a></button>
                    </figcaption>
                </div>
            </figure>

            <?php
                        $question_res = mysqli_query($db, "SELECT * FROM question_category");

                        while ($question_row = mysqli_fetch_assoc($question_res)) {

                            $answer_res = mysqli_query($db, "SELECT * FROM question_answer WHERE category_id = ".$question_row['id']);
                    ?>
                        <h2><?=$question_row['name']?></h2>
                        <?php
                            while ($answer_row = mysqli_fetch_assoc($answer_res)) {
                        ?>
                        <section class="qaPage__accordion">
                            <h3><?=$answer_row['question']?></h3>
                            <div>
                                <p><?=$answer_row['answer']?></p>
                            </div>
                        </section>
                    <?php
                            }
                        }
                    ?>
            <!-- <button class="showMore">Показать ещё</button> -->
        </main>
        <!-- [/END CONTENT] -->
           <!-- [MODAL] -->
    <div class="modalWrapper">
        <section class="modal modal_medium modal__question">
            <h3>Задать вопрос</h3>
            <form onsubmit="return false;">
                <div class="form-box">
                    <input type="text" placeholder="Ваше имя" id="modal-name">
                    <input type="text" placeholder="Ваш телефон | e-mail" id="modal-email">
                    <input type="text" placeholder="Ваш город" id="modal-city">
                    <input type="text" placeholder="Ваше сообщение" id="modal-message">
                </div>
                <div class="form-box">
                    <!-- <button class="sendFile">Выбрать файл</button>
                    <input type="file" id="modal-file"> -->
                    <button class="buttonSubscribe-mobile" onclick="questionRequest()" style="margin: 0;">Отправить</button>
                </div>
                <span class="uText">Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных и
                    ознакомлен(а) с условиями конфиденциальности.</span>
            </form>
        </section>
    </div>
    <!-- [/END MODAL] -->
        <?php
            include ('elements/footer.php');
        ?>
    </section>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script>
        $('.qaPage__accordion').accordion({
            collapsible: true,
            active: false,
            classes: {
                "ui-accordion-header": "qaPage__accordion_open",
                "ui-accordion-header-collapsed": "qaPage__accordion_closed",
                "ui-accordion-content": "qaPage__content"
            },
            beforeActivate: function(){
                var blocks = $( ".qaPage__accordion" ).not(this);
                blocks.each(function(index,block){
                    $(block).find(".ui-accordion-header-active").trigger("click");
                })
            }
        });

        function questionRequest() {
            var questionData = {};
                questionData.modalName = $('#modal-name').val().trim();
                questionData.modalEmail = $('#modal-email').val().trim();
                questionData.modalCity = $('#modal-city').val().trim();
                questionData.modalMessage = $('#modal-message').val().trim();
                let f = false;


                if (questionData.modalName == false && questionData.modalEmail == false && questionData.modalCity == false && questionData.modalMessage == false) {
                    f = 'Не все поля заполнены';
                } else if (questionData.modalEmail != +questionData.modalEmail || questionData.modalEmail == false) {
                    if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(questionData.modalEmail) == false) {
                        f = 'Не корректный email';
                    }
                } else if (questionData.modalEmail == +questionData.modalEmail) {
                    if (questionData.modalEmail.length != 11) {
                        f = 'Не корректный номер телефона';
                    }
                }
                if (f) {
                    new Toast({
                        message: f,
                        type: 'danger'
                    });
                } else {
                    $.post("/m/api/application.php", questionData).done(function(data) {
                        if (data.status == 1) {
                            console.log(data);
                            new Toast({
                                message: 'Сообщение отправленно!',
                                type: 'danger'
                            });
                            $('#modal-name').val('');
                            $('#modal-email').val('');
                            $('#modal-city').val('');
                            $('#modal-message').val('');
                        } else {
                            new Toast({
                                message: 'Не удалось отправить сообщение!',
                                type: 'danger'
                            });
                        }
                    });
                }
        }
    </script>
    <!-- [/SCRIPTS] -->
</body>
</html>