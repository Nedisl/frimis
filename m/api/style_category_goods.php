<?php
    include ('../elements/db.php');

    $category_id = $_POST['category_id'];
    $style_id =$_POST['style_id'];

    $good_res = mysqli_query($db, "SELECT g.*,gp.photo FROM `good` as g LEFT JOIN good_photo as gp ON g.id = gp.good_id AND gp.is_main = 1  WHERE g.category_id = $category_id AND g.style_id = $style_id");
    

    $goods_arr = array();

    while ($good_row = mysqli_fetch_assoc($good_res)) {
        $goods_arr[] = $good_row;
    }

    $good_json = json_encode($goods_arr, JSON_UNESCAPED_UNICODE);
    header('Content-type: application/json');
    echo $good_json;