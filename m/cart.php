
        <!-- [HEAD] -->
        <?php
            include ('elements/header.php');
        ?>
         <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'cart'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Корзина</title>
        <!-- [/END HEAD] -->
        <!-- [MAIN CONTENT] -->
        <main class="basketPage">
            <h1>Ваш заказ</h1>
            <section class="basketPage__itemList">
                        
            </section>
            <figure class="orangeBlock">
                <img src="img/deliveryMan.png" alt="">
                <div class="orangeBlock__wrapper">
                    <figcaption>
                        <h2>При заказе <br>на 2 000 руб.<br>доставка - бесплатно!</h2>
                    </figcaption>
                </div>
            </figure>
            <section class="basketPage__info">
                <a href="/m/page.php?id=delievery">
                    <figure>
                        <img src="img/deliveryCar.jpg" alt="">
                        <figcaption>Оплата и доставка</figcaption>
                    </figure>
                </a>
                <a href="/m/page.php?id=guarantee">
                    <figure>
                        <img src="img/deliveryGarant.jpg" alt="">
                        <figcaption>Гарантии и возврат</figcaption>
                    </figure>
                </a>
            </section>
            <h2>Оформление заказа</h2>
            <!-- <form class="deliveryForm"> -->
                <section class="deliveryForm__userInfo">
                        <input type="text" placeholder="Ваше имя*" id="name">
                        <input type="text" placeholder="Ваша фамилия*" id="lastname">
                        <input type="text" placeholder="Ваш e-mail*" id="email">
                        <input type="text" placeholder="Ваш телефон*" id="phone" class="phone_mask">
                    <!-- <input type="text" placeholder="Город*"> -->
                    <select name="" id="city" class="selectType">
                        <?php
                        $city_res = mysqli_query($db, "SELECT * FROM city");
                        
                        while ($city_row = mysqli_fetch_assoc($city_res)) {
                            ?>
                        <option value="<?=$city_row['code']?>" data-id="<?=$city_row['id']?>"><?=$city_row['name']?></option>
                        <?php
                        }
                            ?>
                    </select>
                    <select class="selectType" name="" id="type">
                        <option value="1">Забрать в магазине</option>
                        <option value="2">Доставка курьером, на дом</option>
                        <option value="3">Доставка в пункт выдачи</option>
                    </select>
                    <div class="deliveryForm__address">
                        <input type="text" placeholder="Улица" id="street">
                        <input type="text" placeholder="Дом" id="house">
                        <input type="text" placeholder="Кв./офис" id="apt">
                    </div>
                    <input type="text" placeholder="Почтовый индекс*" id="index">
                </section>
                <section class="deliveryForm__promo">
                
                <ul class="blockPrice">
                    <li >
                        <span class="blockPrice__caption">Стоимость доставки</span>
                        <span class="blockPrice__value"><small id="sdek__value" style="font-size:16px"></small> <small style="font-size:16px">руб.</small></span>
                    </li>
                    <li >
                        <span class="blockPrice__caption">Стоимость товаров</span>
                        <span id="totalPrice" class="blockPrice__value"> руб.</span>
                    </li>
                    <li style="font-family: GothamPro Bold,Verdana;">
                        <span class="blockPrice__caption">Сумма к оплате</span>
                        <span><b id="totalPrice_end" class="blockPrice__end__value"></b> <b>руб.</b></span>
                    </li>
                    <li>
                        <span class="blockPrice__caption">Время доставки</span>
                        <span class="blockPrice__value" id="sdek__time">0 д.</span>
                    </li>
                    <li>
                        <span class="blockPrice__caption">Способ оплаты</span>
                        <select class="type__payment">
                            <option value="">По карте</option>
                            <option value="">Наличными</option>
                        </select>
                    </li>
                </ul>

                <section class="sendOrder">
                    <div class="data-check"></div>
                    <div class="data-price" style="display: none">Оплата наличными от 2000 р.</div>
                    <button onclick="order()" id="order__btn">Отправить заказ</button>
                    <span>Нажимая на кнопку «Отправить заказ», я соглашаюсь на обработку персональных данных и ознакомлен(а) с условиями конфиденциальности. Если у вас есть вопросы, позвоните нам по номеру 8-888-888-88-88.</span>
                </section>
                </section>
            <!-- </form> -->
        </main>
        <!-- [/END CONTENT] -->

        <!-- [FOOTER] -->
        <?php include('elements/footer.php') ?>

<!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <script src="/js/toast.js"></script>
    <style>
        #city {
            justify-content: flex-start;
            background: none;
            width: 100%;
            height: 45px;
            border: 1px solid #878787;
            border-radius: 2px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            outline: none;
            font-size: 14px;
            font-family: GothamPro,Verdana;
            padding: 0 20px 0 15px;
            margin-bottom: 10px;
    }
    .data-check {
        padding-bottom: 8px;
    }
    .data-price {
        padding-bottom: 8px;
    }
    </style>
    <!-- Custom -->
    <script>
    var basket = localStorage.getItem("basket");
    var total = 0;
    var basketArray = { basket: [] };

    let orderWeight = ''
    let orderLength = ''
    let orderWidth = ''
    let orderHeight = ''
        
    $.get('/m/api/contacts.php?id=' + $('#city option:selected').data('id'), (res) => {

        if (res.length == 0) {
            $('#type').html(`
                <option value="2">Доставка курьером, на дом</option>
                <option value="3">Доставка в пункт выдачи</option>
            `)
            $("#type").selectmenu("destroy");
            $('#type').selectmenu({
                classes: {
                    'ui-selectmenu-button-closed': 'selectType_closed',
                    'ui-selectmenu-button-open': 'selectType_open',
                    'ui-selectmenu-menu': 'selectType__menu'
                },
                change: changeType
            });
            changeType()
            if ($("#type").val() == 1) {
        
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными  в магазине</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    },
                    change: changePrice
                });
                document.querySelector("#order__btn").disabled = false; 
                document.querySelector("#order__btn").style.opacity = '1'
                if (JSON.parse(basket).basket.length == 0) {
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                }
            } else {
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными  курьеру</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    },
                    change: changePrice
                });
                
            }
        } else {
            $('#type').html(`
                <option value="1">Забрать в магазине</option>       
                <option value="2">Доставка курьером, на дом</option>
                <option value="3">Доставка в пункт выдачи</option>
            `)
            $("#type").selectmenu("destroy");
            $('#type').selectmenu({
                classes: {
                    'ui-selectmenu-button-closed': 'selectType_closed',
                    'ui-selectmenu-button-open': 'selectType_open',
                    'ui-selectmenu-menu': 'selectType__menu'
                },
                change: changeType
            });
            changeType()
            if ($("#type").val() == 1) {
        
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными  в магазине</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    },
                    change: changePrice
                });
                document.querySelector("#order__btn").disabled = false; 
                document.querySelector("#order__btn").style.opacity = '1'
                if (JSON.parse(basket).basket.length == 0) {
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                }
            } else {
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными  курьеру</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    },
                    change: changePrice
                });
                
            }
        }
    })
 
    $.post('/m/api/post_api_code.php',{name: $('#city option:selected').html()}, (res) => {
        console.log(JSON.parse(res))
        let content = [] 
        let contentArr = []
        for (let i = 0; i < JSON.parse(res).content.length; i++) {
            content.push(JSON.parse(res).content[i].indexes)
            for (let k = 0; k < content[i].length; k++) {
                contentArr.push(content[i][k])
            }
            
        }
        console.log(contentArr)
        let arr = contentArr.map(element => element.toString());
        console.log(arr)
        var flowers = arr

        $('#index').autocomplete({
            source: flowers,
            maxHeight: 400,
            onSelect: changeType
        })

    })

    document.querySelector('#index').oninput = changeType
    document.querySelector('#index').onchange = changeType

    $('#city').selectmenu({
        change: function(event, ui) {
        
            $.post('/m/api/post_api_code.php',{name: $('#city option:selected').html()}, (res) => {
                console.log(JSON.parse(res))
                let content = [] 
                let contentArr = []
                for (let i = 0; i < JSON.parse(res).content.length; i++) {
                    content.push(JSON.parse(res).content[i].indexes)
                    for (let k = 0; k < content[i].length; k++) {
                        contentArr.push(content[i][k])
                    }
                    
                }
                console.log(contentArr)
                let arr = contentArr.map(element => element.toString());
                console.log(arr)
                var flowers = arr

                $('#index').autocomplete({
                    source: flowers,
                    maxHeight: 400,
                    onSelect: changeType
                })

            })

            $.get('/m/api/contacts.php?id=' + $('#city option:selected').data('id'), (res) => {

                if (res.length == 0) {
                    $('#type').html(`
                        <option value="2">Доставка курьером, на дом</option>
                        <option value="3">Доставка в пункт выдачи</option>
                    `)
                    $("#type").selectmenu("destroy");
                    $('#type').selectmenu({
                        classes: {
                            'ui-selectmenu-button-closed': 'selectType_closed',
                            'ui-selectmenu-button-open': 'selectType_open',
                            'ui-selectmenu-menu': 'selectType__menu'
                        },
                        change: changeType
                    });
                    changeType()
                    if ($("#type").val() == 1) {
        
                        $('.type__payment').html(`
                            <option value="1">По карте</option>
                            <option value="2">Наличными  в магазине</option>
                        `)
                        $(".blockPrice select").selectmenu("destroy");
                        $('.blockPrice select').selectmenu({
                            classes: {
                                'ui-selectmenu-button-closed': 'selectPrice_closed',
                                'ui-selectmenu-button-open': 'selectPrice_open',
                                'ui-selectmenu-menu': 'selectPrice__menu'
                            },
                            change: changePrice
                        });
                        document.querySelector("#order__btn").disabled = false; 
                        document.querySelector("#order__btn").style.opacity = '1'
                    } else {
                        $('.type__payment').html(`
                            <option value="1">По карте</option>
                            <option value="2">Наличными  курьеру</option>
                        `)
                        $(".blockPrice select").selectmenu("destroy");
                        $('.blockPrice select').selectmenu({
                            classes: {
                                'ui-selectmenu-button-closed': 'selectPrice_closed',
                                'ui-selectmenu-button-open': 'selectPrice_open',
                                'ui-selectmenu-menu': 'selectPrice__menu'
                            },
                            change: changeType
                        });
                        changeType()
                    }
                } else {
                    $('#type').html(`
                        <option value="1">Забрать в магазине</option>       
                        <option value="2">Доставка курьером, на дом</option>
                        <option value="3">Доставка в пункт выдачи</option>
                    `)
                    $("#type").selectmenu("destroy");
                    $('#type').selectmenu({
                        classes: {
                            'ui-selectmenu-button-closed': 'selectType_closed',
                            'ui-selectmenu-button-open': 'selectType_open',
                            'ui-selectmenu-menu': 'selectType__menu'
                        },
                        change: changeType
                    });
                    changeType()
                    if ($("#type").val() == 1) {
        
                        $('.type__payment').html(`
                            <option value="1">По карте</option>
                            <option value="2">Наличными  в магазине</option>
                        `)
                        $(".blockPrice select").selectmenu("destroy");
                        $('.blockPrice select').selectmenu({
                            classes: {
                                'ui-selectmenu-button-closed': 'selectPrice_closed',
                                'ui-selectmenu-button-open': 'selectPrice_open',
                                'ui-selectmenu-menu': 'selectPrice__menu'
                            },
                            change: changePrice
                        });
                        document.querySelector("#order__btn").disabled = false; 
                        document.querySelector("#order__btn").style.opacity = '1'
                    } else {
                        $('.type__payment').html(`
                            <option value="1">По карте</option>
                            <option value="2">Наличными  курьеру</option>
                        `)
                        $(".blockPrice select").selectmenu("destroy");
                        $('.blockPrice select').selectmenu({
                            classes: {
                                'ui-selectmenu-button-closed': 'selectPrice_closed',
                                'ui-selectmenu-button-open': 'selectPrice_open',
                                'ui-selectmenu-menu': 'selectPrice__menu'
                            },
                            change: changePrice
                        });
                        
                    }
                }
            })
        }
    })
        if (basket !== null && basket !== '') {
            var basketArray = JSON.parse(basket);

            basketArray.basket.forEach((el, index) => {
                console.log(index);
                $.get('/m/api/good.php?id=' + el.id, (res) => {
                    if (!el.color && res.colors !== null && res.colors[0] !== null && res.colors[0] !== undefined) {
                        el.color = res.colors[0].hex;
                        basketArray.basket[index].color = res.colors[0].hex;
                        localStorage.setItem('basket', JSON.stringify(basketArray));
                    }
                        
                    if (!el.size && res.sizes !== null && res.sizes[0] !== null && res.sizes[0] !== undefined) {
                        el.size = res.sizes[0].name;
                        basketArray.basket[index].size = res.sizes[0].name;
                        localStorage.setItem('basket', JSON.stringify(basketArray));
                    }

                    var sizes = '';
                    if(res.sizes !== null) {
                        res.sizes.forEach((size) => {
                            if (size.name != el.size) {
                                sizes += `<option value="${size.name}">${size.name}</option>`;
                                console.log('sz'+ el.size + ' ' + size.name)
                            } else {
                                console.log(size.name + ' ' + el.size)
                                sizes += `<option value="${el.size}" selected="selected">${el.size}</option>`;
                            }
                        });
                    }
                    total = total + (+el.price) * (+el.quantity);
                    $('#totalPrice').html(total  + ' руб.');
                    $('.basketPage__itemList').append(`
                        <figure class="goodItem" id="good${el.id}">
                            <div class="goodItem__wrapper">
                                <img src="<?=$pathAdm?>` + el.photo + `" alt="">
                                <figcaption>
                                    <a href=good.php?id="` + el.id + `">` + el.name + `</a>
                                    <span class="goodItem__id1c">Артикул: ` + el.art + `</span>
                                    <div class="goodItem__params">
                                        <span class="goodItem__color" style="background-color: ` + el.color + `;"></span>
                                        <select class="selectSize">
                                            ` + sizes + `
                                        </select>
                                    </div>
                                    <div class="itemCounter">
                                        <button class="itemCounter__minus" onclick="decreaseQuantity(${el.id})">-</button>
                                        <input id="q${el.id}" type="text" value="` + el.quantity + `">
                                        <button class="itemCounter__plus" onclick="increaseQuantity(${el.id})">+</button>
                                    </div>
                                    <span class="goodItem__price">` + el.price + ` руб.</span>
                                </figcaption>
                            </div>
                            <button class="goodItem__close" onclick="removeElement(${el.id})">
                                <i class="fas fa-times" aria-hidden="true"></i>
                            </button>
                        </figure>
                    `);      
                });
                goodCheck(el)   
            });
        }

        function goodCheck(el) {
            $.get('/api/good_check.php?id=' + el.id+'&qu=' + el.quantity, (res) => {
                changeType()
                if (JSON.parse(res) == 'falseIss') {
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                    document.querySelector('.data-check').innerHTML = `${el.name} нет на складе`
                  

                } else if (JSON.parse(res) == 'falseQu') {
                    
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                    document.querySelector('.data-check').innerHTML = `${el.name} недостаточно на складе`
       
                } else {
                    document.querySelector("#order__btn").disabled = false; 
                    document.querySelector("#order__btn").style.opacity = '1'
                    document.querySelector("#order__btn").style.opacity = '1'
                    document.querySelector('.data-check').innerHTML = ``
                    changeType()
                }
                
            })
        }

        $('#totalPrice').html(total  + ' руб.');
        function order() {
            var orderData = basketArray;
            orderData.name = $('#name').val().trim();
            orderData.lastname = $('#lastname').val().trim();
            orderData.email = $('#email').val().trim();
            orderData.phone = $('#phone').val().trim();
            orderData.city = $('#city').val();
            orderData.street = $('#street').val().trim();
            orderData.house = $('#house').val().trim();
            orderData.apt = $('#apt').val().trim();
            if ($('.type__payment').val() == '1') {
                orderData.type = '1';
            } else {
                orderData.type = '0';
            }
            orderData.payment = total - (+orderData.bonus);
            orderData.total = document.querySelector('#totalPrice_end').innerHTML;
            
            orderData.weight = orderWeight
            orderData.length = orderLength
            orderData.width = orderWidth
            orderData.height = orderHeight
            console.log(orderData)
            let f = false;
            
            if (orderData.name == false || orderData.lastname == false || orderData.email == false || orderData.phone == false || orderData.city == false || orderData.street == false || orderData.house == false || orderData.apt == false) {
                f = 'Не все поля заполненны';  
            } else if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(orderData.email) == false) {
                f = 'Не корректный email';
            } 
            if (f) {
                new Toast({
                    message: f,
                    type: 'danger'
                });
            } else {
                $.post("/m/api/order.php", orderData).done(function(data) {
                    if (data.status == 1) {
                        document.cookie = "name="+data.name;
                        document.cookie = "phone="+data.phone;
                        document.cookie = "email="+data.email;
                        document.cookie = "city="+data.city;
                        document.cookie = "street="+data.street;
                        document.cookie = "house="+data.house;
                        document.cookie = "apt="+data.apt;
                        document.cookie = "total="+data.total;
                        document.cookie = "basket="+JSON.stringify(data.basket);
                        document.cookie = "orderId="+data.id;
                        document.cookie = "weight="+orderWeight
                        document.cookie = "length="+orderLength
                        document.cookie = "width="+orderWidth
                        document.cookie = "height="+orderHeight
                        if ($('#type').val() == 2 || $('#type').val() == 3) {
                            document.cookie = "type=1";
                        } else {
                            document.cookie = "type=2";
                        }
                        
                        window.location.href = data.url;
                        
                    } else if (data.status == 0) {
                        new Toast({
                            message: 'Сбербанк не отвечает, попробуйте позже',
                            type: 'danger'
                        });
                    }
                });
            }
        }

        let price = $('#sdek__value').html()
        $('.blockPrice__end__value').html(+price + total)

        function increaseQuantity(i) {

            basketArray.basket.forEach(elem => {
                if (elem.id == i) {
                    elem.quantity++
                    $("#q" + i).val(elem.quantity)
                }
                goodCheck(elem)
            })
            localStorage.setItem('basket', JSON.stringify(basketArray));


            var basket = localStorage.getItem("basket");
            total = 0
            JSON.parse(basket).basket.forEach((el, index) => {
                // console.log(el)

                total = total + (+el.price) * (+el.quantity);
                $('#totalPrice').html(total + ' руб.');
            })   
            let price = $('#sdek__value').html()
            $('.blockPrice__end__value').html(+price + total)

            if ($('#type').val() != 1) {
                changePrice()
            }
        }
        function decreaseQuantity(i) {

            basketArray.basket.forEach(elem => {
                if (elem.quantity > 1) {
                    if (elem.id == i) {
                        elem.quantity--
                        $("#q" + i).val(elem.quantity)
                    }
                }
                goodCheck(elem)
            })

            localStorage.setItem('basket', JSON.stringify(basketArray));


            var basket = localStorage.getItem("basket");
            total = 0
            JSON.parse(basket).basket.forEach((el, index) => {
                // console.log(el)

                total = total + (+el.price) * (+el.quantity);
                $('#totalPrice').html(total + ' руб.');
            })  
            let price = $('#sdek__value').html()
            $('.blockPrice__end__value').html(+price + total)

            if ($('#type').val() != 1) {
                changePrice()
            }
        }

        function removeElement(i) {
            var basket = localStorage.getItem("basket");
            // console.log(basketArray.basket)
            // basketArray.basket.splice(i, 1); 
            basketArray.basket = basketArray.basket.filter(item => item.id != i);
            $("#good" + i).remove();
            localStorage.setItem('basket', JSON.stringify(basketArray));  
            basket = localStorage.getItem("basket");   
            console.log(basketArray)     
            console.log("a "+total)
            total = 0
            JSON.parse(basket).basket.forEach((el, index) => {
               

                total += (+el.price) * (+el.quantity);
                goodCheck(el)
                
            })  
            $('#totalPrice').html(total + ' руб.');
            console.log("b "+total)
            let price = $('#sdek__value').html()
            $('.blockPrice__end__value').html(+price + total)

            if ($('#type').val() != 1) {
                changePrice()
            }
            if (JSON.parse(basket).basket.length != 0) {
                document.querySelector("#order__btn").disabled = false; 
                document.querySelector("#order__btn").style.opacity = '1'
            } else {
                document.querySelector("#order__btn").disabled = true; 
                document.querySelector("#order__btn").style.opacity = '0.4'
            }
        }
    $('.selectSize').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectSize_closed',
            'ui-selectmenu-button-open': 'selectSize_open',
            'ui-selectmenu-menu': 'selectSize__menu'
        }
    });

    $('.blockPrice select').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectPrice_closed',
            'ui-selectmenu-button-open': 'selectPrice_open',
            'ui-selectmenu-menu': 'selectPrice__menu'
        }
    });

    $('.selectType').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectType_closed',
            'ui-selectmenu-button-open': 'selectType_open',
            'ui-selectmenu-menu': 'selectType__menu'
        }
    });

    changeType()




    if ($("#type").val() == 1) {
        
        $('.type__payment').html(`
            <option value="1">По карте</option>
            <option value="2">Наличными  в магазине</option>
        `)
        $(".blockPrice select").selectmenu("destroy");
        $('.blockPrice select').selectmenu({
            classes: {
                'ui-selectmenu-button-closed': 'selectPrice_closed',
                'ui-selectmenu-button-open': 'selectPrice_open',
                'ui-selectmenu-menu': 'selectPrice__menu'
            }
        });
        document.querySelector("#order__btn").disabled = false; 
        document.querySelector("#order__btn").style.opacity = '1'
    } else {
        $('.type__payment').html(`
            <option value="1">По карте</option>
            <option value="2">Наличными  курьеру</option>
        `)
        $(".blockPrice select").selectmenu("destroy");
        $('.blockPrice select').selectmenu({
            classes: {
                'ui-selectmenu-button-closed': 'selectPrice_closed',
                'ui-selectmenu-button-open': 'selectPrice_open',
                'ui-selectmenu-menu': 'selectPrice__menu'
            }
        });
        
    }




    $('.blockPrice select').selectmenu({
        change: changePrice
    })



    $('#type').selectmenu({
        change: changeType
    });




    function changePrice(event, ui) {
        let price = $('#sdek__value').html()
        console.log($('.blockPrice select').val())
        if ($('#type').val() != 1) {
            if (total < 2000) {
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    }
                });
            } else {
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными курьеру</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    }
                });
            }
        }
    }


    function changeType() {
            
            if ($("#type").val() == 2) {
                

                if (document.querySelector('#index').value.trim().length == 6) {

                    document.querySelector("#order__btn").disabled = false; 
                    document.querySelector("#order__btn").style.opacity = '1'
                    formSdek = {}
                    formSdek.index = $('#index').val()
                    formSdek.basketHeight = basketArray.basket.length

                    $.post("/m/api/sdek_api.php", formSdek).done(function(data) {
                        data = JSON.parse(data)
                        // console.log(data)
                        if (data == '0') {
                            $('#sdek__value').html('нет')
                            $('#sdek__time').html('нет')
                            $('.blockPrice__end__value').html(total)
                            document.querySelector("#order__btn").disabled = true; 
                            document.querySelector("#order__btn").style.opacity = '0.4'
                        } else {
                            orderWeight = data.weight
                            orderLength = data.length 
                            orderWidth = data.width
                            orderHeight = data.height
                            $('#sdek__value').html(data.price)
                            $('.blockPrice__end__value').html(data.price + total)
                            $('#sdek__time').html('1 д.')
                            document.querySelector("#order__btn").disabled = false; 
                            document.querySelector("#order__btn").style.opacity = '1'
                        }
                    })

                } else {
                    $('#sdek__value').html('нет')
                    $('#sdek__time').html('нет')
                    $('.blockPrice__end__value').html(total)
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                }

            } else if ($("#type").val() == 3) {

                if (document.querySelector('#index').value.trim().length == 6) {

                    document.querySelector("#order__btn").disabled = false; 
                    document.querySelector("#order__btn").style.opacity = '1'
                    formSdek = {}
                    formSdek.index = $('#index').val()
                    formSdek.basketHeight = basketArray.basket.length
                    
                    $.post("/m/api/sdek_api_prioritet.php", formSdek).done(function(data) {
                        data = JSON.parse(data)
                        if (data.length == 0) {
                            $('#sdek__value').html('нет')
                            $('#sdek__time').html('нет')
                            $('.blockPrice__end__value').html(total)
                            document.querySelector("#order__btn").disabled = true; 
                            document.querySelector("#order__btn").style.opacity = '0.4'
                        } else {
                            orderWeight = data.weight
                            orderLength = data.length 
                            orderWidth = data.width
                            orderHeight = data.height
                            console.log(orderHeight)
                            $('#sdek__value').html(data.price)
                            $('.blockPrice__end__value').html(data.price + total)
                            $('#sdek__time').html(data.time + ' д.')
                            document.querySelector("#order__btn").disabled = false; 
                            document.querySelector("#order__btn").style.opacity = '1'
                            if (JSON.parse(basket).basket.length == 0) {
                                document.querySelector("#order__btn").disabled = true; 
                                document.querySelector("#order__btn").style.opacity = '0.4'
                            }
                        }
                    });
                } else {
                    $('#sdek__value').html('нет')
                    $('#sdek__time').html('нет')
                    $('.blockPrice__end__value').html(total)
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                }

            } else {
                document.querySelector("#order__btn").disabled = false; 
                document.querySelector("#order__btn").style.opacity = '1'
                if (JSON.parse(basket).basket.length == 0) {
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                }
                $('#sdek__value').html('0')
                $('#sdek__time').html('0 д.')
                let price = $('#sdek__value').html()
                $('.blockPrice__end__value').html(+price + total)
            }

            if ($("#type").val() == 1) {
                document.querySelector('.data-price').style.display = 'none'
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными  в магазине</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    }
                });
                document.querySelector("#order__btn").disabled = false; 
                document.querySelector("#order__btn").style.opacity = '1'
                if (JSON.parse(basket).basket.length == 0) {
                    document.querySelector("#order__btn").disabled = true; 
                    document.querySelector("#order__btn").style.opacity = '0.4'
                }
            } else {
                $('.type__payment').html(`
                    <option value="1">По карте</option>
                    <option value="2">Наличными  курьеру</option>
                `)
                $(".blockPrice select").selectmenu("destroy");
                $('.blockPrice select').selectmenu({
                    classes: {
                        'ui-selectmenu-button-closed': 'selectPrice_closed',
                        'ui-selectmenu-button-open': 'selectPrice_open',
                        'ui-selectmenu-menu': 'selectPrice__menu'
                    }
                });

            }
            changePrice()
            $('.blockPrice select').selectmenu({
                change: changePrice
            })
        }
    </script>
    <!-- /Custom -->
    <!-- [/SCRIPTS] -->
</body>
</html>