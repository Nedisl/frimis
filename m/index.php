        <?php
            include ('elements/header.php');
        ?>
        
        <?php
            $collection_res = mysqli_query($db, "SELECT * FROM collection");
            $collection_main_res = mysqli_query($db, "SELECT * FROM collection WHERE is_main = 1");
            $category_res = mysqli_query($db, "SELECT * FROM category WHERE is_on_main_page = 1");
            $collection_main_row = mysqli_fetch_array($collection_main_res);


            $a = array();
            if(mysqli_num_rows($collection_res) > 0) {
                while ($row = mysqli_fetch_assoc($collection_res)) {
                    $a[] = $row;
                }
            }
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'index'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Frimis</title>
        <!-- [/END HEAD] -->

        <!-- [MAIN CONTAINER] -->
        <main class="homePage">
        <section class="firstSection">
                <section class="mainSlider owl-carousel">
                    <?php
                        for ($i = 0; $i < mysqli_num_rows($collection_res); $i++) {
                    ?>
                        <figure style="background: #9fa3af;">
                            <img src="<?=$pathAdm?><?= $a[$i]['background'] ?>" alt="">
                            <figcaption>
                                <span class="mainSlider__caption"><?php echo $a[$i]['season'] ?> <?php echo $a[$i]['year'] ?></span>
                                <h2><?php echo $a[$i]['title'] ?></h2>
                                <a href="/m/collection.php?id=<?php echo $a[$i]['id'] ?>">Смотреть</a>
                            </figcaption>
                        </figure>
                    <?php } ?>
                </section>
                <style>
                    .autumnLook:hover {
                        cursor: pointer;
                    }
                </style>
                <a href="/m/looks.php?id=1" class="autumnLook">
                    <figure>
                    <img src="<?=$pathAdm?><?php echo $collection_main_row['image'] ?>" alt="">
                        <div class="autumnLook__wrapper">
                            <figcaption>
                                <h2><?php echo $collection_main_row['call_to_action'] ?></h2>
                            </figcaption>
                        </div>
                    </figure>
                </a>
            </section>
            <section class="secondSection">
                <?php
                    $even = 1;
                    while ($category_row = mysqli_fetch_array($category_res)) {
                        if ($even % 2 != 0) {
                ?>
                    <a href="/m/category.php?id=<?=$category_row['id']; ?>" class="whiteBox top">
                        <img src="<?=$pathAdm?><?=$category_row['image'];?>" alt="">
                        <div class="whiteBox__caption">
                            <h3><?=$category_row['name'];?></h3>
                            <span>от <?=$category_row['price_start'];?> р.</span>
                            <i>Показать</i>
                        </div>
                    </a>

                    <?php } else { ?>

                    <a href="/m/category.php?id=<?=$category_row['id']; ?>" class="whiteBox bottom">
                        <img src="<?=$pathAdm?><?=$category_row['image'];?>" alt="">
                        <div class="whiteBox__caption">
                            <h3><?=$category_row['name'];?></h3>
                            <span>от <?=$category_row['price_start'];?> р.</span>
                            <i>Показать</i>
                        </div>
                    </a>

                <?php
                        }
                        $even++;
                    }
                ?>
                <figure class="orangeBlock">
                    <img src="img/orangeBlock_girl1.png" alt="">
                    <div class="orangeBlock__wrapper">
                        <figcaption>
                            <h2>Готовые образы<br>от стилиста</h2>
                            <a href="/m/looks.php?id=1">Смотреть</a>
                        </figcaption>
                    </div>
                </figure>
            </section>
        </main>
        <!-- [/MAIN CONTAINER] -->

        <!-- [FOOTER] -->
        <?php
            include ('elements/footer.php');
        ?>
        <!-- [/END FOOTER] -->
    </section>

    <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>

    <script>
        /* == [OWL SLIDER] == */
        $('.mainSlider').owlCarousel({
            items: 1,
            loop: true,
            lazyLoad: true,
            autoplay: true,
            autoplayHoverPause: true,
            nav: false,
            dots: true
        });
    </script>
    <!-- [/SCRIPTS] -->
</body>
</html>