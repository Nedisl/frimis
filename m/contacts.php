
        <!-- [HEAD] -->
        <head>
            <script src="https://api-maps.yandex.ru/2.1/?apikey=a9fca58e-e7fc-4cd6-8114-49bcceb70a65&lang=ru_RU" type="text/javascript">
            </script>
        </head>
        <?php
            include ('elements/header.php');

            $city_res = mysqli_query($db, 'SELECT * FROM city');
            $arr = array();
            if(mysqli_num_rows($city_res) > 0) {
                while ($city_row = mysqli_fetch_assoc($city_res)) {
                    $arr[] = $city_row;
                }
            }
        ?>
        <?php 
            
            $meta_res = mysqli_query($db, "SELECT * FROM seo_meta WHERE page = 'contacts'");
            $meta_row = mysqli_fetch_assoc($meta_res);
        ?>
        <meta name="keywords" content="<?php echo $meta_row['keywords'];?>">
        <meta name="description" content="<?php echo $meta_row['description'];?>">
        <meta name="robots" content="<?php echo $meta_row['robots'];?>">
        <title>Контакты</title>
        <!-- [/END HEAD] -->

        <!-- [MAIN CONTENT] -->
        <!-- [/End Head] -->

        <!-- [MAIN CONTENT] -->
        <main class="contactsPage">
            <h1>Контакты</h1>
            <section id="map" style="width: 100%; height: 500px">
                <!-- <img src="uploads/testMap.jpg" width="100%" alt=""> -->
            </section>
            <section class="headContacts">
                <?php
                $contacts_res = mysqli_query($db, "SELECT * FROM contacts");
                while ($contacts_row = mysqli_fetch_array($contacts_res)) {
                ?>
                <figure>
                    <i class="fas fa-phone-alt"></i>
                    <span></i><?=$contacts_row['phone']?></span>
                </figure>
                <figure>
                    <i class="far fa-envelope"></i>
                    <span><?=$contacts_row['email']?></span>
                </figure>
            <?php } ?> 
            </section>
            <h2 class="market_sdek"></h2>
            <select name="" id="ch_city" class="selectCity">
                <?php
                        for ($i = 0; $i < mysqli_num_rows($city_res); $i++) {
                    ?>
                    <option value="<?php echo $arr[$i]['id']?>"><?php echo $arr[$i]['name']?></option>                           
                <?php } ?>     
            </select>
            <section class="contactsPage__wrapper">
                
            </section>
            <button class="contactsPage__callMe" data-modal=".modal__question">Задать вопрос</button>
            <button class="orangeButton" data-modal=".modal__callMe">Позвоните мне</button>
            <button class="orangeButton" data-modal=".modal__promoBonus">Бонусный код</button>
        </main>
        <!-- [/END CONTENT] -->

        <!-- [FOOTER] -->
        <?php
            include ('elements/footer.php');
        ?>
        <!-- [/END FOOTER] -->
    </section>
    
    <!-- [MODALS] -->
    <div class="modalWrapper">
        <section class="modal modal_medium modal__question">
            <h3>Задать вопрос</h3>
            <form onsubmit="return false;">
                <div class="form-box">
                    <input type="text" placeholder="Ваше имя" id="modal-name">
                    <input type="text" placeholder="Ваш телефон | e-mail" id="modal-email">
                    <input type="text" placeholder="Ваш город" id="modal-city">
                    <input type="text" placeholder="Ваше сообщение" id="modal-message">
                </div>
                <div class="form-box">
                    <!-- <button class="sendFile">Выбрать файл</button>
                    <input type="file" id="modal-file"> -->
                    <button class="buttonSubscribe-mobile" onclick="questionRequest()" style="margin: 0;">Отправить</button>
                </div>
                <span class="uText">Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных и
                    ознакомлен(а) с условиями конфиденциальности.</span>
            </form>
        </section>

        <section class="modal modal_mini modal__callMe">
            <h3>Позвоните мне</h3>
            <form action="">
                <div class="form-box">
                    <input type="text" placeholder="Ваше имя">
                    <input type="text" placeholder="Ваш телефон">
                    <input type="submit" value="Отправить">
                </div>
                <span class="uText">Нажимая на кнопку «Отправить», я соглашаюсь на обработку персональных данных и
                    ознакомлен(а) с условиями конфиденциальности.</span>
            </form>
        </section>

        <section class="modal modal_mini modal__promoBonus">
            <h3>Потратить бонусные баллы</h3>
            <form action="">
                <div class="form-box">
                    <span class="hText">
                        Введите, пожалуйста, код.<br>
                        Код отправлен на указанный <br>вами номер.
                    </span>
                    <input type="text" placeholder="Ваш код">
                    <input type="submit" value="Отправить">
                </div>
            </form>
        </section>
    </div>
    <!-- [/MODALS] -->

     <!-- [SCRIPTS] -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="libs/owlcarousel/owl.carousel.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
    cityUrl = '/api/contacts.php?id=1';

    var zoom = 10
    var zoomMark = 17 

    $('#ch_city').selectmenu({
        change: function (event, ui) {
            city = $("#ch_city option:selected").val();
            //console.log(city);
            cityUrl = '/m/api/contacts.php?id='+city;
            $('.contactsPage__wrapper').html('');

            $.get(cityUrl, (res) => {
                myCollection.removeAll();
                ymaps.geocode($('#ch_city option:selected').text())
                .then(function (res) {
                    myMap.setCenter(res.geoObjects.get(0)
                        .geometry.getCoordinates(), zoom);
                });
                $('.market_sdek').html('Магазины в:');
                res.forEach((el) => {
                        console.log(el)
                        myCollection.add(new ymaps.Placemark([+el.lat,+el.lng]));

                        $('.contactsPage__wrapper').append(`
                        <article>
                            <a onclick="myMap.setCenter([${+el.lat}, ${+el.lng}], ${zoomMark});">`+el.place+`</a>
                            <ul>
                                <li>
                                    <i class="fas fa-map-marker-alt"></i>
                                    <span>`+el.address+`</span>
                                </li>
                                <li class="phone-li">
                                    <i class="fas fa-phone-alt"></i>
                                    <span>`+el.phone+`</span>
                                </li>
                                <li>
                                    <i class="far fa-clock"></i>
                                    <span>`+el.pfrom+` - `+el.to+`</span>
                                </li>
                            </ul>
                        </article>`);
                    });

                    if (res.length == 0) {
                        $('.market_sdek').html('Пункты выдачи в:');
                        sdekUrl = `/m/api/city.php?id=${city}`;
                        $.get( sdekUrl, (res) => {
                            res = JSON.parse(res)
                            myCollection.removeAll();
                            res.forEach((el) => {
                                console.log(res)
                                myCollection.add(new ymaps.Placemark([+el.coordY,+el.coordX]));
                                $('.contactsPage__wrapper').append(`
                                    <article>
                                        <a onclick="myMap.setCenter([${+el.coordY}, ${+el.coordX}], ${zoomMark});">${el.Name}</a>
                                        <ul>
                                            <li>
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span>${el.Address}</span>
                                            </li>
                                            <li class="phone-li">
                                                <i class="fas fa-phone-alt"></i>
                                                <span>${el.Phone}</span>
                                            </li>
                                            <li>
                                                <i class="far fa-clock"></i>
                                                <span>${el.WorkTime}</span>
                                            </li>
                                        </ul>
                                    </article>
                                `);
                            });
                        })
                    }

                });
        }
    });
    // Создание карты.
    var myMap;
    var myCollection;
    var coords = [];

    ymaps.ready(init);
    function init(){

        city = $("#ch_city option:selected").val();
            //console.log(city);
        cityUrl = '/m/api/contacts.php?id='+city;

        ymaps.geocode($('#ch_city option:selected').text())
            .then(function (result) {
                console.log(result)
                myMap = new ymaps.Map('map', {
                    center: result.geoObjects.get(0)
                        .geometry.getCoordinates(),
                    zoom: zoom
                });

                myCollection = new ymaps.GeoObjectCollection({}, {
                    preset: 'islands#redIcon', //все метки красные
                    draggable: false // и их можно перемещать
                });

                for (var i = 0; i < coords.length; i++) {
                    myCollection.add(new ymaps.Placemark(coords[i]));
                }

                myMap.geoObjects.add(myCollection);
                myMap.behaviors.disable('drag');
                myMap.behaviors.disable('scrollZoom')
                $.get(cityUrl, (res) => {
           
                    $('.market_sdek').html('Магазины в:');

                    res.forEach((el) => {

                        myCollection.add(new ymaps.Placemark([+el.lat,+el.lng]));

                        $('.contactsPage__wrapper').append(`
                            <article>
                                <a onclick="myMap.setCenter([${+el.lat}, ${+el.lng}], ${zoomMark});">${el.place}</a>
                                <ul>
                                    <li>
                                        <i class="fas fa-map-marker-alt"></i>
                                        <span>${el.address}</span>
                                    </li>
                                    <li class="phone-li">
                                        <i class="fas fa-phone-alt"></i>
                                        <span>${el.phone}</span>
                                    </li>
                                    <li>
                                        <i class="far fa-clock"></i>
                                        <span>${el.from} - ${el.to}</span>
                                    </li>
                                </ul>
                            </article>`);

                    });

                    if (res.length == 0) {
                        $('.market_sdek').html('Пункты выдачи в:');
                        sdekUrl = `/m/api/city.php?id=${city}`;
                        $.get( sdekUrl, (res) => {
                            res = JSON.parse(res)
                            myCollection.removeAll();
                            res.forEach((el) => {
                                console.log(res)
                                myCollection.add(new ymaps.Placemark([+el.coordY,+el.coordX]));
                                $('.contactsPage__wrapper').append(`
                                    <article>
                                        <a onclick="myMap.setCenter([${+el.coordY}, ${+el.coordX}], ${zoomMark});">${el.Name}</a>
                                        <ul>
                                            <li>
                                                <i class="fas fa-map-marker-alt"></i>
                                                <span>${el.Address}</span>
                                            </li>
                                            <li class="phone-li">
                                                <i class="fas fa-phone-alt"></i>
                                                <span>${el.Phone}</span>
                                            </li>
                                            <li>
                                                <i class="far fa-clock"></i>
                                                <span>${el.WorkTime}</span>
                                            </li>
                                        </ul>
                                    </article>
                                `);
                            });
                        })
                    }

                });

            });
    }


    /* == [SELECT UI] == */
    $('.selectCity').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectCity_closed',
            'ui-selectmenu-button-open': 'selectCity_open',
            'ui-selectmenu-menu': 'selectCity__menu'
        }
    });

    $('.selectSize').selectmenu({
        classes: {
            'ui-selectmenu-button-closed': 'selectSize_closed',
            'ui-selectmenu-button-open': 'selectSize_open',
            'ui-selectmenu-menu': 'selectSize__menu'
        }
    });
    </script>
    <script>
         function questionRequest() {
            var questionData = {};
                questionData.modalName = $('#modal-name').val().trim();
                questionData.modalEmail = $('#modal-email').val().trim();
                questionData.modalCity = $('#modal-city').val().trim();
                questionData.modalMessage = $('#modal-message').val().trim();
                let f = false;


                if (questionData.modalName == false && questionData.modalEmail == false && questionData.modalCity == false && questionData.modalMessage == false) {
                    f = 'Не все поля заполнены';
                } else if (questionData.modalEmail != +questionData.modalEmail || questionData.modalEmail == false) {
                    if (/^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(questionData.modalEmail) == false) {
                        f = 'Не корректный email';
                    }
                } else if (questionData.modalEmail == +questionData.modalEmail) {
                    if (questionData.modalEmail.length != 11) {
                        f = 'Не корректный номер телефона';
                    }
                }
                if (f) {
                    new Toast({
                        message: f,
                        type: 'danger'
                    });
                } else {
                    $.post("/m/api/application.php", questionData).done(function(data) {
                        if (data.status == 1) {
                            console.log(data);
                            new Toast({
                                message: 'Сообщение отправленно!',
                                type: 'danger'
                            });
                            $('#modal-name').val('');
                            $('#modal-email').val('');
                            $('#modal-city').val('');
                            $('#modal-message').val('');
                        } else {
                            new Toast({
                                message: 'Не удалось отправить сообщение!',
                                type: 'danger'
                            });
                        }
                    });
                }
        }
    </script>
    <style>
        .phone-li {
            display: flex;
        }
    </style>
    <!-- [/SCRIPTS] -->
</body>

</html>