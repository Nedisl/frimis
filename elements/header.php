<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/fancybox-master/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="styles.min.css">
    <link rel="stylesheet" href="styles.css">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="libs/owlcarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="css/toast.css">
    <link rel="stylesheet" href="css/additional.css">
    <link rel="stylesheet" href="libs/owlcarousel/owl.theme.default.min.css">
    <script src="https://kit.fontawesome.com/c6f437d80c.js" crossorigin="anonymous"></script>
    <script>
        function search() {
            var q = $('#searchInput').val();
            if (q != '') {
                window.location.href = '/search.php?search=' + q;
            }
        }
    </script>
</head>
<body>
        <?php
        $pathAdm = 'http://frimisadm.onoartsr.beget.tech/storage/';
        ?>
    <div class="mainWrapper">
        <!-- [HEAD] -->
        <header>
            <!-- [Top Nav] -->
            <nav>
                <li><a href="looks.php?id=1">Готовые образы</a></li>
                <li><a href="instashop.php">Instashop</a></li>
                <li>
                    <a href="#">Покупателям</a>
                    <ul>
                        <li><a href="stepsBuy.php">Как заказать</a></li>
                        <li><a href="page.php?id=pay">Оплата</a></li>
                        <li><a href="page.php?id=bonus">Бонусная программа</a></li>
                        <li><a href="page.php?id=delievery">Доставка</a></li>
                        <li><a href="page.php?id=guarantee">Гарантии</a></li>
                        <li><a href="page.php?id=refund">Возврат</a></li>
                        <li><a href="qaPage.php">Вопрос-ответ</a></li>
                        <li><a href="reviews.php">Отзывы</a></li>
                        <li><a href="about.php">О компании</a></li>
                        <li><a href="page.php?id=privacy">Политика конфиденциальности</a></li>
                    </ul>
                </li>
                <li><a href="contacts.php">Контакты</a></li>
                <li><a href="franchise.php">Франшиза</a></li>
            </nav>
            <!-- [/End Nav] -->

            <!-- [Logo Panel] -->
            <section class="headBlock">
                <a href="/"><img src="img/logo.jpg" alt=""></a>
                <menu>
                    <li>
                        <form class="searchBox" action="">
                            <input id="searchInput" type="text" placeholder="Введите товар...">
                            <button onclick="search()"><i class="fas fa-search"></i></button>
                        </form>
                    </li>
                    <li>
                        <button onclick="window.location.href = 'eye.php'" class="goodsHistory__show"  id="goods-eye"><i class="far fa-eye"></i></button>
                        <section class="goodsHistory">

                        </section>
                    </li>
                    <li>
                        <button onclick="window.location.href = 'heart.php'" class="choosenHistory__show"  id="goods-heart"><i class="far fa-heart"></i></button>
                        <section class="choosenHistory">

                        </section>
                    </li>
                    <li>
                        <button onclick="window.location.href = 'cart.php'" class="cartHistory__show"  id="goods-cart"><i class="fas fa-shopping-basket"></i></button>
                        <section class="cartHistory">

                        </section>
                    </li>
                </menu>
            </section>
            <script>
            let viewed = { viewedArray: [] };
            let choosen = { choosenArray: [] };
            let cart = { cartArray: [] };
            let arrval = document.querySelector('.goodsHistory');
            let arrvalch = document.querySelector('.choosenHistory');
            let arrvalca = document.querySelector('.cartHistory');

            document.querySelector('.goodsHistory').onmouseleave = function() {
                document.querySelector('.goodsHistory').style.display = 'none';
            }
            document.querySelector('.choosenHistory').onmouseleave = function() {
                document.querySelector('.choosenHistory').style.display = 'none';
            }
            document.querySelector('.cartHistory').onmouseleave = function() {
                document.querySelector('.cartHistory').style.display = 'none';
            }

            document.querySelector('.headBlock').onmouseleave = function() {
                document.querySelector('.goodsHistory').style.display = 'none';
                document.querySelector('.choosenHistory').style.display = 'none';
                document.querySelector('.cartHistory').style.display = 'none';
            }


            if (localStorage.getItem('choosen')) {
                choosenArray = JSON.parse(localStorage.getItem('choosen'));
                choosen = choosenArray; 
            } else {
                choosenArray = {};
                choosen = choosenArray;
            }

            if (localStorage.getItem('basket')) {
                cartArray = JSON.parse(localStorage.getItem('basket'));
                cart = cartArray;
            } else {
                cartArray = {};
                cart = cartArray;
            }
            
            if (localStorage.getItem('viewed')) {
                viewedArray = JSON.parse(localStorage.getItem('viewed'));
                viewed = viewedArray;
            } else {
                viewedArray = {};
                viewed = viewedArray;
            }
           
            let charr = [];
            if (choosenArray.choosen != undefined) {
                for (let i = 0; i < choosenArray.choosen.length; i++) {
                    charr.push(choosenArray.choosen[i].id)  
                }
                console.log(charr)
            }
            

            document.querySelector('#goods-cart').onmouseenter = function() {
                document.querySelector('.goodsHistory').style.display = 'none';
                document.querySelector('.choosenHistory').style.display = 'none';
                let out = '';

                console.log(cart['basket'])
                if (cart['basket'] == undefined || !localStorage.getItem('basket') || cart['basket'].length == 0) {
                    $('.cartHistory').hide();
                } else {
                    $('.cartHistory').show();
                }
                
                if (localStorage.getItem('basket')) {
                    for (let i = 0; i < cart['basket'].length; i++) {
                        if (cart["basket"][i]["photo"] == null) {
                            cart["basket"][i]["photo"] = 'img/box.jpg';
                        }
                        out +=`<figure id="good${cart["basket"][i]["id"]}">`;
                        out += '<a href="good.php?id='+cart["basket"][i]["id"]+'"><img src="<?=$pathAdm?>'+cart["basket"][i]["photo"]+'" alt=""></a>';
                        out += '<figcaption>';
                        out += '<a href="good.php?id='+cart['basket'][i]['id']+'">'+cart['basket'][i]['name']+'</a>';
                        out += '<span>Артикул: '+cart['basket'][i]['art']+'</span>';
                        out += '<strong>'+cart['basket'][i]['price']+'  руб.</strong>';
                        out += '</figcaption>';
                        out += `<div class="goodsHistory__panel">
                                    <button onclick='putToChoosen(\`${JSON.stringify(cart["basket"][i])}\`, \`\`, \`\`)'><i class="far fa-heart"></i></button>
                                    <button onclick="removeElement(${cart["basket"][i]["id"]})"><i class="fas fa-times"></i></button>
                                </div>`;
                        out +='</figure>';
                    }
                } 
                
                arrvalca.innerHTML = out;
            }
             

            document.querySelector('#goods-eye').onmouseenter = function() {
                document.querySelector('.choosenHistory').style.display = 'none';
                document.querySelector('.cartHistory').style.display = 'none';
                let out = '';

                if (viewed['viewed'] == undefined || !localStorage.getItem('viewed') || viewed['viewed'].length == 0) {
                    $('.goodsHistory').hide();
                } else {
                    $('.goodsHistory').show();
                }
            

                if (localStorage.getItem('viewed')) {
                    for (let i = 0; i < viewed['viewed'].length; i++) {
                        if (viewed["viewed"][i]["photo"] == null) {
                            viewed["viewed"][i]["photo"] = 'img/box.jpg';
                        }
                        out +=`<figure id="good${viewed['viewed'][i]['id']}">`;
                        out += '<a href="good.php?id='+viewed["viewed"][i]["id"]+'"><img src="<?=$pathAdm?>'+viewed["viewed"][i]["photo"]+'" alt=""></a>';
                        out += '<figcaption>';
                        out += '<a href="good.php?id='+viewed['viewed'][i]['id']+'">'+viewed['viewed'][i]['name']+'</a>';
                        out += '<span>Артикул: '+viewed['viewed'][i]['art']+'</span>';
                        out += '<strong>'+viewed['viewed'][i]['price']+'  руб.</strong>';
                        out += '</figcaption>';
                        out += `<div class="goodsHistory__panel">
                                    <button id="choosColor`+viewed['viewed'][i]['id']+`" onclick='putToChoosen(\`${JSON.stringify(viewed["viewed"][i])}\`, \`\`, \`\`)'><i class="far fa-heart"></i></button>
                                    <button onclick='putToBasket(\`${JSON.stringify(viewed["viewed"][i])}\`, \`\`, \`\`)'><i class="fas fa-shopping-basket"></i></button>
                                    <button onclick="removeElementEye(${viewed['viewed'][i]['id']})"><i class="fas fa-times"></i></button>
                                </div>`;
                        out +='</figure>';
                    }
                } 
                
                arrval.innerHTML = out;
            }
            
            document.querySelector('#goods-heart').onmouseenter = function() {
                document.querySelector('.goodsHistory').style.display = 'none';
                document.querySelector('.cartHistory').style.display = 'none';
                let out = '';
             
                       
                if (choosen["choosen"] == undefined || !localStorage.getItem('choosen') || choosen["choosen"].length == 0) {
                    $('.choosenHistory').hide();
                } else {
                    $('.choosenHistory').show();
                }

                if (localStorage.getItem('choosen')) {
                    for (let i = 0; i < choosen['choosen'].length; i++) {
                        if (choosen["choosen"][i]["photo"] == null) {
                            choosen["choosen"][i]["photo"] = 'img/box.jpg';
                        }
                        out +=`<figure id="good${choosen["choosen"][i]["id"]}">`;
                        out += '<a href="good.php?id='+choosen["choosen"][i]["id"]+'"><img src="<?=$pathAdm?>'+choosen["choosen"][i]["photo"]+'" alt=""></a>';
                        out += '<figcaption>';
                        out += '<a href="good.php?id='+choosen["choosen"][i]['id']+'">'+choosen["choosen"][i]['name']+'</a>';
                        out += '<span>Артикул: '+choosen["choosen"][i]['art']+'</span>';
                        out += '<strong>'+choosen["choosen"][i]['price']+'  руб.</strong>';
                        out += '</figcaption>';
                        out += `<div class="goodsHistory__panel">
                                    <button onclick='putToBasket(\`${JSON.stringify(choosen["choosen"][i])}\`, \`\`, \`\`)'><i class="fas fa-shopping-basket"></i></button>
                                    <button onclick="removeElementHeart(${choosen["choosen"][i]["id"]})"><i class="fas fa-times"></i></button>
                                </div>`;
                        out +='</figure>';
                    }
                } 
                arrvalch.innerHTML = out;
            } 

            function removeElement(i) {
                // cartArray.basket.splice(i, 1); 
                cartArray.basket = cartArray.basket.filter(item => item.id != i);
                $("#good" + i).remove();
                localStorage.setItem('basket', JSON.stringify(cartArray));
                if (cart['basket'].length == 0 || !localStorage.getItem('basket')) {
                    $('.cartHistory').hide();
                } else {
                    $('.cartHistory').show();
                }
            }  

            function removeElementEye(i) {
                // viewed["viewed"].splice(i, 1); 
                viewed["viewed"] = viewed["viewed"].filter(item => item.id != i);
                $("#good" + i).remove();
                localStorage.setItem('viewed', JSON.stringify(viewed));
                if (viewed['viewed'].length == 0 || !localStorage.getItem('viewed')) {
                    $('.goodsHistory').hide();
                } else {
                    $('.goodsHistory').show();
                }

            }

            function removeElementHeart(i) {
                // choosen["choosen"].splice(i, 1); 
                choosen["choosen"] = choosen["choosen"].filter(item => item.id != i);
                $("#good" + i).remove();
                localStorage.setItem('choosen', JSON.stringify(choosen));
                
            }


            function putToBasket(element, size, color) {
                var element = JSON.parse(element);
                element.quantity = 1;
                element.color = color;
                element.size = size;
                console.log(element.size + ' ' + element.color);
                var basket = localStorage.getItem("basket");
                var basketArray = { basket: [] };
                
                if (basket !== null && basket !== '') {
                    var basketArray = JSON.parse(basket);
                    var index = basketArray.basket.findIndex(el => el.id === element.id && el.color === color && el.size === size);
                    console.log(element);
                    if (index !== null && index !== -1) {
                        console.log('in:' + index);
                        basketArray.basket[index].quantity = +basketArray.basket[index].quantity + 1;
                        console.log(element.size);
                        new Toast({
                            message: 'Товар успешно добавлен в корзину',
                            type: 'danger'
                        });
                    } else {
                        console.log(element.size);
                        basketArray.basket.push(element);
                        new Toast({
                            message: 'Товар успешно добавлен в корзину',
                            type: 'danger'
                        });
                    }
                } else {
                    console.log(element.size);
                    basketArray.basket.push(element);
                    new Toast({
                        message: 'Товар успешно добавлен в корзину',
                        type: 'danger'
                    });
                }
                
                localStorage.setItem('basket', JSON.stringify(basketArray));
            }

            function putToChoosen(element) {
                var element = JSON.parse(element);
                var choosen = localStorage.getItem("choosen");
                var choosenArray = { choosen: [] };
                
                if (choosen !== null && choosen !== '') {
                    var choosenArray = JSON.parse(choosen);
                    var index = choosenArray.choosen.findIndex(el => el.id === element.id);
                    if (index === null || index === -1) {
                        $("#choosColor" + element.id).css("color", "#ff4625");
                        console.log(element.id);
                        choosenArray.choosen.push(element);
                        new Toast({
                            message: 'Товар успешно добавлен в избранное',
                            type: 'danger'
                        });
                    } else {
                        $("#choosColor" + element.id).css("color", "rgb(73, 73, 73)");
                        choosenArray.choosen.splice(index, 1); 
                        new Toast({
                            message: 'Товар удален из избранного',
                            type: 'danger'
                        });
                    }
                } else {
                    console.log(element.id);
                    $("#choosColor" + element.id).css("color", "#ff4625");
                    choosenArray.choosen.push(element);
                    new Toast({
                            message: 'Товар успешно добавлен в избранное',
                            type: 'danger'
                    });
                }
                
                localStorage.setItem('choosen', JSON.stringify(choosenArray));
            }
            </script>
            <!-- [/End Panel] -->
        </header>
        <!-- [/END HEAD] -->