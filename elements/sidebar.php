 <?php
    include ("db.php");
    $categories_res = mysqli_query($db, "SELECT * FROM category");
?>
<!-- $categories_row['is_full_on_sidebar'] == 1 AND  -->
 <!-- [LEFT SIDE MENU] -->
 <aside>
                <nav class="menuBar">
                    <li><a href="/category.php?id=all&new=1&look=0&color=0">НОВИНКИ</a></li>
                    <li><a href="/category.php?id=all&best=1&look=0&color=0">ЛУЧШИЕ ПРЕДЛОЖЕНИЯ</a></li>
                    <li><a href="/category.php?id=all&sale=1&look=0&color=0">РАСПРОДАЖА</a></li>
                    <?php 
                        while ($categories_row = mysqli_fetch_array($categories_res)) { 
                            if ($categories_row['root_id'] == 0) { 
                                ?>
                            <?php 
                                $cat_id = $categories_row['id']; 
                                $goods_res = mysqli_query($db, "SELECT * FROM category WHERE root_id = $cat_id"); 
                                ?>
                            <li>
                            <a class="menuBar__more"><?php echo $categories_row['name']; ?></a>
                                <ul>
                                    <?php 
                                        while ($goods_row = mysqli_fetch_array($goods_res)) {  
                                            ?>
                                        <li><a href="/category.php?id=<?= $goods_row['id'] ?>"><?php echo $goods_row['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        <?php }
                        } ?>
                    <li>    
                    </li>
                </nav>
            </aside>
            <style>
                .menuBar__more:hover {
                    cursor: pointer;
                }
                .mainWrapper .contentWrapper > aside .menuBar > li:nth-last-child(2) {
                    margin-top: 0px !important;
                }
            </style>
            <!-- [/END MENU] -->