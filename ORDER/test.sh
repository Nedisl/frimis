#!/bin/bash

curl -X POST \
  http://localhost:8888 \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: 707' \
  -H 'Content-Type: application/json' \
  -H 'Host: localhost:8888' \
  -H 'Postman-Token: 731442ca-6ec2-4a59-9d22-3ace1ef79b57,be1c5381-b45c-4e6b-b84c-19d7241f7180' \
  -H 'User-Agent: PostmanRuntime/7.19.0' \
  -H 'cache-control: no-cache' \
  -d '{
    "CustomerFullname": "Sharipov Vlad Ignatevich",
    "CustomerPhone": "+79990121234",
    "CustomerEmail": "go@boeu.ru",
    "CustomerCityCode": 270,
    "CustomerCityPostalCode": "420500",
    "SenderAddressStreet": "Холодильная",
    "SenderAddressHouse": "113",
    "SenderAddressFlat": "13",
    "SenderCityCode": 13,
    "SenderCityPostalCode": "320500",
    "DeliveryPvzCode": "NSK67",
    "OrderID": "1992189",
    "DeliveryNumber": "kekNumber1",
    "PackageWeight": 1000,
    "Items": [
        {
            "WareKey": "NN0001",
            "Cost": 500,
            "Weight": 120,
            "Amount": 2,
            "Comment": "Tesla SuperCar [Required Comment]"
        }
    ]
}'

# update OrderID if fails