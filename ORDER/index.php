<?php
include ('../elements/db.php');

$name = $_POST['name'];
$phone = $_POST['phone'];
$email = $_POST['email'];
$city = $_POST['city'];
$street = $_POST['street'];
$house = $_POST['house'];
$apt = $_POST['apt'];
$last_id = $_POST['last_id'];
$weight = $_POST['weight'];
$basket = json_decode($_POST['goods']);


$items = array();
$items_item = array();

foreach ($basket as $value) { 
    // $good_id = $value['id'];
    // $good_price = $value['price'];
    // $quantity = $value['quantity'];

    $items_item['WareKey'] = $value['id'];
    $items_item['Cost'] = $value['price'];
    $items_item['Weight'] = $weight;
    $items_item['Amount'] = $value['quantity'];
    $items_item['Comment'] = 'Tesla SuperCar [Required Comment]';

    $items[] = $items_item;
} 





ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use CdekSDK\Common;
use CdekSDK\Requests;

require 'vendor/autoload.php';

$appConfig = new stdClass();

// TODO: set correct tariff code
// TODO: change to production configs

$appConfig->tariffCode = 138;

$appConfig->orderIDPrefix = 'IM1337_';

$appConfig->account = 'z9GRRu7FxmO53CQ9cFfI6qiy32wpfTkd';
$appConfig->password = 'w24JTCv4MnAcuRTx0oHjHLDtyt3I6IBq';
$appConfig->baseURL = 'https://integration.edu.cdek.ru';

$client = new \CdekSDK\CdekClient(
    $appConfig->account,
    $appConfig->password
    ,new \GuzzleHttp\Client([
        'base_uri' => $appConfig->baseURL,
    ])
);

// this is an example of what should be passed as json
$neededData = [
    'CustomerFullname' => $name,
    'CustomerPhone' => $phone,
    'CustomerEmail' => $email, // this is optional argument
    'CustomerCityCode' => 270,
    'CustomerCityPostalCode' => '420500',

    'SenderAddressStreet' => $street,
    'SenderAddressHouse' => $house,
    'SenderAddressFlat' => $apt,
    'SenderCityCode' => $city,
    'SenderCityPostalCode' => '320500',

    'DeliveryPvzCode' => 'NSK67',
    'OrderID' => $last_id, // todo:  update this field if there is error 'Введенный номер отправления ИМ не уникальный. Введите уникальный номер' 
    // Денис обещал уточнить что использовать в этом месте.
    'DeliveryNumber' => 'kekNumber1',

    'PackageWeight' => 1000, // grams
    'Items' => $items,
];
echo json_encode($neededData);
// error_log(json_encode($neededData), 0);

function respondWithError4xx($error)
{
    // TODO: ask convention what to do if fail? how to pass error?
    $res['status'] = 0;
    $res['message'] = $error;
    echo json_encode($res);
    http_response_code(400);
}

function createOrder($data)
{
    global $client;
    global $appConfig;

// TODO: set normal Number value
    $orderInfo = [
        'Number' => $appConfig->orderIDPrefix . $data['OrderID'],
        'SendCityCode' => $data['SenderCityCode'],
        'SendCityPostCode' => $data['SenderCityPostalCode'],
        'RecCityCode' => $data['CustomerCityCode'],
        'RecCityPostCode' => $data['CustomerCityPostalCode'],
        'RecipientName' => $data['CustomerFullname'],
        'Phone' => $data['CustomerPhone'],
        'TariffTypeCode' => $appConfig->tariffCode, // Посылка дверь-дверь от ИМ
    ];
    if (isset($data['CustomerEmail'])) {
        $orderInfo['RecipientEmail'] = $data['CustomerEmail'];
    }
    $order = new Common\Order($orderInfo);

    $order->setSender(Common\Sender::create()->setAddress(Common\Address::create([
        'Street' => $data['SenderAddressStreet'],
        'House' => $data['SenderAddressHouse'],
        'Flat' => $data['SenderAddressFlat'],
    ])));

    $order->setAddress(Common\Address::create([
        'PvzCode' => $data['DeliveryPvzCode']
    ]));

    $package = Common\Package::create([
        'Number' => $data['OrderID'],
        'BarCode' => $data['OrderID'],
        'Weight' => $data['PackageWeight'],
        // TODO: if sizes of package are known, pass them
        'SizeA' => $_POST['length'], // Длина (в сантиметрах), в пределах от 1 до 1500
        'SizeB' => $_POST['weight'],
        'SizeC' => $_POST['height'],
    ]);

    foreach ($data['Items'] as $item) {
        $item['Payment'] = 0;
        $package->addItem(new Common\Item($item));
    }

    $order->addPackage($package);

    $request = new Requests\DeliveryRequest([
        'Number' => $data['DeliveryNumber'],
    ]);
    $request->addOrder($order);

    $response = $client->sendDeliveryRequest($request);


    if ($response->hasErrors()) {

        foreach ($response->getMessages() as $message) {
            if ($message->getErrorCode() !== '') {
                // Это ошибка; бывают случаи когда сообщение с ошибкой - пустая строка.
                // Потому нужно смотреть и на код, и на описание ошибки.
                error_log($message->getErrorCode(), 0);
                error_log($message->getMessage(), 0);
            }
        }
        $res['ok'] = false;
        $res['message'] = json_encode($response->getMessages());
        return $res;
    } else {
        
        foreach ($response->getOrders() as $order) {
            // сверяем данные заказа, записываем номер
            //! todo: save dispatchnumber to db
            $db->query('INSERT INTO `order` (dispatch) VALUES("'.$order->getDispatchNumber().'")');
            error_log($order->getNumber(), 0);
            error_log($order->getDispatchNumber(), 0);
        }

        $res['ok'] = true;
        $res['message'] = 'ok';

        return $res;
    }


}

// TODO: toggle for debug
$input = json_decode(file_get_contents('php://input'), true);
if (json_last_error() !== JSON_ERROR_NONE) {
respondWithError4xx('invalid json');
}
// TODO: toggle for debug
// $input = $neededData;

$res = createOrder($input);

if (!$res['ok']) {
    respondWithError4xx($res['message']);
}
else {
    http_response_code(200);
}


